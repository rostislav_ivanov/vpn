﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Security.ExchangeActiveSyncProvisioning;
using Windows.Storage.Streams;
using Windows.System.Profile;
using Windows.UI.Xaml;

namespace VPN.BackgroundAgent
{
    public sealed class SystemInfo
    {
        public static string GetAppVersion()
        {
            dynamic type = Application.Current.GetType();
            var version = (new AssemblyName(type.Assembly.FullName)).Version;
            return String.Format("{0}.{1}", version.Major, version.Minor);
        }

        public static string GetDeviceID()
        {
            HardwareToken token = HardwareIdentification.GetPackageSpecificToken(null);
            IBuffer hardwareId = token.Id;

            HashAlgorithmProvider hasher = HashAlgorithmProvider.OpenAlgorithm("MD5");
            IBuffer hashed = hasher.HashData(hardwareId);

            string hashedString = CryptographicBuffer.EncodeToHexString(hashed);
            return hashedString;
        }

        public static string GetDeviceModel()
        {
            var deviceInformation = new EasClientDeviceInformation();
            return deviceInformation.FriendlyName;
        }

        public static string GetCurrentLanguageNameTwoLetter()
        {
            //CultureInfo ci = new CultureInfo(Windows.System.UserProfile.GlobalizationPreferences.Languages[0]);
            //return ci.TwoLetterISOLanguageName;

            return Windows.System.UserProfile.GlobalizationPreferences.Languages[0].Substring(0, 2);
        }

        public static string GetTimeZone()
        {
            return ((TimeZoneInfo.Local.BaseUtcOffset < TimeSpan.Zero) ? "-" : "+") + TimeZoneInfo.Local.BaseUtcOffset.ToString("hhmm");
        }
    }
}
