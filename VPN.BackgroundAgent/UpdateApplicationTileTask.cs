﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.Web.Http;
using NotificationsExtensions.TileContent;
using NotificationsExtensions.ToastContent;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.System;
using Windows.UI.Notifications;
using NotificationsExtensions.TileContent;
using VPN.Model;
using VPN.Model.SocialNetworks;
using UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding;

namespace VPN.BackgroundAgent
{
    public sealed class UpdateApplicationTileTask : IBackgroundTask
    {
        async void IBackgroundTask.Run(IBackgroundTaskInstance taskInstance)
        {
            var deferral = taskInstance.GetDeferral();
            try
            {
                await ExecuteInner(true);
            }
            finally
            {
                deferral.Complete();
            }
        }

        internal static async void ExecuteBackgroundTask()
        {
            try
            {
                await ExecuteInner(false);
            }
            catch (Exception ex)
            {
                if (Debugger.IsAttached) Debugger.Break();
            }
        }

        public static void SetUpPushNotification(bool isVPNNotConnected)
        {
            try
            {
                if (ApplicationData.Current.LocalSettings.Values["isUserKnowHowToConnect"] != null || !isVPNNotConnected)
                {
                    bool firstOrSecond;

                    if (ApplicationData.Current.LocalSettings.Values["ShowMakeReview"] == null)
                    {
                        firstOrSecond = true;
                    }
                    else if (ApplicationData.Current.LocalSettings.Values["ShowMakeReview"].Equals("FirstShown"))
                    {
                        firstOrSecond = false;
                    }
                    else
                    {
                        return;
                    }

                    var notificatioDeliveryTime = firstOrSecond ? DateTime.Now.Add(new TimeSpan(0, 0, 5, 0)) : DateTime.Now.Add(new TimeSpan(0, 0, 10, 0));

                    var toastContent = ToastContentFactory.CreateToastText01();
                    toastContent.TextBodyWrap.Text =
                        Localization.LocalizedResources.GetLocalizedString("S_PUSH_RATE_US");
                    toastContent.Duration = ToastDuration.Long;
                    var tostNotification = toastContent.CreateNotification();

                    tostNotification.Activated += (s, e) => Launcher.LaunchUriAsync(new Uri("vpnunlimited://runApp"));

                    var recurringToast = new ScheduledToastNotification(tostNotification.Content,
                        notificatioDeliveryTime) { Id = firstOrSecond ? "MakeReview1" : "MakeReview2" };
                    ToastNotificationManager.CreateToastNotifier().AddToSchedule(recurringToast);

                    ApplicationData.Current.LocalSettings.Values["ShowMakeReview"] = firstOrSecond
                        ? "Sheduled1"
                        : "Sheduled2";
                }
                else
                {
                    if (ApplicationData.Current.LocalSettings.Values["ShowVPNNotConnected"] == null)
                    {
                        DateTime firstLoginTime = new DateTime((long)ApplicationData.Current.LocalSettings.Values["FirstLoginTime"]);

                        var howMuchTimePassed = DateTime.Now - firstLoginTime;
                        if (howMuchTimePassed.TotalMinutes > 5)
                        {
                            var toastContent = ToastContentFactory.CreateToastText01();
                            toastContent.TextBodyWrap.Text = Localization.LocalizedResources.GetLocalizedString("S_NOTIFI_VPN_DISABLED");
                            toastContent.Duration = ToastDuration.Long;
                            var tostNotification = toastContent.CreateNotification();

                            tostNotification.Activated += (s, e) => Launcher.LaunchUriAsync(new Uri("vpnunlimited://runApp"));

                            var recurringToast = new ToastNotification(tostNotification.Content);

                            ApplicationData.Current.LocalSettings.Values["ShowVPNNotConnected"] = "AlreadyShown";
                            ToastNotificationManager.CreateToastNotifier().Show(recurringToast);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (Debugger.IsAttached) Debugger.Break();
            }
        }

        private static async Task ExecuteInner(bool isBackgroundAgentCall)
        {
            bool isVPNNotConnected = !(await CheckVPNConnection());

            if (AccountStatus != null)
            {
                SetUpPushNotification(isVPNNotConnected);


                string timeLeftString;
                TimeSpan timeLeft = new TimeSpan(0, 0, (int)AccountStatus.User.TimeLeft);

                if ((int) AccountStatus.User.TimeLeft == 0)
                {
                    timeLeftString = Localization.LocalizedResources.GetLocalizedString("WinPhoneTimeIsExperied");
                }
                else if (timeLeft.TotalDays > 50 * 365)
                {
                    timeLeftString = Localization.LocalizedResources.GetLocalizedString("S_INIFINITE_PLAN");
                }
                else
                {
                    if (timeLeft.TotalDays > 365)
                    {
                        timeLeftString = String.Concat((int)(timeLeft.TotalDays / 365), " ", Localization.LocalizedResources.GetLocalizedString("S_YEAR"), " ",
                        (int)((timeLeft.TotalDays % 365) / 30), " ", Localization.LocalizedResources.GetLocalizedString("S_MONTHS"));
                    }
                    else if (timeLeft.TotalDays > 30)
                    {
                        timeLeftString = String.Concat((int)(timeLeft.TotalDays / 30), " ", Localization.LocalizedResources.GetLocalizedString("S_MONTHS"), " ",
                            (int)(timeLeft.TotalDays % 30), " ", Localization.LocalizedResources.GetLocalizedString("S_DAYS"));
                    }
                    else
                    {
                        timeLeftString = String.Concat((int)timeLeft.TotalDays, " ", Localization.LocalizedResources.GetLocalizedString("S_DAYS"), " ",
                            (int)timeLeft.Hours, " ", Localization.LocalizedResources.GetLocalizedString("S_HOURS"));
                    }
                    timeLeftString = String.Concat(Localization.LocalizedResources.GetLocalizedString("S_SUBSCRIPTION_REMAINING"), ": ", timeLeftString);
                }

                var tileManager = TileUpdateManager.CreateTileUpdaterForApplication();
                tileManager.Clear();

                var tile150X150 = TileContentFactory.CreateTileSquare150x150PeekImageAndText01();
                tile150X150.Image.Src = "ms-appx:///Assets/logo.png";
                tile150X150.Branding = TileBranding.Name;

                var tile150X150Back = TileContentFactory.CreateTileSquare150x150Text04();
                tile150X150Back.TextBodyWrap.Text = timeLeftString;
                tile150X150Back.Branding = TileBranding.Name;

                var tile71X71 = TileContentFactory.CreateTileSquare71x71Image();
                tile71X71.Image.Src = "ms-appx:///Assets/squareLogo.png";
                tile71X71.Branding = TileBranding.Name;
                tile150X150.Square71x71Content = tile71X71;

                var tile310X150Front = TileContentFactory.CreateTileWide310x150Image();
                tile310X150Front.Image.Src = "ms-appx:///Assets/widelogo.png";
                tile310X150Front.Square150x150Content = tile150X150;
                tile310X150Front.Branding = TileBranding.Name;

                var tile310X150Back = TileContentFactory.CreateTileWide310x150Text03();

                tile310X150Back.TextHeadingWrap.Text = timeLeftString;
                tile310X150Back.Square150x150Content = tile150X150Back;
                tile310X150Back.Branding = TileBranding.Name;
                tileManager.Update(tile310X150Front.CreateNotification());
                tileManager.Update(tile310X150Back.CreateNotification());
                tileManager.EnableNotificationQueueForWide310x150(true);
                tileManager.EnableNotificationQueueForSquare150x150(true);
            }
           
        }



        private static string VPNservice = "com.simplexsolutionsinc.vpnguard";
        private static string ApiLoginUrlString = "https://auth.simplexsolutionsinc.com/";
        private static string ApiUrlString = "https://api.vpnunlimitedapp.com/";
        private static string GoogleOauthServiceName = "googleplus";
        private static string FacebookOauthServiceName = "facebook";

        private static string GoogleCredentialsLocalSettingsName = "google";
        private static string FacebookCredentialsLocalSettingsName = "facebook";
        private static string KeepSolidCredentialsLocalSettingsName = "keepsolid";

        private static string GoogleClientId = "922621893574-grqe7ehb5sq05b4ag38nf0ho19icq2lt.apps.googleusercontent.com";
        private static string GoogleClientSecret = "VqZR9tiTyrP6-UY9G48_EJTg";

        private static string EncryptPassword = "L,2W~fKo|]*,VFiZ8&n";

        private static ResponceOnAccountStatus AccountStatus { get; set; }
        private static string Session { get; set; }
        private static string ToBase64(string str)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(str));
        }

        private static async Task<bool> CheckVPNConnection()
        {
            await AutoLogin();
            await GetAccountAsync();

            if (AccountStatus == null)
                return false;

            return AccountStatus.User.IsVPNActive;
        }

        private static CredentialsGoogle _сredentialsGoogle;
        [IgnoreDataMember]
        private static CredentialsGoogle CredentialsGoogle
        {
            get
            {
                if (_сredentialsGoogle == null)
                {
                    _сredentialsGoogle = LoadFromLocalSettings<CredentialsGoogle>(GoogleCredentialsLocalSettingsName);

                    if (_сredentialsGoogle != null)
                    {
                        string refreshTokenDecrypted = Decrypt(_сredentialsGoogle.RefreshToken, _сredentialsGoogle.Email, _сredentialsGoogle.Key);
                        _сredentialsGoogle.RefreshToken = refreshTokenDecrypted;
                    }
                }

                return _сredentialsGoogle;
            }
        }

        private static CredentialsFacebook _сredentialsFacebook;
        [IgnoreDataMember]
        private static CredentialsFacebook CredentialsFacebook
        {
            get
            {
                if (_сredentialsFacebook == null)
                {
                    _сredentialsFacebook = LoadFromLocalSettings<CredentialsFacebook>(FacebookCredentialsLocalSettingsName);

                    if (_сredentialsFacebook != null)
                    {
                        string refreshTokenDecrypted = Decrypt(_сredentialsFacebook.AccessToken, _сredentialsFacebook.Email, _сredentialsFacebook.Key);
                        _сredentialsFacebook.AccessToken = refreshTokenDecrypted;
                    }
                }

                return _сredentialsFacebook;
            }
        }

        private static CredentialsKeepSolidID _сredentialsKeepSolidID;
        [IgnoreDataMember]
        private static CredentialsKeepSolidID CredentialsKeepSolidID
        {
            get
            {
                if (_сredentialsKeepSolidID == null)
                {
                    _сredentialsKeepSolidID = LoadFromLocalSettings<CredentialsKeepSolidID>(KeepSolidCredentialsLocalSettingsName);

                    if (_сredentialsKeepSolidID != null)
                    {
                        string refreshTokenDecrypted = Decrypt(_сredentialsKeepSolidID.Password, _сredentialsKeepSolidID.Email, _сredentialsKeepSolidID.Key);
                        _сredentialsKeepSolidID.Password = refreshTokenDecrypted;
                    }
                }

                return _сredentialsKeepSolidID;
            }
        }

        private static async Task<CredentialsGoogle> RefreshAccessToken(CredentialsGoogle savedCredentials)
        {
            const string TokenUrl = "https://www.googleapis.com/oauth2/v3/token";

            var body = new StringBuilder();
            body.Append("refresh_token=");
            body.Append(Uri.EscapeDataString(savedCredentials.RefreshToken));
            body.Append("&client_id=");
            body.Append(Uri.EscapeDataString(GoogleClientId));
            body.Append("&client_secret=");
            body.Append(Uri.EscapeDataString(GoogleClientSecret));
            body.Append("&grant_type=refresh_token");

            var request = new Windows.Web.Http.HttpRequestMessage(Windows.Web.Http.HttpMethod.Post, new Uri(TokenUrl, UriKind.Absolute))
            {
                Content = new HttpStringContent(body.ToString(), UnicodeEncoding.Utf8, "application/x-www-form-urlencoded"),
            };


            var httpClient = new Windows.Web.Http.HttpClient();
            var responseMessage = await httpClient.SendRequestAsync(request);

            responseMessage.EnsureSuccessStatusCode();

            var dataStream = await responseMessage.Content.ReadAsBufferAsync();

            var serializer = new DataContractJsonSerializer(typeof(CredentialsGoogle));
            var oauthcredentialsWithNewAccessToken = (CredentialsGoogle)serializer.ReadObject(dataStream.AsStream());

            savedCredentials.AccessToken = oauthcredentialsWithNewAccessToken.AccessToken;
            savedCredentials.ExpiresDate = oauthcredentialsWithNewAccessToken.ExpiresDate;
            savedCredentials.TokenType = oauthcredentialsWithNewAccessToken.TokenType;

            return savedCredentials;
        }
        private static async Task<bool> AutoLogin()
        {
            var googleCredentials = CredentialsGoogle;
            if (googleCredentials != null)
            {
                var googleCredentialsWithAccessToken = await RefreshAccessToken(googleCredentials);
                await LoginGoogleAsync(googleCredentialsWithAccessToken);
                return true;
            }

            var facebookCredentials = CredentialsFacebook;
            if (facebookCredentials != null && facebookCredentials.RealExpirationDate > DateTime.UtcNow)
            {
                await LoginFacebookAsync(facebookCredentials);
                return true;
            }

            var keepSolidCredentials = CredentialsKeepSolidID;
            if (keepSolidCredentials != null)
            {
                await LoginAsync(keepSolidCredentials.Email, keepSolidCredentials.Password);
                return true;
            }

            return false;
        }

        private static async Task<bool> GetAccountAsync()
        {
            if (Session == null)
            {
                return false;
            }

            var multipartFormDataContent = new HttpMultipartFormDataContent();
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("account_status")), "action");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(Session)), "session");

            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(ApiUrlString, UriKind.Absolute))
            {
                Content = multipartFormDataContent
            };

            var httpClient = new HttpClient();
            var responseMessage = await httpClient.SendRequestAsync(request);

            responseMessage.EnsureSuccessStatusCode();

            var dataStream = await responseMessage.Content.ReadAsBufferAsync();
            var serializer = new DataContractJsonSerializer(typeof(ResponceOnAccountStatus));
            var result = (ResponceOnAccountStatus)serializer.ReadObject(dataStream.AsStream());

            if (result.Response != 200)
            {
                if (result.Response == 503)
                {
                    await AutoLogin();
                    return await GetAccountAsync();
                }
                else
                {
                    throw new VPNResponceException(result.Response.ToString());
                }
            }

            AccountStatus = result;
            return true;
        }

        private static async Task<bool> LoginFacebookAsync(CredentialsFacebook credentialsFacebook)
        {
            var serializer = new DataContractJsonSerializer(typeof(CredentialsFacebookVPNServer));
            string oauthcredentialsString;

            using (MemoryStream ms = new MemoryStream())
            {
                serializer.WriteObject(ms, new CredentialsFacebookVPNServer(credentialsFacebook));
                byte[] bytearray = ms.ToArray();
                oauthcredentialsString = Encoding.UTF8.GetString(bytearray, 0, bytearray.Count());
            }

            var multipartFormDataContent = new HttpMultipartFormDataContent();
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("loginsocial")), "action");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("2")), "social_login_version");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(credentialsFacebook.Email)), "login");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(FacebookOauthServiceName)), "oauthservice");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(oauthcredentialsString)), "oauthcredentials");
            AddParametersForRequestToVPN(multipartFormDataContent);

            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(ApiLoginUrlString, UriKind.Absolute))
            {
                Content = multipartFormDataContent
            };

            var httpClient = new HttpClient();
            var responseMessage = await httpClient.SendRequestAsync(request);

            var dataStream = await responseMessage.Content.ReadAsBufferAsync();
            serializer = new DataContractJsonSerializer(typeof(ResponseOnLogin));
            var result = (ResponseOnLogin)serializer.ReadObject(dataStream.AsStream());

            if (result.Response != 200)
            {
                throw new VPNResponceException(result.Response.ToString());
            }

            Session = result.Session;

            return true;
        }


        private static async Task<bool> LoginGoogleAsync(CredentialsGoogle credentialsGoogle)
        {
            var serializer = new DataContractJsonSerializer(typeof(CredentialsGoogleVPNServer));
            string oauthcredentialsString;

            using (MemoryStream ms = new MemoryStream())
            {
                serializer.WriteObject(ms, new CredentialsGoogleVPNServer(credentialsGoogle));
                byte[] bytearray = ms.ToArray();
                oauthcredentialsString = Encoding.UTF8.GetString(bytearray, 0, bytearray.Count());
            }

            var multipartFormDataContent = new HttpMultipartFormDataContent();
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("loginsocial")), "action");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("2")), "social_login_version");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(credentialsGoogle.Email)), "login");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(GoogleOauthServiceName)), "oauthservice");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(oauthcredentialsString)), "oauthcredentials");
            AddParametersForRequestToVPN(multipartFormDataContent);

            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(ApiLoginUrlString, UriKind.Absolute))
            {
                Content = multipartFormDataContent
            };

            var httpClient = new HttpClient();
            var responseMessage = await httpClient.SendRequestAsync(request);

            var dataStream = await responseMessage.Content.ReadAsBufferAsync();
            serializer = new DataContractJsonSerializer(typeof(ResponseOnLogin));
            var result = (ResponseOnLogin)serializer.ReadObject(dataStream.AsStream());

            if (result.Response != 200)
            {
                throw new VPNResponceException(result.Response.ToString());
            }

            Session = result.Session;

            return true;
        }

        private static async Task<bool> LoginAsync(string _email, string _password)
        {
            var multipartFormDataContent = new HttpMultipartFormDataContent();
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("login")), "action");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(_email)), "login");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(_password)), "password");
            AddParametersForRequestToVPN(multipartFormDataContent);

            var request = new Windows.Web.Http.HttpRequestMessage(Windows.Web.Http.HttpMethod.Post, new Uri(ApiLoginUrlString, UriKind.Absolute))
            {
                Content = multipartFormDataContent
            };

            var httpClient = new Windows.Web.Http.HttpClient();
            var responseMessage = await httpClient.SendRequestAsync(request);

            responseMessage.EnsureSuccessStatusCode();

            var dataStream = await responseMessage.Content.ReadAsBufferAsync();
            var serializer = new DataContractJsonSerializer(typeof(ResponseOnLogin));
            var result = (ResponseOnLogin)serializer.ReadObject(dataStream.AsStream());

            if (result.Response != 200)
            {
                throw new VPNResponceException(result.Response.ToString());
            }

            Session = result.Session;

            return true;
        }

        private static void AddParametersForRequestToVPN(HttpMultipartFormDataContent multipartFormDataContent)
        {
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(SystemInfo.GetDeviceModel())), "device");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(SystemInfo.GetDeviceID())), "deviceid");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("WinPhone")), "platform");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("8.1")), "platformversion");            //ToDo: no way to determine version
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("1.0")), "appversion");                 //ToDo: app version
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(VPNservice)), "service");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(SystemInfo.GetCurrentLanguageNameTwoLetter())), "locale");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(SystemInfo.GetTimeZone())), "timezone");
        }

        private static T LoadFromLocalSettings<T>(string name) where T : class
        {
            try
            {
                string localSettingsValue = (string)ApplicationData.Current.LocalSettings.Values[name];
                if (localSettingsValue == null) return null;

                using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(localSettingsValue)))
                {
                    var serializer = new DataContractJsonSerializer(typeof(T));
                    return (T)serializer.ReadObject(stream);
                }
            }
            catch
            {
                return null;
            }
        }

        public static string Decrypt(string dataToDecrypt, string openPartOfPassword, string salt)
        {
            string password = SystemInfo.GetDeviceID() + SystemInfo.GetDeviceModel() +
                openPartOfPassword + EncryptPassword;

            // Generate a key and IV from the password and salt
            IBuffer aesKeyMaterial;
            IBuffer iv;
            uint iterationCount = 10000;
            GenerateKeyMaterial(password, salt, iterationCount, out aesKeyMaterial, out iv);

            // Setup an AES key, using AES in CBC mode and applying PKCS#7 padding on the input
            SymmetricKeyAlgorithmProvider aesProvider = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbcPkcs7);
            CryptographicKey aesKey = aesProvider.CreateSymmetricKey(aesKeyMaterial);

            // Convert the base64 input to an IBuffer for decryption
            IBuffer ciphertext = CryptographicBuffer.DecodeFromBase64String(dataToDecrypt);

            // Decrypt the data and convert it back to a string
            IBuffer decrypted = CryptographicEngine.Decrypt(aesKey, ciphertext, iv);
            byte[] decryptedArray = decrypted.ToArray();
            return Encoding.UTF8.GetString(decryptedArray, 0, decryptedArray.Length);
        }

        private static void GenerateKeyMaterial(string password, string salt, uint iterationCount, out IBuffer keyMaterial, out IBuffer iv)
        {
            // Setup KDF parameters for the desired salt and iteration count
            IBuffer saltBuffer = CryptographicBuffer.ConvertStringToBinary(salt, BinaryStringEncoding.Utf8);
            KeyDerivationParameters kdfParameters = KeyDerivationParameters.BuildForPbkdf2(saltBuffer, iterationCount);

            // Get a KDF provider for PBKDF2, and store the source password in a Cryptographic Key
            KeyDerivationAlgorithmProvider kdf = KeyDerivationAlgorithmProvider.OpenAlgorithm(KeyDerivationAlgorithmNames.Pbkdf2Sha256);
            IBuffer passwordBuffer = CryptographicBuffer.ConvertStringToBinary(password, BinaryStringEncoding.Utf8);
            CryptographicKey passwordSourceKey = kdf.CreateKey(passwordBuffer);

            // Generate key material from the source password, salt, and iteration count.  Only call DeriveKeyMaterial once,
            // since calling it twice will generate the same data for the key and IV.
            int keySize = 256 / 8;
            int ivSize = 128 / 8;
            uint totalDataNeeded = (uint)(keySize + ivSize);
            IBuffer keyAndIv = CryptographicEngine.DeriveKeyMaterial(passwordSourceKey, kdfParameters, totalDataNeeded);

            // Split the derived bytes into a seperate key and IV
            byte[] keyMaterialBytes = keyAndIv.ToArray();
            keyMaterial = WindowsRuntimeBuffer.Create(keyMaterialBytes, 0, keySize, keySize);
            iv = WindowsRuntimeBuffer.Create(keyMaterialBytes, keySize, ivSize, ivSize);
        }

        private static string GenerateSalt()
        {
            IBuffer randomBuffer = CryptographicBuffer.GenerateRandom(1024);
            var hasher = HashAlgorithmProvider.OpenAlgorithm("SHA256");
            IBuffer hashed = hasher.HashData(randomBuffer);
            return CryptographicBuffer.EncodeToBase64String(hashed);
        }
    }
}