﻿using System.Linq;
using System;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.UI.Notifications;

namespace VPN.ViewModel
{
    public static class BackgroundAgentsHelper
    {
        private const string TaskEntryPoint = "VPN.BackgroundAgent.UpdateApplicationTileTask";
        private const string OnInternetUpdater = "VPN on internet available task";
        private const string OnMaintenanceUpdater = "VPN on maintenance timer task";

        public static async Task RegisterUpdateApplicationTileAgent()
        {
            foreach (var task in BackgroundTaskRegistration.AllTasks)
            {
                var taskName = task.Value.Name;
                if ( taskName == OnInternetUpdater || taskName == OnMaintenanceUpdater)
                    task.Value.Unregister(true);
            }

            /*if (BackgroundTaskRegistration.AllTasks.Values.FirstOrDefault(i => i.Name == OnMaintenanceUpdater) == null)
            {
                await RegisterUpdateAppTileTask(OnMaintenanceUpdater, new MaintenanceTrigger(15, false), true);
            }*/
            if (BackgroundTaskRegistration.AllTasks.Values.FirstOrDefault(i => i.Name == OnInternetUpdater) == null)
            {
                await RegisterUpdateAppTileTask(OnInternetUpdater, new SystemTrigger(SystemTriggerType.InternetAvailable, false), false);           
            }

            
        }

        private static async Task RegisterUpdateAppTileTask(string name, IBackgroundTrigger trigger, bool isWithInternetCondition)
        {
            var status = await BackgroundExecutionManager.RequestAccessAsync();
            var builder = new BackgroundTaskBuilder { Name = name, TaskEntryPoint = TaskEntryPoint };
            builder.SetTrigger(trigger);
            if (isWithInternetCondition) builder.AddCondition(new SystemCondition(SystemConditionType.InternetAvailable));
            builder.Register();
        }
    }
}
