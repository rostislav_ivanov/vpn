﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MetroLab.Common;
using VPN.Model.SocialNetworks;
using VPN.ViewModel.Http;

namespace VPN.ViewModel.Pages
{
    [DataContract]
    public class ChangePasswordPageViewModel : PageViewModel
    {
        public string EmailTextBoxPlaceHolder { get { return Localization.LocalizedResources.GetLocalizedString("S_EMAIL_TEMPLATE"); } }
        public string ChangePasswordPageHeader { get { return Localization.LocalizedResources.GetLocalizedString("S_CHANGE_PASS_TITLE").ToUpper(); } }
        public string PasswordTextBoxHeader { get { return Localization.LocalizedResources.GetLocalizedString("S_PASSWORD_PLACE"); } }
        public string NewPasswordTextBoxHeader { get { return Localization.LocalizedResources.GetLocalizedString("S_NEW_PASSWORD"); } }
        public string EmailTextBoxHeader { get { return Localization.LocalizedResources.GetLocalizedString("S_EMAIL_PLACE"); } }
        public string ChangePasswordCommandButtonText { get { return Localization.LocalizedResources.GetLocalizedString("S_CHANGE_PASSWORD_BTN").ToLower(); } }

        private string _email;
        [DataMember]
        public string Email
        {
            get { return _email; }
            set
            {
                SetProperty(ref _email, value);
            }
        }

        private string _password;
        [DataMember]
        public string Password
        {
            get { return _password; }
            set
            {
                SetProperty(ref _password, value);
            }
        }

        private string _newPassword;
        [DataMember]
        public string NewPassword
        {
            get { return _newPassword; }
            set
            {
                SetProperty(ref _newPassword, value);
            }
        }

        private bool _isLoadingEnabled;
        public bool IsLoadingEnabled
        {
            get { return _isLoadingEnabled; }
            set { SetProperty(ref _isLoadingEnabled, value); }
        }

        private ICommand _changePasswordCommand;
        public ICommand ChangePasswordCommand
        {
            get
            {
                return GetCommand(ref _changePasswordCommand, async o =>
                {
                    if (IsLoadingEnabled) return;

                    IsLoadingEnabled = true;
                    if (string.IsNullOrEmpty(Email) || !Email.Contains("@"))
                    {
                        RunLoadFailed(Localization.LocalizedResources.GetLocalizedString("S_INVALID_EMAIL"),
                        true, null, Localization.LocalizedResources.GetLocalizedString("S_ERROR"));
                    }
                    else if (string.IsNullOrEmpty(Password) || string.IsNullOrEmpty(NewPassword))
                    {
                        RunLoadFailed(Localization.LocalizedResources.GetLocalizedString("S_NO_VALID_PASSWORD"),
                        true, null, Localization.LocalizedResources.GetLocalizedString("S_ERROR"));
                    }
                    else if (NewPassword.Length < 6)
                    {
                        RunLoadFailed(Localization.LocalizedResources.GetLocalizedString("S_SMALL_PASS_ALERT"),
                        true, null, Localization.LocalizedResources.GetLocalizedString("S_ERROR"));
                    }
                    else
                    {
                        if (await TryLoadAsyncInner(async () => await VPNServerAgent.Current.ChangePassword(_password, _newPassword)))
                        {
                            AutoLoginAgent.Current.CredentialsKeepSolidID = new CredentialsKeepSolidID()
                            {
                                Email = Email,
                                Password = NewPassword,
                            };

                            AutoLoginAgent.Current.Session = null;

                            if (await TryLoadAsyncInner(async () => await AutoLoginAgent.AutoLogin()))
                            {
                                await ShowDialogAsync(Localization.LocalizedResources.GetLocalizedString("S_PASS_CHANGED"),
                                    Localization.LocalizedResources.GetLocalizedString("S_WARNING"), null, DialogOkButtonTitle);

                                await AppViewModel.Current.GoBack();
                            }

                        }
                    }
                    IsLoadingEnabled = false;
                });
            }
        }
    }
}
