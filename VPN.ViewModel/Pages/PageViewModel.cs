﻿using System;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Windows.ApplicationModel.DataTransfer;
using Windows.Storage;
using Windows.Storage.Streams;
using MetroLab.Common;

namespace VPN.ViewModel
{
    [DataContract]
    public class PageViewModel : VPNDataLoadViewModel, IPageViewModel
    {

        public virtual void OnNavigating() {}

        [IgnoreDataMember]
        public IPrintHelper PrintHelper { get; set; }
        public virtual async Task RequestData(DataTransferManager manager, DataRequestedEventArgs args) { }

        protected async Task<IRandomAccessStream> GetStreamByLocalUri(Uri localUri)
        {
            var imageStream = new InMemoryRandomAccessStream();
            var resultStorageFile = await StorageFile.GetFileFromApplicationUriAsync(localUri);
            await RandomAccessStream.CopyAsync(await resultStorageFile.OpenSequentialReadAsync(), imageStream);
            await imageStream.FlushAsync();
            return imageStream;
        }

        public virtual async Task OnSerializingToStorageAsync() { }

        public virtual async Task OnSerializedToStorageAsync() { }

        public virtual async Task OnNavigatingAsync() { }

    }
}
