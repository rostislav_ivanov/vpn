﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MetroLab.Common;
using VPN.Model;
using VPN.ViewModel.Items;

namespace VPN.ViewModel.Pages
{
    [DataContract]
    public class ServerCredentialsPageViewModel : PageViewModel
    {
        public string ServerDetailsMessageText { get { return Localization.LocalizedResources.GetLocalizedString("WinPhoneServerDetailsMessage"); } }

        public string ProtocolValueText { get { return Localization.LocalizedResources.GetLocalizedString("WinPhoneConnectionTypeL2TP"); } }

        public string PasswordTitleText { get { return Localization.LocalizedResources.GetLocalizedString("S_PASSWORD_PLACE") + ":"; } }

        public string UserNameText { get { return Localization.LocalizedResources.GetLocalizedString("S_PROXY_USER_NAME ") + ":"; } }
        public string ProtocolTypeTitleText { get { return Localization.LocalizedResources.GetLocalizedString("WinPhoneProtocolTypeTitle") + ":"; } }
        public string ConnectionTypeTitleText { get { return Localization.LocalizedResources.GetLocalizedString("WinPhoneConnectionTypeTitle") + ":"; } }
        public string ConnectionValueText { get { return Localization.LocalizedResources.GetLocalizedString("WinPhoneConnectionValue"); } }
        public string PresharedKeyText { get { return Localization.LocalizedResources.GetLocalizedString("WinPhonePresharedKeyTitle") + ":"; } }

        public string ServerIPTitleText { get { return Localization.LocalizedResources.GetLocalizedString("WinPhoneServerIPTitle") + ":"; } }

        public string ManualButtonText { get { return Localization.LocalizedResources.GetLocalizedString("S_HOW_TO").ToLowerInvariant(); } }
        

        private ServerViewModel _server;
        [DataMember]
        public ServerViewModel Server
        {
            get { return _server; }
            set { SetProperty(ref _server, value); }
        }

        private Config _config;
        [DataMember]
        public Config Config
        {
            get { return _config; }
            set { SetProperty(ref _config, value); }
        }

        public ServerCredentialsPageViewModel(ServerViewModel server, Config config)
        {
            this.Server = server;
            this.Config = config;
        }

        private ICommand _openGuideGalleryPage;
        public ICommand OpenGuideGalleryPage
        {
            get
            {
                return  GetCommand(ref _openGuideGalleryPage, async o =>
                    await AppViewModel.Current.NavigateToViewModel(new GuideGalleryPageViewModel(Config)));
            }
        }

        
    }
}
