﻿using System.Collections.ObjectModel;
using Windows.ApplicationModel.DataTransfer;
using Windows.ApplicationModel.Store;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Notifications;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using MetroLab.Common;
using NotificationsExtensions.TileContent;
using VPN.Model;
using VPN.Model.SocialNetworks;
using VPN.ViewModel.Adapters;
using VPN.ViewModel.Http;
using VPN.ViewModel.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Windows.Input;
using VPN.ViewModel.Pages;



using System.Linq;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.UI.Notifications;


namespace VPN.ViewModel
{
    [DataContract]
    public class MainPageViewModel : BaseTilesPageViewModel
    {
        private ObservableCollection<ServerViewModel> _servers;

        [DataMember]
        public ObservableCollection<ServerViewModel> Servers
        {
            get { return _servers; }
            set { if (SetProperty(ref _servers, value)) OnPropertyChanged("Servers"); }
        }

        private ServerViewModel _selectedServer;

        [DataMember]
        public ServerViewModel SelectedServer
        {
            get { return _selectedServer; }
            set
            {
                if (SetProperty(ref _selectedServer, value))
                {
                    OnPropertyChanged("SelectedServer");
                    OnPropertyChanged("CurrentServerText");
                }
            }
        }

        /*
        private List<NotificationItem> _notificationsList;
        [DataMember]
        public List<NotificationItem> NotificationsList
        {
            get { return _notificationsList; }
            set { if (SetProperty(ref _notificationsList, value)) OnPropertyChanged("NotificationsList"); }
        }*/

        private List<PropositionViewModel> _propositionsList;
        [DataMember]
        public List<PropositionViewModel> PropositionsList
        {
            get { return _propositionsList; }
            set { if (SetProperty(ref _propositionsList, value)) OnPropertyChanged("PropositionsList"); }
        }

        private string _realIP;
        [IgnoreDataMember]
        public string RealIP
        {
            get { return String.Concat(Localization.LocalizedResources.GetLocalizedString("S_MAP_TITLE"), ": ", _realIP); }
            set { SetProperty(ref _realIP, value); }
        }

        private string _currentIP;
        [IgnoreDataMember]
        public string CurrentIP
        {
            get { return String.Concat(Localization.LocalizedResources.GetLocalizedString("WinPhoneVitrualIPTitle"), ": ", _currentIP); }
            set { SetProperty(ref _currentIP, value); }
        }

        private string _timeLeft;
        [IgnoreDataMember]
        public string TimeLeft
        {
            get
            {
                if (IsExpired)
                    return Localization.LocalizedResources.GetLocalizedString("WinPhoneTimeIsExperied");
                else
                    return String.Concat(Localization.LocalizedResources.GetLocalizedString("S_SUBSCRIPTION_REMAINING"), ": ", _timeLeft);
            }
            set { SetProperty(ref _timeLeft, value); }
        }

        [IgnoreDataMember]
        public string CurrentServerText  {
            get
            {
                if (null == SelectedServer || null == SelectedServer.CountryPlusCity)
                    return null;
                else
                    return Localization.LocalizedResources.GetLocalizedString("S_CONNECTED_TO")
                                .Replace("%s", SelectedServer.CountryPlusCity); }
        }

        [IgnoreDataMember]
        public string SelectServerTitle
        {
            get { return String.Concat(Localization.LocalizedResources.GetLocalizedString("S_MAP_LIST_TITLE"), ":"); }
        }

        public string ConnectionProblemTitle
        {
            get { return Localization.LocalizedResources.GetLocalizedString("S_CONNECTION_PROBLEM"); }
        }
        
        private bool onOrOff;

        [IgnoreDataMember]
        public bool OnOrOff
        {
            get { return onOrOff; }
            set
            {
                if (SetProperty(ref onOrOff, value))
                {
                    OnPropertyChanged("ConnectionBlockText");
                    OnPropertyChanged("ConnectionBlockForeground");
                }
                OnPropertyChanged("OnOrOff");
            }
        }

        private bool _isExpired;
        public bool IsExpired
        {
            get { return _isExpired; }
            set
            {
                SetProperty(ref _isExpired, value);
            }
        }
        [IgnoreDataMember]
        public string ConnectionBlockText
        {
            get
            {
                if (OnOrOff)
                    return Localization.LocalizedResources.GetLocalizedString("S_MAP_FOOTER_TITLE_ON");
                else
                    return Localization.LocalizedResources.GetLocalizedString("S_MAP_FOOTER_TITLE_OFF");
            }
        }

        [IgnoreDataMember]
        public SolidColorBrush ConnectionBlockForeground
        {
            get
            {
                if (OnOrOff)
                    return new SolidColorBrush(Color.FromArgb(255, 131, 186, 31));
                else
                    return new SolidColorBrush(Color.FromArgb(255, 229, 20, 0));
            }
        }

        [IgnoreDataMember]
        public string ContentUnavailableText
        {
            get { return Localization.LocalizedResources.GetLocalizedString("S_INTERNET_PROBLEM"); }
        }
        [IgnoreDataMember]
        public string TryAgainText
        {
            get { return Localization.LocalizedResources.GetLocalizedString("WinPhoneTryAgainButton"); }
        }

        public string ConnectionHeaderText
        {
            get { return Localization.LocalizedResources.GetLocalizedString("WinPhoneConnectionTitle").ToLower(); }
        }

        /*ToDo: push notifications
        public string NotificationsHeaderText
        {
            get { return Localization.LocalizedResources.GetLocalizedString("NotificationsTitle").ToLower(); }
        }*/
        
        public string PurchasesHeaderText
        {
            get { return Localization.LocalizedResources.GetLocalizedString("S_PURCHASES").ToLower(); }
        }

        public string GetForFreeButtonText
        {
            get { return Localization.LocalizedResources.GetLocalizedString("WinPhoneGetForFreeButton").ToLower(); }
        }

        public string BuyMoreTitleText
        {
            get { return Localization.LocalizedResources.GetLocalizedString("S_BUY_MORE") + ":"; }
        }

        public string SettingsButtonText
        {
            get { return Localization.LocalizedResources.GetLocalizedString("S_SETTINGS").ToLower(); }
        }

        private bool _areButtonsDisabled;
        public bool AreButtonsDisabled
        {
            get { return _areButtonsDisabled; }
            set { SetProperty(ref _areButtonsDisabled, value); }
        }

        private bool IsUserKnowHowToConnect;


        private const string TaskEntryPoint = "VPN.BackgroundAgent.UpdateApplicationTileTask";
        private const string OnTimerUpdater = "VPN is not connected on timer task";
        private const string OnInternetUpdater = "VPN on internet available task";
        private const string OnMaintenanceUpdater = "VPN on maintenance timer task";

        private ICommand _settingsCommand;
        public ICommand SettingsCommand { 
            get 
            {
                return GetCommand(ref _settingsCommand, async o => await AppViewModel.Current.NavigateToViewModel(new SettingsPageViewModel()));
            }
        }


        private ICommand _addFriend;
        public ICommand AddFriend
        {
            get
            {
                return GetCommand(ref _addFriend, async o =>
                    await AppViewModel.Current.NavigateToViewModel(new InviteFriendPageViewModel()));
            }
        }

        private ICommand _loadInAppsCommand;
        [IgnoreDataMember]
        public ICommand LoadInAppsCommand
        {
            get { return GetCommand(ref _loadInAppsCommand, o => LoadPurchases()); }
        }

        private async Task LoadPurchases()
        {
            
            System.Uri certificateFile = new System.Uri("ms-appx:///Assets/WindowsStoreProxy.xml");
            Windows.Storage.StorageFile simulatorSettingsFile = await Windows.Storage.StorageFile.GetFileFromApplicationUriAsync(certificateFile);

            Windows.ApplicationModel.Store.CurrentAppSimulator.ReloadSimulatorAsync(simulatorSettingsFile);
            
            //new string[] { "com.KeepSolid.VPN.10days.WinPhone", "com.KeepSolid.VPN.1month.WinPhone", "com.KeepSolid.VPN.1year.WinPhone",
            //"com.KeepSolid.VPN.3months.WinPhone","com.KeepSolid.VPN.3years.WinPhone"});

            try
            {
                ListingInformation listings = await CurrentAppSimulator.LoadListingInformationAsync();
                var adapter = new StoreInAppToPropositionViewModelAdapter();
                var propositionsListFromStore= new List<PropositionViewModel>(listings.ProductListings.Values.Select(adapter.Convert));
                propositionsListFromStore.Sort(new PropositionViewModelsComparer());
                PropositionsList = propositionsListFromStore;
            }
            catch(Exception ex){}

            /*
            PropositionsList = new List<PropositionViewModel>
            {
                new PropositionViewModel {Discount = 70, Period = Localization.LocalizedResources.GetLocalizedString("S_PURCHASE_SUBTITLE_3_YEAR"), Price = "64.99$"},
                new PropositionViewModel {Discount = 45, Period = Localization.LocalizedResources.GetLocalizedString("S_PURCHASE_SUBTITLE_1_YEAR"), Price = "27.99$"},
                new PropositionViewModel {Discount = 34, Period = Localization.LocalizedResources.GetLocalizedString("S_PURCHASE_SUBTITLE_3_MONTH"), Price = "9.99$"},
                new PropositionViewModel {Discount = 0, Period = Localization.LocalizedResources.GetLocalizedString("S_PURCHASE_SUBTITLE_1_MONTH"), Price = "3.99$"},
                new PropositionViewModel {Discount = 0, Period = Localization.LocalizedResources.GetLocalizedString("S_PURCHASE_SUBTITLE_10_DAYS"), Price = "1.99$"}
            };
            */
        }

        protected override async Task<bool> LoadAsync()
        {
            LoadLocaliztion();
            IsUserKnowHowToConnect = CacheAgent.IsUserKnowHowToConnect;

            /* ToDo: push notifications
            NotificationsList = new List<NotificationItem>
            {
                new NotificationItem { Name = "Get the 2x boost for", IconName = "icon_rocket", Url = "http://sc2tv.ru", 
                    Type = NotificationItem.NotificationType.Browser},
                new NotificationItem { Name = Localization.LocalizedResources.GetLocalizedString("S_RATE_US"), IconName = "icon_rate", 
                    Type = NotificationItem.NotificationType.Rate},
                new NotificationItem { Name = Localization.LocalizedResources.GetLocalizedString("S_SHARE"), IconName = "icon_share", 
                    Type = NotificationItem.NotificationType.Share },
            };*/

            await LoadPurchases();

            if (!await LoadAsync(VPNServerAgent.Current)) return false;

            return true;
        }

        public async void SelectServer(ServerViewModel selectedServer)
        {
            if (AreButtonsDisabled) return;
            AreButtonsDisabled = true;
            
            if (!IsExpired)
            { 
                if (await TryLoadAsyncInner(async () => await VPNServerAgent.Current.GetConnectionCredentialsAsync(selectedServer.Region)))
                {
                    var config = VPNServerAgent.Current.VPNConnectionConfig;
                    if (!CacheAgent.IsUserKnowHowToConnect)
                    {
                        await AppViewModel.Current.NavigateToViewModel(new GuideGalleryPageViewModel(config));
                        
                    }
                    else
                    {
                        await AppViewModel.Current.NavigateToViewModel(new ServerCredentialsPageViewModel(selectedServer, config));
                    }
                }
            }

            AreButtonsDisabled = false;
        }


        protected async Task<bool> LoadAsync(VPNServerAgent agent)
        {
            if (await TryLoadAsyncInner(async () => await agent.GetVPNServersListAsync()))
            {
                var adapter = new ServerToServerViewModelAdapter();
                Servers = new ObservableCollection<ServerViewModel>(VPNServerAgent.Current.ServersList.Items.Select(adapter.Convert));

                await UpdateStatusAsync();

                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> UpdateStatusAsync()
        {
            bool isSuccess = false;
            try
            {
                isSuccess = await VPNServerAgent.Current.GetAccountAsync();
            }
            catch (Exception)
            {
                OnOrOff = false;
                return false;
            }

            if (!isSuccess || VPNServerAgent.Current.ServersList == null)
            {
                OnOrOff = false;
                return false;
            }

            var account = VPNServerAgent.Current.AccountStatus;
            OnOrOff = account.User.IsVPNActive;
            RealIP = account.User.RealIP;
            CurrentIP = account.User.CurrentIP;

         
            if ((int) account.User.TimeLeft == 0)
            {
                IsExpired = true;
            }

            TimeSpan timeLeft = new TimeSpan(0, 0, (int) account.User.TimeLeft);

            if (timeLeft.TotalDays > 50 * 365)
            {
                TimeLeft = Localization.LocalizedResources.GetLocalizedString("S_INIFINITE_PLAN");
            }
            else if (timeLeft.TotalDays > 365)
            {
                TimeLeft = String.Concat((int)(timeLeft.TotalDays / 365), " ", Localization.LocalizedResources.GetLocalizedString("S_YEAR"), " ",
                (int)((timeLeft.TotalDays % 365) / 30), " ", Localization.LocalizedResources.GetLocalizedString("S_MONTHS"));
            }
            else if (timeLeft.TotalDays > 30)
            {
                TimeLeft = String.Concat((int)(timeLeft.TotalDays / 30), " ", Localization.LocalizedResources.GetLocalizedString("S_MONTHS"), " ",
                    (int)(timeLeft.TotalDays % 30), " ", Localization.LocalizedResources.GetLocalizedString("S_DAYS"));
            }
            else
            {
                TimeLeft = String.Concat((int)(timeLeft.TotalDays), " ", Localization.LocalizedResources.GetLocalizedString("S_DAYS"), " ",
                    (int)(timeLeft.Hours), " ", Localization.LocalizedResources.GetLocalizedString("S_HOURS"));
            }


            if (OnOrOff)
            {
                if (!IsUserKnowHowToConnect) //To avoid writing in locale settings data every minute
                {
                    CacheAgent.IsUserKnowHowToConnect = true;  
                    IsUserKnowHowToConnect = true;
                }
                SelectedServer = Servers.Where(f => f.Region == account.User.VPNRegion).First();
            }

            return true;
        }

        protected override async Task<bool> UpdateInnerAsync() { return await LoadAsync(); }

        public async void SelectProposition(PropositionViewModel selectedProposition)
        {
            if (AreButtonsDisabled) return;
            AreButtonsDisabled = true;
            /*

            PurchaseResults purchaseResults = await CurrentAppSimulator.RequestProductPurchaseAsync("com.KeepSolid.VPN.10days.WinPhone");
            //string receipt = await CurrentAppSimulator.GetProductReceiptAsync("com.KeepSolid.VPN.10days.WinPhone");
            */
            /*
            System.Uri certificateFile = new System.Uri("ms-appx:///Assets/WindowsStoreProxy.xml");
            Windows.Storage.StorageFile simulatorSettingsFile = await Windows.Storage.StorageFile.GetFileFromApplicationUriAsync(certificateFile);

            Windows.ApplicationModel.Store.CurrentAppSimulator.ReloadSimulatorAsync(simulatorSettingsFile);
            PurchaseResults purchaseResults = await CurrentAppSimulator.RequestProductPurchaseAsync("com.KeepSolid.VPN.10days.WinPhone");

            ProductLicense productLicense = null;
            if (CurrentAppSimulator.LicenseInformation.ProductLicenses.TryGetValue("com.KeepSolid.VPN.10days.WinPhone", out productLicense))
            {
                if (productLicense.IsActive)
                {
                    await ShowDialogAsync(Localization.LocalizedResources.GetLocalizedString("S_PURCHASE_CONFIRMED"), null, null);
                }
            }
            */

            PurchaseResults purchaseResults;
            try
            {
                purchaseResults = await CurrentApp.RequestProductPurchaseAsync(selectedProposition.ID);

                FulfillmentResult fulfillmentResult = FulfillmentResult.ServerError;
                if (purchaseResults.Status == ProductPurchaseStatus.NotFulfilled)
                {
                    fulfillmentResult = await CurrentApp.ReportConsumableFulfillmentAsync(selectedProposition.ID, purchaseResults.TransactionId);
                }

                if (purchaseResults.Status == ProductPurchaseStatus.Succeeded || fulfillmentResult == FulfillmentResult.Succeeded)
                {
                    ProductLicense productLicense = null;
                    if (CurrentApp.LicenseInformation.ProductLicenses.TryGetValue(selectedProposition.ID, out productLicense))
                    {
                        if (await TryLoadAsyncInner(async () =>
                            await VPNServerAgent.Current.PurchaseAsync(purchaseResults.ReceiptXml, purchaseResults.TransactionId.ToString())))
                        {
                            await ShowDialogAsync(Localization.LocalizedResources.GetLocalizedString("S_PURCHASE_CONFIRMED"), null,
                            new UICommandInvokedHandler(CommandHandlers), null);
                        }
                    }
                }

            }
            catch (Exception ex){}

            AreButtonsDisabled = false;
        }

        public async void CommandHandlers(IUICommand commandLabel)
        {
            await UpdateStatusAsync();
        }
    }
}
