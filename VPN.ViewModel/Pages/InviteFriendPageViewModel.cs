﻿using System.Runtime.Serialization;
using System.Threading.Tasks;
using VPN.ViewModel;
using VPN.ViewModel.Http;

namespace VPN.ViewModel
{
    [DataContract]
    public class InviteFriendPageViewModel : PageViewModel
    {
        public string InviteFriendsPageDescription { get { return VPN.Localization.LocalizedResources.GetLocalizedString("S_TIP_INVITES"); } }
        public string InviteFriendsPageHeader { get { return VPN.Localization.LocalizedResources.GetLocalizedString("S_GET_FREE").ToUpper(); } }
        public string InviteFriendsListContactsItemText { get { return VPN.Localization.LocalizedResources.GetLocalizedString("WinPhoneInviteFriendsListContactsItem"); } }

        //public string InviteFriendsListGooglePlusItemText { get { return VPN.Localization.LocalizedResources.GetLocalizedString("InviteFriendsListGooglePlusItem"); } }
        public string InviteFriendsListHeader { get { return VPN.Localization.LocalizedResources.GetLocalizedString("S_INVITE_FRIENDS") + ":"; } }

        public async Task InviteFriendAsync(string friendemail)
        {
            if (await TryLoadAsyncInner(async () =>
                await VPNServerAgent.Current.InviteFriendAsync(friendemail)))
            {
                await ShowDialogAsync(Localization.LocalizedResources.GetLocalizedString("S_INVITE_SUCCESS"),"",null,Localization.LocalizedResources.GetLocalizedString("S_OK"));
            }
        }
    }
}
