﻿using System.Collections.Generic;
using System.Resources;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using MetroLab.Common;
using VPN.ViewModel.Items;
using VPN.Localization;
using VPN.ViewModel;

namespace VPN.ViewModel.Pages
{
    [DataContract]
    public class OverviewGalleryPageViewModel : PageViewModel
    {
        private List<OverviewPageViewModel> _items;
        [DataMember]
        public List<OverviewPageViewModel> Items
        {
            get { return _items; }
            set { SetProperty(ref _items, value); }
        }

        [DataMember]
        public bool IsLoadedFromSettings { get; set; }

        protected override async Task<bool> LoadAsync()
        {
            Items = new List<OverviewPageViewModel>
            {
                new OverviewPageViewModel(){ ImagePath = "/Assets/Overview/img-quick-tour-1.png", Title = LocalizedResources.GetLocalizedString("S_QTOUR_FIRST_TITLE"),
                    Text1 = LocalizedResources.GetLocalizedString("S_QTOUR_FIRST_HEADER"), Text2 = LocalizedResources.GetLocalizedString("S_QTOUR_FIRST_FOOTER")},

                new OverviewPageViewModel(){ ImagePath = "/Assets/Overview/img-quick-tour-2.png", Title = LocalizedResources.GetLocalizedString("S_QTOUR_SECOND_TITLE"),
                    Text1 = LocalizedResources.GetLocalizedString("S_QTOUR_SECOND_HEADER"), Text2 = LocalizedResources.GetLocalizedString("S_QTOUR_SECOND_FOOTER_FIRST"),
                    Text3 = LocalizedResources.GetLocalizedString("S_QTOUR_SECOND_FOOTER_SECOND"), Text4 = LocalizedResources.GetLocalizedString("S_QTOUR_SECOND_FOOTER_THIRD")},

                new OverviewPageViewModel(){ ImagePath = "/Assets/Overview/img-quick-tour-3.png", Title = LocalizedResources.GetLocalizedString("S_QTOUR_THIRD_TITLE"),
                    Text1 = LocalizedResources.GetLocalizedString("WinPhoneQuickTourThirdHeader"), 
                    Text2 = LocalizedResources.GetLocalizedString("WinPhoneQuickTourThirdFooterFirst"), IsLastPage = true }
            };

            return await base.LoadAsync();
        }

        public string ButtonNextText { get { return LocalizedResources.GetLocalizedString("S_NEXT_BTN"); } }
    }
}
