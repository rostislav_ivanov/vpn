﻿using System;
using System.Collections.Generic;
using System.Resources;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Windows.Input;
using MetroLab.Common;
using VPN.Model;
using VPN.ViewModel.Items;
using VPN.Localization;
using VPN.ViewModel;

namespace VPN.ViewModel.Pages
{
    [DataContract]
    public class GuideGalleryPageViewModel : PageViewModel
    {
        public GuideGalleryPageViewModel(Config config)
        {
            this.Config = config;
        }

        [DataMember]
        public Config Config;

        private List<GuidePageViewModel> _items;
        [DataMember]
        public List<GuidePageViewModel> Items
        {
            get { return _items; }
            set { SetProperty(ref _items, value); }
        }

        private int _selectedIndex;
        [DataMember]
        public int SelectedIndex { get { return _selectedIndex; }
            set { SetProperty(ref _selectedIndex, value); }
        }

        public string ButtonNextText
        {
            get
            {
                string nextTextLower = LocalizedResources.GetLocalizedString("S_NEXT_BTN");
                return String.Concat(char.ToUpper(nextTextLower[0]),nextTextLower.Substring(1)," >");
            }
        }

        public string FinishButtonText
        {
            get { return LocalizedResources.GetLocalizedString("WinPhoneFinishTutorialButton"); }
        }

        protected override async Task<bool> LoadAsync()
        {
            bool isUserHasUpdate1 = true;
            try
            {
                var result = Type.GetType("Windows.Phone.Notification.Management.AccessoryManager, Windows, Version=255.255.255.255, Culture=neutral, PublicKeyToken=null, ContentType=WindowsRuntime", false);

                if (result == null)
                {
                    isUserHasUpdate1 = false;
                }
            }
            catch (Exception e)
            {
            }

            List<GuidePageViewModel> items = new List<GuidePageViewModel>();
            string localizationNameTemplate = "WinPhoneTutorialPage";
            string ScreenPathTemplate = "/Assets/VPNGuide/00{0}.png";
            string stepNumberString; 

            stepNumberString = "1";
            items.Add(new GuidePageViewModel()
            {
                StepNumber = stepNumberString,
                StepDescription = LocalizedResources.GetLocalizedString(localizationNameTemplate + stepNumberString),
                ScreenPath = string.Format(ScreenPathTemplate, stepNumberString)
            });

            if (!isUserHasUpdate1)
            {
                stepNumberString = "2";
                items.Add(new GuidePageViewModel()
                {
                    StepNumber = stepNumberString,
                    StepDescription = LocalizedResources.GetLocalizedString("WinPhoneTutorialPage10"),
                    ScreenPath = string.Format("/Assets/VPNGuide/010.png")
                });

                stepNumberString = "3";
                items.Add(new GuidePageViewModel()
                {
                    StepNumber = stepNumberString,
                    StepDescription = LocalizedResources.GetLocalizedString("WinPhoneTutorialPage11"),
                    ScreenPath = string.Format("/Assets/VPNGuide/011.png")
                });
            }

            int beginIndex = isUserHasUpdate1 ? 0 : 2;

            for (int i = 2; i <= 9; i++)
            {
                stepNumberString = i.ToString();
                items.Add(new GuidePageViewModel()
                {
                    StepNumber = (beginIndex + i).ToString(),
                    StepDescription = LocalizedResources.GetLocalizedString(localizationNameTemplate + stepNumberString),
                    ScreenPath = string.Format(ScreenPathTemplate,stepNumberString)
                });
            }

            items[beginIndex + 3].DataToCopy = Config.Addresses[0].IP;
            items[beginIndex + 6].DataToCopy = Config.UsernameXauth;
            items[beginIndex + 7].DataToCopy = Config.Password;
            items[beginIndex + 8].DataToCopy = Config.PresharedKey;

            items.Add(new GuidePageViewModel()
            {
                StepNumber = (beginIndex + 10).ToString(),
                StepDescription = LocalizedResources.GetLocalizedString("WinPhoneTutorialPage12"),
                ScreenPath = string.Format("/Assets/VPNGuide/012.png")
            });

            items[beginIndex + 9].IsLastPage = true;


            Items = items;

            return await base.LoadAsync();
        }
    }
}
