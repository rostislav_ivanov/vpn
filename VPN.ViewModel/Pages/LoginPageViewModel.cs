﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Threading;
using Windows.ApplicationModel.Activation;
using Windows.Security.Authentication.OnlineId;
using Windows.Storage.Search;
using Windows.UI.Core;
using MetroLab.Common;
using System.Threading.Tasks;
using System.Windows.Input;
using VPN.Model;
using VPN.Model.SocialNetworks;
using VPN.Model.SocialNetworks.Exceptions;
using VPN.ViewModel.Http;
using VPN.ViewModel.Items;


namespace VPN.ViewModel.Pages
{
    [DataContract]
    public class LoginPageViewModel : BaseTilesPageViewModel
    {
        [IgnoreDataMember] public string LoginFacebookString { get { return Localization.LocalizedResources.GetLocalizedString("S_SIGN_FACEBOOK"); } }
        [IgnoreDataMember] public string LoginGooglePlusString { get { return Localization.LocalizedResources.GetLocalizedString("S_SIGN_GOOGLE"); } }
        [IgnoreDataMember] public string LoginKeepsolidIdString { get { return Localization.LocalizedResources.GetLocalizedString("S_SIGN_NATIVE"); } }
        [IgnoreDataMember] public string RegisterNewUserString { get { return Localization.LocalizedResources.GetLocalizedString("S_SIGN_CREATE"); } }
        [IgnoreDataMember] public string LoginPageDescription { get { return Localization.LocalizedResources.GetLocalizedString("S_SIGN_TITLE"); } }
        [IgnoreDataMember] public string ContentUnavailableText { get { return Localization.LocalizedResources.GetLocalizedString("S_INTERNET_PROBLEM"); } }
        [IgnoreDataMember] public string TryAgainText { get { return Localization.LocalizedResources.GetLocalizedString("WinPhoneTryAgainButton"); } }

        /*private bool _areButtonsDisabled;
        [IgnoreDataMember] public bool AreButtonsDisabled         //ToDo
        {
            get { return _areButtonsDisabled; }
            set { SetProperty(ref _areButtonsDisabled, value); }
        }*/

        private bool _isNeedToShowContent;
        [IgnoreDataMember] public bool IsNeedToShowContent      //ToDo
        {
            get { return _isNeedToShowContent & IsLoadedSuccessfully; }
            set { SetProperty(ref _isNeedToShowContent, value); }
        }

        private enum SocialNetwork
        {
            Facebook,
            Google
        };

        private SocialNetwork provider;

        private ICommand _loginFacebookCommand;

        public ICommand LoginFacebookCommand
        {
            get
            {
                return GetCommand(ref _loginFacebookCommand, async o =>
                {
                    if (IsLoadingActive) return;
                    IsLoadingActive = true;
                     
                    provider = SocialNetwork.Facebook;

                    await FacebookAuthenticationAgent.AuthenticateWithApp(); //unfortunately it always return true

                    IsLoadingActive = false;
                });
            }
        }

        private ICommand _loginGooglePlusCommand;
        public ICommand LoginGooglePlusCommand
        {
            get
            {
                return GetCommand(ref _loginGooglePlusCommand, async o =>
                {
                    if (IsLoadingActive) return;
                    IsLoadingActive = true;

                    provider = SocialNetwork.Google;

                    GoogleAuthenticationAgent.Current.LoginAsync();
                });
            }
        }

        private ICommand _loginCommand;
        public ICommand LoginCommand
        {
            get
            {
                return GetCommand(ref _loginCommand, async o =>
                {
                    if (IsLoadingActive) return;
                    //await Task.Delay(1000);
                    await AppViewModel.Current.NavigateToViewModel(new LoginKeepSolidIDPageViewModel());
                });
            }
        }

        private ICommand _registerCommand;
        public ICommand RegisterCommand
        {
            get
            {
                return GetCommand(ref _registerCommand, async o =>
                {
                    if (IsLoadingActive) return;
                    //await Task.Delay(1000);
                    await AppViewModel.Current.NavigateToViewModel(new RegistrationPageViewModel());
                });
            }
        }
        public async void Finalize(WebAuthenticationBrokerContinuationEventArgs args)
        {
            if(provider == SocialNetwork.Google)
            {
                if (await TryLoadAsyncInner(async () => await GoogleAuthenticationAgent.Current.GetSession(args)))
                {
                    if (await TryLoadAsyncInner(async () => await VPNServerAgent.Current.LoginGoogleAsync(
                                            AutoLoginAgent.Current.CredentialsGoogle)))
                    {
                        AppViewModel.Current.ClearNavigationStack();
                        await AppViewModel.Current.NavigateToViewModel(new MainPageViewModel());
                    }
                }
            }
            else if (provider == SocialNetwork.Facebook)
            {
                CredentialsFacebook credentialsFacebook = await FacebookAuthenticationAgent.Current.Finalize(args);

                if (await TryLoadAsyncInner(async () => await VPNServerAgent.Current.LoginFacebookAsync(credentialsFacebook)))
                {
                    AppViewModel.Current.ClearNavigationStack();
                    await AppViewModel.Current.NavigateToViewModel(new MainPageViewModel());
                }

            }

            IsLoadingActive = false;
        }

        public async void FinalizeAppAuthentication(string queryString)
        {
            IsLoadingActive = true;
            if (await TryLoadAsyncInner(async () =>
                            await FacebookAuthenticationAgent.Current.AuthenticationFromAppReceivedAsync(queryString)))
            {
                var facebookCredentials = AutoLoginAgent.Current.CredentialsFacebook;
                if (await TryLoadAsyncInner(async () => await VPNServerAgent.Current.LoginFacebookAsync(
                    facebookCredentials)))
                {
                    AppViewModel.Current.ClearNavigationStack();
                    await AppViewModel.Current.NavigateToViewModel(new MainPageViewModel());
                }
            }

            IsLoadingActive = false;
        }

        protected override async Task<bool> LoadAsync()
        {
            LoadLocaliztion();

            if(!await TryLoadAsyncInner(async () => await AutoLoginAgent.AutoLogin()))
            {
                if (IsWasProblemsWithConnection)
                {
                    return false;
                }
                else
                {
                    AutoLoginAgent.Current.CredentialsFacebook = null;
                    AutoLoginAgent.Current.CredentialsGoogle = null;
                    AutoLoginAgent.Current.CredentialsKeepSolidID = null;

                    CacheAgent.DeleteLoginCache();
                }
            }

            return true;
        }

        protected override async Task<bool> UpdateInnerAsync() { return await LoadAsync(); }
    }
}
