﻿using VPN.ViewModel.Items;
using System.Runtime.Serialization;
using System.Windows.Input;

namespace VPN.ViewModel
{
    [DataContract]
    public abstract class BaseTilesPageViewModel : PageViewModel
    {
        private ICommand _tileCommand;

        [IgnoreDataMember]
        public ICommand TileCommand {
            get { return GetCommand(ref _tileCommand, TileAction); }
        }

        protected virtual void TileAction(object clickedItem)
        {
            var tileItem = clickedItem as TileItemViewModel;
            if (tileItem != null && tileItem.TileCommand != null)
                tileItem.TileCommand.Execute(clickedItem);
        }
    }
}
