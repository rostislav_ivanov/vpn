﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Cryptography;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.Web.Http;
using Windows.Web.Http.Headers;
using MetroLab.Common;
using System.Windows.Input;
using VPN.Model.SocialNetworks;
using VPN.ViewModel;
using VPN.Model;
using VPN.ViewModel.Http;

namespace VPN.ViewModel.Pages
{
    [DataContract]
    public class LoginKeepSolidIDPageViewModel : PageViewModel
    {
        public string EmailTextBoxPlaceHolder { get { return Localization.LocalizedResources.GetLocalizedString("S_EMAIL_TEMPLATE"); } }
        public string LoginKeepSolidIdPageHeader { get { return Localization.LocalizedResources.GetLocalizedString("S_LOGIN_TITLE").ToUpper(); } }
        public string PasswordTextBoxHeader { get { return Localization.LocalizedResources.GetLocalizedString("S_PASSWORD_PLACE"); } }
        public string EmailTextBoxHeader { get { return Localization.LocalizedResources.GetLocalizedString("S_EMAIL_PLACE"); } }
        public string ForgotPasswordText { get { return Localization.LocalizedResources.GetLocalizedString("S_FORGOT_PASS"); } }
        public string LoginButtonText { get { return Localization.LocalizedResources.GetLocalizedString("S_LOGIN").ToLower(); } }
        
        private string _email;
        [DataMember]
        public string Email {
            get { return _email; }
            set
            {
                SetProperty(ref _email, value);
            }
        }

        private string _password;
        [DataMember]
        public string Password
        {
            get { return _password; }
            set
            {
                SetProperty(ref _password, value);
            }
        }

        private bool _areButtonsDisabled;
        public bool AreButtonsDisabled
        {
            get { return _areButtonsDisabled; }
            set { SetProperty(ref _areButtonsDisabled, value); }
        }

        private ICommand _loginCommand;
        public ICommand LoginCommand 
        { 
            get 
            { 
                return GetCommand(ref _loginCommand, async o =>
                {
                    if (AreButtonsDisabled) return;
                    AreButtonsDisabled = true;
                    if (string.IsNullOrEmpty(Email) || !Email.Contains("@"))
                    {
                        RunLoadFailed(Localization.LocalizedResources.GetLocalizedString("S_INVALID_EMAIL"),
                        true, null, Localization.LocalizedResources.GetLocalizedString("S_ERROR"));
                    }
                    else if (string.IsNullOrEmpty(Password))
                    {
                        RunLoadFailed(Localization.LocalizedResources.GetLocalizedString("S_NO_VALID_PASSWORD"),
                        true, null, Localization.LocalizedResources.GetLocalizedString("S_ERROR"));
                    }
                    else
                    {
                        if (await TryLoadAsyncInner(async () => await VPNServerAgent.Current.LoginAsync(_email, _password, false)))
                        {
                            AutoLoginAgent.Current.CredentialsKeepSolidID = new CredentialsKeepSolidID()
                            {
                                Email = Email,
                                Password = Password,
                            };

                            AppViewModel.Current.ClearNavigationStack();
                            await AppViewModel.Current.NavigateToViewModel(new MainPageViewModel());
                        }
                    }
                    AreButtonsDisabled = false;
                }); 
            }
       }

        private ICommand _remindPasswordCommand;
        public ICommand RemindPasswordCommand
        {
            get
            {
                return GetCommand(ref _remindPasswordCommand, async o =>
                {
                    if (AreButtonsDisabled) return;
                    AreButtonsDisabled = true;

                    if (string.IsNullOrEmpty(Email) || !Email.Contains("@"))
                    {
                        RunLoadFailed(Localization.LocalizedResources.GetLocalizedString("S_INVALID_EMAIL"),
                            true, null, Localization.LocalizedResources.GetLocalizedString("S_ERROR"));
                    }
                    else
                    {
                        if (await TryLoadAsyncInner(async () => await VPNServerAgent.Current.RemindPassword(_email)))
                        {
                            await ShowDialogAsync(Localization.LocalizedResources.GetLocalizedString("S_RECOVERY_MAIL_SENT").Replace("%s", _email),
                                Localization.LocalizedResources.GetLocalizedString("S_WARNING"), null, DialogOkButtonTitle);
                        }
                    }

                    AreButtonsDisabled = false;
                });
            }
        }

        protected override Task<CacheState> LoadFromCacheAsync()
        {
            string email = AutoLoginAgent.Current.GetKeepSolidEmail();
            if (email != null)
            {
                Email = email;
            }
            return base.LoadFromCacheAsync();
        }
    }
}
