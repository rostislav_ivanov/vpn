﻿using System.Runtime.Serialization;
using System.Windows.Input;
using Windows.UI.Popups;
using MetroLab.Common;
using VPN.Model.SocialNetworks;
using VPN.ViewModel.Http;

namespace VPN.ViewModel.Pages
{
    [DataContract]
    public class RegistrationPageViewModel : PageViewModel
    {
        public string EmailTextBoxPlaceHolder { get { return Localization.LocalizedResources.GetLocalizedString("S_EMAIL_TEMPLATE"); } }
        public string RegistrationPageHeader { get { return Localization.LocalizedResources.GetLocalizedString("S_SIGN_CREATE").ToUpper(); } }
        public string PasswordTextBoxHeader { get { return Localization.LocalizedResources.GetLocalizedString("S_PASSWORD_PLACE"); } }
        public string EmailTextBoxHeader { get { return Localization.LocalizedResources.GetLocalizedString("S_EMAIL_PLACE"); } }
        public string RegisterButtonText { get { return Localization.LocalizedResources.GetLocalizedString("S_REGISTER").ToLower(); } }

        private string _email;
        [DataMember]
        public string Email
        {
            get { return _email; }
            set
            {
                SetProperty(ref _email, value);
            }
        }

        private string _password;
        [DataMember]
        public string Password
        {
            get { return _password; }
            set
            {
                SetProperty(ref _password, value);
            }
        }

        private bool _isLoadingEnabled;
        public bool IsLoadingEnabled
        {
            get { return _isLoadingEnabled; }
            set { SetProperty(ref _isLoadingEnabled, value); }
        }

        private ICommand _registerCommand;
        public ICommand RegisterCommand
        {
            get
            {
                return GetCommand(ref _registerCommand, async o =>
                {
                    IsLoadingEnabled = true;
                    if (string.IsNullOrEmpty(Email) || !Email.Contains("@"))
                    {
                        RunLoadFailed(Localization.LocalizedResources.GetLocalizedString("S_INVALID_EMAIL"),
                        true, null, Localization.LocalizedResources.GetLocalizedString("S_ERROR"));
                    }
                    else if (string.IsNullOrEmpty(Password))
                    {
                        RunLoadFailed(Localization.LocalizedResources.GetLocalizedString("S_NO_VALID_PASSWORD"),
                        true, null, Localization.LocalizedResources.GetLocalizedString("S_ERROR"));
                    }
                    else if (Password.Length < 6)
                    {
                        RunLoadFailed(Localization.LocalizedResources.GetLocalizedString("S_SMALL_PASS_ALERT"),
                        true, null, Localization.LocalizedResources.GetLocalizedString("S_ERROR"));
                    }
                    else
                    {
                        if (await TryLoadAsyncInner(async () => await VPNServerAgent.Current.RegisterAsync(_email, _password)))
                        {
                            AutoLoginAgent.Current.CredentialsKeepSolidID = new CredentialsKeepSolidID()
                            {
                                Email = Email,
                                Password = Password,
                            };

                            AppViewModel.Current.ClearNavigationStack();
                            await AppViewModel.Current.NavigateToViewModel(new MainPageViewModel());
                        }
                    }
                    IsLoadingEnabled = false;
                });
            }
        }
    }
}
