﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Windows.ApplicationModel.DataTransfer;
using Windows.UI.Popups;
using MetroLab.Common;
using System.Windows.Input;
using VPN.ViewModel.Http;
using VPN.ViewModel.Items;

namespace VPN.ViewModel.Pages
{
    [DataContract]
    public class SettingsPageViewModel : BaseTilesPageViewModel
    {
        public string SettingsHeaderText { get { return Localization.LocalizedResources.GetLocalizedString("action_settings").ToUpper(); } }
        public string ChangePasswordCommandButtonText { get { return Localization.LocalizedResources.GetLocalizedString("S_CHANGE_PASSWORD_BTN").ToLower(); } }
        public string LogoutButtonText { get { return Localization.LocalizedResources.GetLocalizedString("S_LOGOUT_BTN").ToLower(); } }
        public string AboutHeaderText { get { return Localization.LocalizedResources.GetLocalizedString("WinPhoneAboutHeader").ToLower(); } }
        public string ProfileHeaderText { get { return Localization.LocalizedResources.GetLocalizedString("WinPhoneProfileHeader").ToLower(); } }
        public string MakeAppBetterTitleText { get { return Localization.LocalizedResources.GetLocalizedString("WinPhoneMakeAppBetterTitle"); } }
        public string InformationTitleText { get { return Localization.LocalizedResources.GetLocalizedString("S_INFO"); } }
        public string FAQTitleText { get { return Localization.LocalizedResources.GetLocalizedString("S_FAQ"); } }
        public string PrivacyPolicyText { get { return Localization.LocalizedResources.GetLocalizedString("WinPhonePrivacyPolicyTitle"); } }

        public string SocialChanelsTitleText { get { return Localization.LocalizedResources.GetLocalizedString("WinPhoneSocialChanelsTitle"); } }

        public string FacebookLinkText { get { return Localization.LocalizedResources.GetLocalizedString("S_FOLLOW_FACEBOOK"); } }

        public string TwitterLinkText { get { return Localization.LocalizedResources.GetLocalizedString("S_FOLLOW_TWITTER"); } }
        
        private string _email;
        [DataMember]
        public string UserEmail
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }

        private string _url;
        public string Url {
            get { return _url; }
            set { SetProperty(ref _url, value); }
        }

        public bool IsUserLoggedUsingKeepSolidAccount
        {
            get { return AutoLoginAgent.Current.CredentialsKeepSolidID != null; }
        }

        private ICommand _loginCommand;
        public ICommand LogoutCommand { get
        {
            return GetCommand(ref _loginCommand, async o =>
            {
                //ToDo: our messagebox
                MessageDialog msg = new MessageDialog(
                    Localization.LocalizedResources.GetLocalizedString("S_LOGOUT_CONFIRMATION"),
                    Localization.LocalizedResources.GetLocalizedString("S_INFORMATION_WARNING"));

                msg.Commands.Add(new UICommand(Localization.LocalizedResources.GetLocalizedString("S_OK"), new UICommandInvokedHandler(CommandHandlers)));
                msg.Commands.Add(new UICommand(Localization.LocalizedResources.GetLocalizedString("S_CANCEL"), new UICommandInvokedHandler(CommandHandlers)));

                await msg.ShowAsync();
            });}
        }

        public async void CommandHandlers(IUICommand commandLabel)
        {
            var Actions = commandLabel.Label;
            if (Actions == Localization.LocalizedResources.GetLocalizedString("S_OK"))
            {
                await AutoLoginAgent.Current.Logout();

                AppViewModel.Current.ClearNavigationStack();
                AppViewModel.Current.GoHome();
            }
        }

        private ICommand _changePasswordCommand;
        public ICommand ChangePasswordCommand
        {
            get
            {
                return GetCommand(ref _changePasswordCommand, async o =>
                {
                    if (AutoLoginAgent.Current.CredentialsKeepSolidID != null)
                    {
                        await
                            AppViewModel.Current.NavigateToViewModel(new ChangePasswordPageViewModel()
                            {
                                Email = AutoLoginAgent.Current.CredentialsKeepSolidID.Email
                            });
                    }
                });
            }
        }

        private ICommand _FAQCommand;
        public ICommand FAQCommand
        {
            get
            {
                return GetCommand(ref _FAQCommand, async o =>
                {
                    if (Constants.GetCurrentLanguageNameTwoLetter().Equals("ru"))
                    {
                        await Windows.System.Launcher.LaunchUriAsync(new Uri("https://www.vpnunlimitedapp.com/ru/help"));
                    }
                    else
                    {
                        await Windows.System.Launcher.LaunchUriAsync(new Uri("https://www.vpnunlimitedapp.com/help"));
                    }
                });
            }
        }

        private ICommand _userCabinetCommand;
        public ICommand UserCabinetCommand
        {
            get
            {
                return GetCommand(ref _userCabinetCommand, async o =>
                    await Windows.System.Launcher.LaunchUriAsync(new Uri(AutoLoginAgent.Current.CabinetUrl)));
            }
        }

        private ICommand _facebookAppCommand;
        public ICommand FacebookAppCommand
        {
            get
            {
                return GetCommand(ref _facebookAppCommand, async o =>
                    await Windows.System.Launcher.LaunchUriAsync(new Uri("https://www.facebook.com/vpnunlimitedapp")));
            }
        }

        private ICommand _twitterAppCommand;
        public ICommand TwitterAppCommand
        {
            get
            {
                return GetCommand(ref _twitterAppCommand, async o =>
                    await Windows.System.Launcher.LaunchUriAsync(new Uri("https://twitter.com/vpnunlimited")));
            }
        }

        private ICommand _privacyPolicyCommand;
        public ICommand PrivacyPolicyCommand
        {
            get
            {
                return GetCommand(ref _privacyPolicyCommand, async o =>
                    await Windows.System.Launcher.LaunchUriAsync(new Uri("https://www.keepsolid.com/privacy")));
            }
        }

        protected override async Task<bool> LoadAsync()
        {
            UserEmail = AutoLoginAgent.Current.UserEmail;
            return true;//await base.LoadAsync();
        }

        public string SupportString { get { return Localization.LocalizedResources.GetLocalizedString("S_CONTACT_SUPPORT"); } }
        public string RateUsString { get { return Localization.LocalizedResources.GetLocalizedString("S_RATE_US"); } }
        public string ApplicationOverviewString { get { return Localization.LocalizedResources.GetLocalizedString("S_QUICK_TOUR_LABEL"); } }
        public string ShareString { get { return Localization.LocalizedResources.GetLocalizedString("S_SHARE"); } }

        public override async Task RequestData(DataTransferManager manager, DataRequestedEventArgs args)
        {
            var requestData = args.Request.Data;
            var requestProperties = requestData.Properties;
            
            requestProperties.ApplicationName = Localization.LocalizedResources.GetLocalizedString("app_name");
            requestProperties.Title = Localization.LocalizedResources.GetLocalizedString("WinPhoneShareTitle");
            requestData.SetText(Localization.LocalizedResources.GetLocalizedString("S_SHARE_FACEBOOK_TEXT"));
        }
    }
}
