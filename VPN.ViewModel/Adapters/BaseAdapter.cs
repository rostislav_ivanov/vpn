﻿using System;
using System.Threading.Tasks;

namespace VPN.ViewModel.Adapters
{
    public abstract class BaseAdapter<TInput, TOutput>
        where TInput : class
        where TOutput : class, new()
    {
        public TOutput Convert(TInput input)
        {
            var output = new TOutput();
            Init(input, output);
            return output;
        }

        public abstract void Init(TInput input, TOutput output);
        public virtual async Task InitAsync(TInput input, TOutput output) { }
    }
}
