﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VPN.ViewModel.Items;
using Windows.ApplicationModel.DataTransfer;
using Windows.ApplicationModel.Store;

namespace VPN.ViewModel.Adapters
{
    public class StoreInAppToPropositionViewModelAdapter : BaseAdapter<ProductListing, PropositionViewModel>
    {
        public override void Init(ProductListing input, PropositionViewModel output)
        {
            output.Price = input.FormattedPrice;
            output.ID = input.ProductId;
            switch(input.ProductId)
            {
                case "com.KeepSolid.VPN.3years.WinPhone":
                    output.Period = Localization.LocalizedResources.GetLocalizedString("S_PURCHASE_SUBTITLE_3_YEAR");
                    output.Discount = 70;
                    output.DaysCount = 1092;
                    break;
                case "com.KeepSolid.VPN.1year.WinPhone.Consumable":
                    output.Period = Localization.LocalizedResources.GetLocalizedString("S_PURCHASE_SUBTITLE_1_YEAR");
                    output.Discount = 45;
                    output.DaysCount = 364;
                    break;
                case "com.KeepSolid.VPN.3months.WinPhone.Consumable":
                    output.Period = Localization.LocalizedResources.GetLocalizedString("S_PURCHASE_SUBTITLE_3_MONTH");
                    output.Discount = 34;
                    output.DaysCount = 90;
                    break;
                case "com.KeepSolid.VPN.1month.WinPhone.Consumable":
                    output.Period = Localization.LocalizedResources.GetLocalizedString("S_PURCHASE_SUBTITLE_1_MONTH");
                    output.Discount = 0;
                    output.DaysCount = 30;
                    break;
                case "com.KeepSolid.VPN.10days.WinPhone":
                    output.Period = Localization.LocalizedResources.GetLocalizedString("S_PURCHASE_SUBTITLE_10_DAYS");
                    output.Discount = 0;
                    output.DaysCount = 10;
                    break;
            }

        }
    }
}
