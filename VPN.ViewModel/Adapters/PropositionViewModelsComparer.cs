﻿using System;
using System.Collections;
using System.Collections.Generic;
using VPN.ViewModel.Items;

namespace VPN.ViewModel.Adapters
{
    public class PropositionViewModelsComparer : IComparer<PropositionViewModel>
    {
        int IComparer<PropositionViewModel>.Compare(PropositionViewModel x, PropositionViewModel y)
        {
            return x.DaysCount > y.DaysCount ? -1 : 1;
        }
    }
}
