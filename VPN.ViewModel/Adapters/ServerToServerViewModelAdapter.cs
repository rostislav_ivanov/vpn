﻿using System;
using VPN.Model;
using VPN.ViewModel.Items;

namespace VPN.ViewModel.Adapters
{
    public class ServerToServerViewModelAdapter : BaseAdapter<Server,ServerViewModel>
    {
        static string PathToFlagsIcons = 
            "https://39e5a1dabc4707021dd5-935414efe171f56dbde59246d9507091.ssl.cf1.rackcdn.com/winphone/{0}.png";

        static string PathToOptimalIcon = "/Assets/Icons/optimal.jpg";

        public override void Init(Server input, ServerViewModel output)
        {
            if(input.CountryCode != null)
                output.FlagIcon = String.Format(PathToFlagsIcons, input.CountryCode);
            else
                output.FlagIcon = PathToOptimalIcon;

            output.Country = input.Name;
            output.City = input.Description;
            output.Region = input.Region;

        }
    }
}
