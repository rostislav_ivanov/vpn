﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Windows.Input;
using MetroLab.Common;
using VPN.Model;
using VPN.Model.SocialNetworks.Exceptions;
using VPN.ViewModel.Http;

namespace VPN.ViewModel
{
    [DataContract]
    public class VPNDataLoadViewModel : DataLoadViewModel
    {
        private ICommand _updateCommand;
        [IgnoreDataMember]
        public ICommand UpdateCommand
        {
            get { return GetCommand(ref _updateCommand, o => UpdateAsync()); }
        }

        protected void LoadLocaliztion()
        {
            LoadingMessage = Localization.LocalizedResources.GetLocalizedString("WinPhoneLoading");
        }

        protected override async Task<bool> TryLoadAsyncInner(Func<Task<bool>> loadAction)
        {
            try
            {
                if (_isContentActual) return true;
                IsWorkingOffline = false;
                IsWasProblemsWithConnection = false;
                var isLoadedSuccessfully = await loadAction();
                return isLoadedSuccessfully;
            }
            catch (WebException ex)
            {
                IsWasProblemsWithConnection = true;
                if (ex.Response == null && ((byte) ex.Status == 1 || (byte) ex.Status == 2))
                    RunLoadFailed(Localization.LocalizedResources.GetLocalizedString("S_INTERNET_PROBLEM"),
                        true, null, Localization.LocalizedResources.GetLocalizedString("S_ERROR"));
                else
                    RunLoadFailed(Localization.LocalizedResources.GetLocalizedString("S_REQUEST_FAILED"),
                        true, null, Localization.LocalizedResources.GetLocalizedString("S_ERROR"), true);

                return false;
            }
            catch (VPNResponceException ex)
            {
                string errorDescription;
                bool isNotAutoLogin = false;

                int vpnErrorCode;
                //if (ex.Message.Contains(" NotAutoLogin")) isNotAutoLogin = true;

                int.TryParse(ex.Message, out vpnErrorCode);

                switch (vpnErrorCode)
                {
                    case 501:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_INVALID_REQUEST_SERV");
                        break;
                    case 502:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_INVALID_PARAMS_SERV");
                        break;
                    case 503:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_INVALID_SESSION_SERV");
                        break;
                    case 505:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_INVALID_DB_CONNECTION_SERV");
                        break;
                    case 305:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_ENCRYPTYON_FAILURE_SERV");
                        break;
                    case 306:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_INVALID_URL_SERV");
                        break;
                    case 342:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_APPLESERVER_NOT_RESPONSE_SERV");
                        break;
                    case 343:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_INVALID_PURCHASE_SERV");
                        break;
                    case 504:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_INVALID_USERNAME_SERV");
                        break;
                    case 301:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_ALREADY_CONFIRMED_SERV");
                        break;
                    case 302:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_INVALID_USERNAME_PASSWORD_SERV");
                        break;
                    case 303:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_LOGIN_ATTEMPTS_EXCEEDED_SERV").Replace("%s", AutoLoginAgent.Current.UserEmail ?? "");
                        break;
                    case 304:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_INVALID_REFCODE_SERV");
                        break;
                    case 308:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_TOO_MUCH_REGS");
                        break;
                    case 341:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_PURCHASE_ALREADY_EXISTS_SERV");
                        break;
                    case 361:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_TOO_MUCH_MAIL");
                        break;
                    case 309:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_TOO_MANY_DEVICES");
                        break;
                    case 310:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_USER_ALREADY_EXIST");
                        break;
                    case 311:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_NEED_CONFIRM_EMAIL");
                        break;
                    case 323:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_ACCOUNT_CANT_BE_USED");
                        break;
                    case 313:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_USER_BANNED");
                        break;
                    case 314:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_USER_DELETED");
                        break;
                    case 327:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_PROFILE_IS_INCORRECT");
                        break;
                    default:
                        errorDescription = Localization.LocalizedResources.GetLocalizedString("S_APPLESERVER_NOT_RESPONSE_SERV");
                        break;
                }

                RunLoadFailed(errorDescription.Replace("%s", vpnErrorCode.ToString()), true, null, Localization.LocalizedResources.GetLocalizedString("S_ERROR"), true);

                /*if (!isNotAutoLogin && vpnErrorCode == 302 || vpnErrorCode == 308 || vpnErrorCode == 309 || vpnErrorCode == 311 ||  
                    vpnErrorCode == 303 || vpnErrorCode == 313 || vpnErrorCode == 323 || vpnErrorCode == 314 || vpnErrorCode == 327)
                {
                    AppViewModel.Current.ClearNavigationStack();
                    AppViewModel.Current.GoHome();
                }*/

                return false;
            }
            catch (FacebookAppReturnInvalidDataException)
            {
                //ToDo: Run web authentification
                return false;
            }
            catch (FacebookOAuthException ex)
            {
//#if DEBUG
                RunLoadFailed(ex.Message,
                    true, null, Localization.LocalizedResources.GetLocalizedString("S_ERROR"), true);
//#else
               //RunLoadFailed(Localization.LocalizedResources.GetLocalizedString("S_AUTH_SOCIAL_ERROR"),
               //     true, null, Localization.LocalizedResources.GetLocalizedString("S_ERROR"), true);
//#endif
                return false;
            }
            catch (HttpRequestException hrex)
            {
                IsWasProblemsWithConnection = true;
                var webException = hrex.InnerException as WebException;
                if (webException != null && webException.Response == null &&
                    ((byte) webException.Status == 1 || (byte) webException.Status == 2))
                    RunLoadFailed(Localization.LocalizedResources.GetLocalizedString("S_INTERNET_PROBLEM"),
                        true, null, Localization.LocalizedResources.GetLocalizedString("S_ERROR"));
                else
                    RunLoadFailed(Localization.LocalizedResources.GetLocalizedString("S_REQUEST_FAILED"),
                        true, null, Localization.LocalizedResources.GetLocalizedString("S_ERROR"), true);

                return false;
            }
            catch (Exception ex)
            {
                if (!NetworkInterface.GetIsNetworkAvailable())
                {
                    IsWasProblemsWithConnection = true;

                    RunLoadFailed(Localization.LocalizedResources.GetLocalizedString("S_INTERNET_PROBLEM"),
                        true, null, Localization.LocalizedResources.GetLocalizedString("S_ERROR"));

                    return false;
                }

                if (ex.HResult == -2147012816 || ex.HResult == -2147012889 || ex.HResult == -2147012879
                    || ex.HResult == -2147012865 || ex.HResult == -2147012867)
                {
                    //0x80072EE7  -2147012889  ERROR_INTERNET_NAME_NOT_RESOLVED (Suggests a networking connection problem, perhaps DNS or DHCP is not working)
                    //0x80072EF1  -2147012879  ERROR_INTERNET_OPERATION_CANCELLED
                    //0x80072EFF  -2147012865
                    //0x80072EFD  -2147012867
                    IsWasProblemsWithConnection = true;

                    RunLoadFailed(Localization.LocalizedResources.GetLocalizedString("S_REQUEST_FAILED"),
                        true, null, Localization.LocalizedResources.GetLocalizedString("S_ERROR"), true);

                    return false;
                }

#if DEBUG
                var message = ex.ToString();
                RunLoadFailed(message);
#else
                
                IsWasProblemsWithConnection = true;

                RunLoadFailed(Localization.LocalizedResources.GetLocalizedString("S_REQUEST_FAILED"),
                    true, null, Localization.LocalizedResources.GetLocalizedString("S_ERROR"), true);
#endif
                return false;
            }
        }
    }
}
