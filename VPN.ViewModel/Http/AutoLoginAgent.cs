﻿using System;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Windows.Storage;
using VPN.Model;
using VPN.Model.SocialNetworks;

namespace VPN.ViewModel.Http
{
    [DataContract]
    public class AutoLoginAgent
    {
        public static AutoLoginAgent Current = new AutoLoginAgent();

        [DataMember]
        public string UserEmail { get; set; }
        [DataMember]
        public string Session { get; set; }
        [DataMember]
        public string CabinetUrl { get; set; }

        private CredentialsGoogle _сredentialsGoogle;
        [IgnoreDataMember]
        public CredentialsGoogle CredentialsGoogle
        {
            get
            {
                if (_сredentialsGoogle == null)
                {
                    _сredentialsGoogle = CacheAgent.LoadFromLocalSettings<CredentialsGoogle>(CacheAgent.GoogleCredentialsLocalSettingsName);

                    if (_сredentialsGoogle != null)
                    {
                        string refreshTokenDecrypted = CacheAgent.Decrypt(_сredentialsGoogle.RefreshToken, _сredentialsGoogle.Email, _сredentialsGoogle.Key);
                        _сredentialsGoogle.RefreshToken = refreshTokenDecrypted;
                    }
                }

                return _сredentialsGoogle;
            }
            set
            {
                _сredentialsGoogle = value;
                if (value != null)
                {
                    string key;
                    string refreshTokenOriginal = value.AccessToken;
                    string refreshTokenEncrypted = CacheAgent.Encrypt(value.RefreshToken, value.Email, out key);
                    value.RefreshToken = refreshTokenEncrypted;
                    value.Key = key;
                    CacheAgent.SaveToLocalSettings(CacheAgent.GoogleCredentialsLocalSettingsName, value);

                    value.AccessToken = refreshTokenOriginal;
                }
            }
        }

        private CredentialsFacebook _сredentialsFacebook;
        [IgnoreDataMember]
        public CredentialsFacebook CredentialsFacebook
        {
            get
            {
                if (_сredentialsFacebook == null)
                {
                    _сredentialsFacebook = CacheAgent.LoadFromLocalSettings<CredentialsFacebook>(CacheAgent.FacebookCredentialsLocalSettingsName);

                    if (_сredentialsFacebook != null)
                    {
                        string refreshTokenDecrypted = CacheAgent.Decrypt(_сredentialsFacebook.AccessToken, _сredentialsFacebook.Email, _сredentialsFacebook.Key);
                        _сredentialsFacebook.AccessToken = refreshTokenDecrypted;
                    }
                }

                return _сredentialsFacebook;
            }
            set
            {
                _сredentialsFacebook = value;
                if (value != null)
                {
                    string key;
                    string refreshTokenOriginal = value.AccessToken;
                    string refreshTokenEncrypted = CacheAgent.Encrypt(value.AccessToken, value.Email, out key);
                    value.AccessToken = refreshTokenEncrypted;
                    value.Key = key;
                    CacheAgent.SaveToLocalSettings(CacheAgent.FacebookCredentialsLocalSettingsName, value);

                    value.AccessToken = refreshTokenOriginal;
                }
            }
        }

        private CredentialsKeepSolidID _сredentialsKeepSolidID;
        [IgnoreDataMember]
        public CredentialsKeepSolidID CredentialsKeepSolidID
        {
            get
            {
                if (_сredentialsKeepSolidID == null)
                {
                    _сredentialsKeepSolidID = CacheAgent.LoadFromLocalSettings<CredentialsKeepSolidID>(CacheAgent.KeepSolidCredentialsLocalSettingsName);

                    if (_сredentialsKeepSolidID != null)
                    {
                        string refreshTokenDecrypted = CacheAgent.Decrypt(_сredentialsKeepSolidID.Password, _сredentialsKeepSolidID.Email, _сredentialsKeepSolidID.Key);
                        _сredentialsKeepSolidID.Password = refreshTokenDecrypted;
                    }
                }

                return _сredentialsKeepSolidID;
            }
            set
            {
                _сredentialsKeepSolidID = value;
                if (value != null)
                {
                    string key;
                    string refreshTokenOriginal = value.Password;
                    string refreshTokenEncrypted = CacheAgent.Encrypt(value.Password, value.Email, out key);
                    value.Password = refreshTokenEncrypted;
                    value.Key = key;
                    CacheAgent.SaveToLocalSettings(CacheAgent.KeepSolidCredentialsLocalSettingsName, value);

                    value.Password = refreshTokenOriginal;
                }
            }
        }

        public static async Task<bool> AutoLogin()
        {
            var googleCredentials = Current.CredentialsGoogle;
            if (googleCredentials != null)
            {
                var googleCredentialsWithAccessToken = await GoogleAuthenticationAgent.RefreshAccessToken(googleCredentials);
                await VPNServerAgent.Current.LoginGoogleAsync(googleCredentialsWithAccessToken);
                return true;
            }

            var facebookCredentials = Current.CredentialsFacebook;
            if (facebookCredentials != null && facebookCredentials.RealExpirationDate > DateTime.UtcNow)
            {
                await VPNServerAgent.Current.LoginFacebookAsync(facebookCredentials);
                return true;
            }

            var keepSolidCredentials = Current.CredentialsKeepSolidID;
            if (keepSolidCredentials != null)
            {
                await VPNServerAgent.Current.LoginAsync(keepSolidCredentials.Email, keepSolidCredentials.Password, true);
                return true;
            }

            return false;
        }

        public async Task Logout()
        {
            CredentialsFacebook = null;
            CredentialsGoogle = null;
            CredentialsKeepSolidID = null;

            CacheAgent.DeleteLoginCache();
            await VPNServerAgent.Current.LogoutAsync();

            CabinetUrl = null;
            Session = null;
        }

        public void SaveKeepSolidEmail(string email)
        {
            UserEmail = email;
            ApplicationData.Current.LocalSettings.Values["KeepSolidEmail"] = email;
        }

        public string GetKeepSolidEmail()
        {
            return (string)ApplicationData.Current.LocalSettings.Values["KeepSolidEmail"];
        }
    }
}
