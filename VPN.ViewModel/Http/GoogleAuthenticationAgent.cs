﻿using System;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Authentication.Web;
using Windows.ApplicationModel.Activation;
using Windows.Web.Http;
using VPN.Model.SocialNetworks;
using UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding;

namespace VPN.ViewModel.Http
{
    public class GoogleAuthenticationAgent
    {
        public readonly static GoogleAuthenticationAgent Current = new GoogleAuthenticationAgent();
        public const string GoogleServiceProviderName = "Google";

        public async void LoginAsync()
        {
            var googleUrl = new StringBuilder();
            googleUrl.Append("https://accounts.google.com/o/oauth2/auth?client_id=");
            googleUrl.Append(Uri.EscapeDataString(Constants.GoogleClientId));
            googleUrl.Append("&scope=openid%20email%20profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me");
            googleUrl.Append("&redirect_uri=");
            googleUrl.Append(Uri.EscapeDataString(Constants.GoogleCallbackUrl));
            googleUrl.Append("&state=foobar");
            googleUrl.Append("&response_type=code");

            var startUri = new Uri(googleUrl.ToString());

            WebAuthenticationBroker.AuthenticateAndContinue(startUri, new Uri(Constants.GoogleCallbackUrl), null, WebAuthenticationOptions.None);
        }

        private string GetCode(string webAuthResultResponseData)
        {
            // Success code=4/izytpEU6PjuO5KKPNWSB4LK3FU1c
            var split = webAuthResultResponseData.Split('&');

            return split.FirstOrDefault(value => value.Contains("code"));
        }

        public async Task<bool> GetSession(WebAuthenticationBrokerContinuationEventArgs args)
        {
            WebAuthenticationResult result = args.WebAuthenticationResult;

            if (result.ResponseStatus == WebAuthenticationStatus.Success)
            {
                var code = GetCode(result.ResponseData);
                CredentialsGoogle credentials = await GetToken(code);

                credentials.Code = code;
                credentials.ServiceProvider = GoogleServiceProviderName;

                AutoLoginAgent.Current.CredentialsGoogle = credentials;

                return true;
            }
            if (result.ResponseStatus == WebAuthenticationStatus.ErrorHttp)
            {
                throw new WebException();
            }
            if (result.ResponseStatus == WebAuthenticationStatus.UserCancel)
            {
                return false;
            }

            return false;
        }

        private static async Task<CredentialsGoogle> GetToken(string code)
        {
            const string TokenUrl = "https://accounts.google.com/o/oauth2/token";

            var body = new StringBuilder();
            body.Append(code);
            body.Append("&client_id=");
            body.Append(Uri.EscapeDataString(Constants.GoogleClientId));
            body.Append("&client_secret=");
            body.Append(Uri.EscapeDataString(Constants.GoogleClientSecret));
            body.Append("&redirect_uri=");
            body.Append(Uri.EscapeDataString(Constants.GoogleCallbackUrl));
            body.Append("&grant_type=authorization_code");

            var request = new Windows.Web.Http.HttpRequestMessage(Windows.Web.Http.HttpMethod.Post, new Uri(TokenUrl, UriKind.Absolute))
            {
                Content = new HttpStringContent(body.ToString(), UnicodeEncoding.Utf8, "application/x-www-form-urlencoded"),
            };


            var httpClient = new Windows.Web.Http.HttpClient();
            var responseMessage = await httpClient.SendRequestAsync(request);

            responseMessage.EnsureSuccessStatusCode();

            var dataStream = await responseMessage.Content.ReadAsBufferAsync();

            var serializer = new DataContractJsonSerializer(typeof(CredentialsGoogle));
            var oauthcredentialsGoogle = (CredentialsGoogle)serializer.ReadObject(dataStream.AsStream());

            oauthcredentialsGoogle.Email = await GetEmail(oauthcredentialsGoogle.AccessToken);

            return oauthcredentialsGoogle;
        }

        private static async Task<string> GetEmail(string accessToken)
        {
            const string getEmailUrl = "https://www.googleapis.com/plus/v1/people/me?";

            
            var request = new Windows.Web.Http.HttpRequestMessage(Windows.Web.Http.HttpMethod.Get,
                new Uri(String.Concat(getEmailUrl, "access_token=", accessToken), UriKind.Absolute));


            var httpClient = new Windows.Web.Http.HttpClient();
            var responseMessage = await httpClient.SendRequestAsync(request);

            responseMessage.EnsureSuccessStatusCode();

            var dataStream = await responseMessage.Content.ReadAsBufferAsync();


            var serializer = new DataContractJsonSerializer(typeof(ResponceOnGetUserProfileGoogle));
            var responceOnGetUserProfileGoogle = (ResponceOnGetUserProfileGoogle) serializer.ReadObject(dataStream.AsStream());

            return responceOnGetUserProfileGoogle.EmailsList[0].Email;
        }


        public static async Task<CredentialsGoogle> RefreshAccessToken(CredentialsGoogle savedCredentials)
        {
            const string TokenUrl = "https://www.googleapis.com/oauth2/v3/token";

            var body = new StringBuilder();
            body.Append("refresh_token=");
            body.Append(Uri.EscapeDataString(savedCredentials.RefreshToken));
            body.Append("&client_id=");
            body.Append(Uri.EscapeDataString(Constants.GoogleClientId));
            body.Append("&client_secret=");
            body.Append(Uri.EscapeDataString(Constants.GoogleClientSecret));
            body.Append("&grant_type=refresh_token");

            var request = new Windows.Web.Http.HttpRequestMessage(Windows.Web.Http.HttpMethod.Post, new Uri(TokenUrl, UriKind.Absolute))
            {
                Content = new HttpStringContent(body.ToString(), UnicodeEncoding.Utf8, "application/x-www-form-urlencoded"),
            };


            var httpClient = new Windows.Web.Http.HttpClient();
            var responseMessage = await httpClient.SendRequestAsync(request);

            responseMessage.EnsureSuccessStatusCode();

            var dataStream = await responseMessage.Content.ReadAsBufferAsync();

            var serializer = new DataContractJsonSerializer(typeof(CredentialsGoogle));
            var oauthcredentialsWithNewAccessToken = (CredentialsGoogle)serializer.ReadObject(dataStream.AsStream());

            savedCredentials.AccessToken = oauthcredentialsWithNewAccessToken.AccessToken;
            savedCredentials.ExpiresDate = oauthcredentialsWithNewAccessToken.ExpiresDate;
            savedCredentials.TokenType = oauthcredentialsWithNewAccessToken.TokenType;

            return savedCredentials;
        }
    }
}
