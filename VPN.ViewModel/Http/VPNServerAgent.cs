﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.Web.Http;
using MetroLab.Common;
using VPN.Model;
using VPN.Model.SocialNetworks;

namespace VPN.ViewModel.Http
{
    public class VPNServerAgent
    {
        public readonly static VPNServerAgent Current = new VPNServerAgent();

        public const string VPNservice = "com.simplexsolutionsinc.vpnguard";
        public const string ApiLoginUrlString = "https://auth.simplexsolutionsinc.com/";
        public const string ApiUrlString = "https://api.vpnunlimitedapp.com/";
        public const string GoogleOauthServiceName = "googleplus";
        public const string FacebookOauthServiceName = "facebook";

        public ResponceOnAccountStatus AccountStatus;
        public ResponceOnServersList ServersList;
        public Config VPNConnectionConfig;

        public static string ToBase64(string str)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(str));
        }
        public async Task<bool> LoginAsync(string _email, string _password, bool isAutoLogin)
        {
            var multipartFormDataContent = new HttpMultipartFormDataContent();
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("login")), "action");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(_email)), "login");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(_password)), "password");
            AddParametersForRequestToVPN(multipartFormDataContent);

            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(ApiLoginUrlString, UriKind.Absolute))
            {
                Content = multipartFormDataContent
            };

            var httpClient = new HttpClient();
            var responseMessage = await httpClient.SendRequestAsync(request);

            responseMessage.EnsureSuccessStatusCode();
  
            var dataStream = await responseMessage.Content.ReadAsBufferAsync();
            var serializer = new DataContractJsonSerializer(typeof(ResponseOnLogin));
            var result = (ResponseOnLogin)serializer.ReadObject(dataStream.AsStream());

            if (result.Response != 200)
            {
                if (!isAutoLogin)
                {
                    throw new VPNResponceException(result.Response.ToString());
                }
                else
                {
                    await CheckIfLogoutIsNeededAsync(result.Response);
                    //throw new VPNResponceException(result.Response.ToString());
                }
            }

            AutoLoginAgent.Current.Session = result.Session;
            AutoLoginAgent.Current.CabinetUrl = result.UserInfo.CabinetUrl;
            AutoLoginAgent.Current.SaveKeepSolidEmail(_email);

            if (ApplicationData.Current.LocalSettings.Values["ShowVPNNotConnected"] == null)
            {
                ApplicationData.Current.LocalSettings.Values["FirstLoginTime"] = DateTime.Now.Ticks;
            }

            return true;
        }

        public async Task<bool> RegisterAsync(string _email, string _password)
        {
            var multipartFormDataContent = new HttpMultipartFormDataContent();
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("register")), "action");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(_email)), "login");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(_password)), "password");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("win_phone_purch")), "purch_type");
            AddParametersForRequestToVPN(multipartFormDataContent);

            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(ApiLoginUrlString, UriKind.Absolute))
            {
                Content = multipartFormDataContent
            };

            var httpClient = new HttpClient();
            var responseMessage = await httpClient.SendRequestAsync(request);

            responseMessage.EnsureSuccessStatusCode();

            var dataStream = await responseMessage.Content.ReadAsBufferAsync();
            var serializer = new DataContractJsonSerializer(typeof(ResponseOnLogin));
            var result = (ResponseOnLogin)serializer.ReadObject(dataStream.AsStream());

            if (result.Response != 200)
            {
                throw new VPNResponceException(result.Response.ToString());
            }
            else
            {
                return await LoginAsync(_email, _password, false);
            }
        }
        public async Task<bool> GetVPNServersListAsync()
        {
            if (AutoLoginAgent.Current.Session == null)
            {
                bool loginResult = await AutoLoginAgent.AutoLogin();

                if (loginResult == false) return false;
            }

            var multipartFormDataContent = new HttpMultipartFormDataContent();
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("vpn_servers")), "action");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(AutoLoginAgent.Current.Session)), "session");

            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(ApiUrlString, UriKind.Absolute))
            {
                Content = multipartFormDataContent
            };

            var httpClient = new HttpClient();
            var responseMessage = await httpClient.SendRequestAsync(request);

            responseMessage.EnsureSuccessStatusCode();
            
            var dataStream = await responseMessage.Content.ReadAsBufferAsync();
            var serializer = new DataContractJsonSerializer(typeof(ResponceOnServersList));
            var result = (ResponceOnServersList)serializer.ReadObject(dataStream.AsStream());

            if (result.Response != 200)
            {
                

                if (result.Response == 503)
                {
                    await AutoLoginAgent.AutoLogin();
                    return await GetVPNServersListAsync();
                }
                else
                {
                    throw new VPNResponceException(result.Response.ToString());
                }
            }

            ServersList = result;
            return true;
        }

        public async Task<bool> GetAccountAsync()
        {
            if (AutoLoginAgent.Current.Session == null)
            {
                bool loginResult = await AutoLoginAgent.AutoLogin();

                if (loginResult == false) return false;
            }

            var multipartFormDataContent = new HttpMultipartFormDataContent();
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("account_status")), "action");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(AutoLoginAgent.Current.Session)), "session");

            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(ApiUrlString, UriKind.Absolute))
            {
                Content = multipartFormDataContent
            };

            var httpClient = new HttpClient();
            var responseMessage = await httpClient.SendRequestAsync(request);

            responseMessage.EnsureSuccessStatusCode();
           
            var dataStream = await responseMessage.Content.ReadAsBufferAsync();
            var serializer = new DataContractJsonSerializer(typeof(ResponceOnAccountStatus));
            var result = (ResponceOnAccountStatus)serializer.ReadObject(dataStream.AsStream());

            if (result.Response != 200)
            {
                if (result.Response == 503)
                {
                    await AutoLoginAgent.AutoLogin();
                    return await GetAccountAsync();
                }
                else
                {
                    throw new VPNResponceException(result.Response.ToString());
                }
            }

            AccountStatus = result;
            return true;
        }

        public async Task<bool> GetConnectionCredentialsAsync(string region)
        {
            if (AutoLoginAgent.Current.Session == null)
            {
                bool loginResult = await AutoLoginAgent.AutoLogin();

                if (loginResult == false) return false;
            }

            var multipartFormDataContent = new HttpMultipartFormDataContent();
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("config_info")), "action");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(AutoLoginAgent.Current.Session)), "session");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("ipsec-psk")), "protocol");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(region)), "region");

            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(ApiUrlString, UriKind.Absolute))
            {
                Content = multipartFormDataContent
            };

            var httpClient = new HttpClient();
            var responseMessage = await httpClient.SendRequestAsync(request);

            responseMessage.EnsureSuccessStatusCode();

            var dataStream = await responseMessage.Content.ReadAsBufferAsync();
            var serializer = new DataContractJsonSerializer(typeof(ResponceOnConfigInfo));
            var result = (ResponceOnConfigInfo)serializer.ReadObject(dataStream.AsStream());

            if (result.Response != 200)
            {
                if (result.Response == 503)
                {
                    await AutoLoginAgent.AutoLogin();
                    return await GetConnectionCredentialsAsync(region);
                }
                else
                {
                    throw new VPNResponceException(result.Response.ToString()); 
                }
            }

            VPNConnectionConfig = result.Config;
            return true;
        }

        public async Task<bool> LoginFacebookAsync(CredentialsFacebook credentialsFacebook)
        {
            var serializer = new DataContractJsonSerializer(typeof(CredentialsFacebookVPNServer));
            string oauthcredentialsString;

            using (MemoryStream ms = new MemoryStream())
            {
                serializer.WriteObject(ms, new CredentialsFacebookVPNServer(credentialsFacebook));
                byte[] bytearray = ms.ToArray();
                oauthcredentialsString = Encoding.UTF8.GetString(bytearray, 0, bytearray.Count());
            }

            var multipartFormDataContent = new HttpMultipartFormDataContent();
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("loginsocial")), "action");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("2")), "social_login_version");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(credentialsFacebook.Email)), "login");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(FacebookOauthServiceName)), "oauthservice");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(oauthcredentialsString)), "oauthcredentials");
            AddParametersForRequestToVPN(multipartFormDataContent);

            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(ApiLoginUrlString, UriKind.Absolute))
            {
                Content = multipartFormDataContent
            };

            var httpClient = new HttpClient();
            var responseMessage = await httpClient.SendRequestAsync(request);

            var dataStream = await responseMessage.Content.ReadAsBufferAsync();
            serializer = new DataContractJsonSerializer(typeof(ResponseOnLogin));
            var result = (ResponseOnLogin)serializer.ReadObject(dataStream.AsStream());

            if (result.Response != 200)
            {
                
 
                throw new VPNResponceException(result.Response.ToString());
            }

            AutoLoginAgent.Current.Session = result.Session;
            AutoLoginAgent.Current.UserEmail = credentialsFacebook.Email;
            AutoLoginAgent.Current.CabinetUrl = result.UserInfo.CabinetUrl;

            if (ApplicationData.Current.LocalSettings.Values["ShowVPNNotConnected"] == null)
            {
                ApplicationData.Current.LocalSettings.Values["FirstLoginTime"] = DateTime.Now.Ticks;
            }

            return true;
        }


        public async Task<bool> LoginGoogleAsync(CredentialsGoogle credentialsGoogle)
        {
            var serializer = new DataContractJsonSerializer(typeof(CredentialsGoogleVPNServer));
            string oauthcredentialsString;

            using (MemoryStream ms = new MemoryStream())
            {
                serializer.WriteObject(ms, new CredentialsGoogleVPNServer(credentialsGoogle));
                byte[] bytearray = ms.ToArray();
                oauthcredentialsString = Encoding.UTF8.GetString(bytearray, 0, bytearray.Count());
            }

            var multipartFormDataContent = new HttpMultipartFormDataContent();
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("loginsocial")), "action");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("2")), "social_login_version");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(credentialsGoogle.Email)), "login");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(GoogleOauthServiceName)), "oauthservice");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(oauthcredentialsString)), "oauthcredentials");
            AddParametersForRequestToVPN(multipartFormDataContent);

            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(ApiLoginUrlString, UriKind.Absolute))
            {
                Content = multipartFormDataContent
            };

            var httpClient = new HttpClient();
            var responseMessage = await httpClient.SendRequestAsync(request);

            var dataStream = await responseMessage.Content.ReadAsBufferAsync();
            serializer = new DataContractJsonSerializer(typeof(ResponseOnLogin));
            var result = (ResponseOnLogin)serializer.ReadObject(dataStream.AsStream());

            if (result.Response != 200)
            {
                

                throw new VPNResponceException(result.Response.ToString());
            }

            AutoLoginAgent.Current.Session = result.Session;
            AutoLoginAgent.Current.UserEmail = credentialsGoogle.Email;
            AutoLoginAgent.Current.CabinetUrl = result.UserInfo.CabinetUrl;

            if (ApplicationData.Current.LocalSettings.Values["ShowVPNNotConnected"] == null)
            {
                ApplicationData.Current.LocalSettings.Values["FirstLoginTime"] = DateTime.Now.Ticks;
            }

            return true;
        }

        private void AddParametersForRequestToVPN(HttpMultipartFormDataContent multipartFormDataContent)
        {
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(Constants.GetDeviceModel())), "device");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(Constants.GetDeviceID())), "deviceid");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("WinPhone")), "platform");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("8.1")), "platformversion");            //ToDo: no way to determine version
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(Constants.GetAppVersion())), "appversion");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(VPNservice)), "service");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(Constants.GetCurrentLanguageNameTwoLetter())), "locale");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(Constants.GetTimeZone())), "timezone");
        }

        public async Task<bool> LogoutAsync()
        {
            if (AutoLoginAgent.Current.Session == null)
            {
                return true;
            }

            var multipartFormDataContent = new HttpMultipartFormDataContent();
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("logout")), "action");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(AutoLoginAgent.Current.Session)), "session");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(VPNservice)), "service");

            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(ApiUrlString, UriKind.Absolute))
            {
                Content = multipartFormDataContent
            };

            var httpClient = new HttpClient();
            var responseMessage = await httpClient.SendRequestAsync(request);

            var dataStream = await responseMessage.Content.ReadAsBufferAsync();
            var serializer = new DataContractJsonSerializer(typeof(ResponseOnLogin));
            var result = (ResponseOnLogin)serializer.ReadObject(dataStream.AsStream());

            return true;
        }

        public async Task<bool> InviteFriendAsync(string friendemail)
        {
            if (AutoLoginAgent.Current.Session == null)
            {
                bool loginResult = await AutoLoginAgent.AutoLogin();

                if (loginResult == false) return false;
            }

            var multipartFormDataContent = new HttpMultipartFormDataContent();
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("simplexinvite")), "action");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(AutoLoginAgent.Current.Session)), "session");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(VPNservice)), "service");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(friendemail)), "login");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("email")), "type");
            
            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(ApiLoginUrlString, UriKind.Absolute))
            {
                Content = multipartFormDataContent
            };

            var httpClient = new HttpClient();
            var responseMessage = await httpClient.SendRequestAsync(request);

            var dataStream = await responseMessage.Content.ReadAsBufferAsync();
            var serializer = new DataContractJsonSerializer(typeof(ResponceOnFriendInvitation));
            var result = (ResponceOnFriendInvitation)serializer.ReadObject(dataStream.AsStream());

            if (result.Response != 200)
            {
                

                if (result.Response == 503)
                {
                    await AutoLoginAgent.AutoLogin();
                    return await InviteFriendAsync(friendemail);
                }
                else
                {
                    throw new VPNResponceException(result.Response.ToString());
                }
            }

            return true;
        }

        public async Task<bool> ChangePassword(string _password, string _newPassword)
        {
            if (AutoLoginAgent.Current.Session == null)
            {
                bool loginResult = await AutoLoginAgent.AutoLogin();

                if (loginResult == false) return false;
            }

            var multipartFormDataContent = new HttpMultipartFormDataContent();
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("changeaccountpassword")), "action");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(AutoLoginAgent.Current.Session)), "session");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(_password)), "password");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(_newPassword)), "newpassword");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("win_phone_purch")), "purch_type");
            AddParametersForRequestToVPN(multipartFormDataContent);

            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(ApiLoginUrlString, UriKind.Absolute))
            {
                Content = multipartFormDataContent
            };

            var httpClient = new HttpClient();
            var responseMessage = await httpClient.SendRequestAsync(request);

            responseMessage.EnsureSuccessStatusCode();

            var dataStream = await responseMessage.Content.ReadAsBufferAsync();
            var serializer = new DataContractJsonSerializer(typeof(ResponseOnLogin));
            var result = (ResponseOnLogin)serializer.ReadObject(dataStream.AsStream());

            if (result.Response != 200)
            {
                

                if (result.Response == 503)
                {
                    await AutoLoginAgent.AutoLogin();
                    return await ChangePassword(_password, _newPassword);
                }
                else
                {
                    throw new VPNResponceException(result.Response.ToString());
                }
            }

            return true;
        }

        public async Task<bool> RemindPassword(string _email)
        {
            var multipartFormDataContent = new HttpMultipartFormDataContent();
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("recoveryaccountpasswordmailsend")), "action");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(_email)), "login");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(VPNservice)), "service");

            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(ApiLoginUrlString, UriKind.Absolute))
            {
                Content = multipartFormDataContent
            };

            var httpClient = new HttpClient();
            var responseMessage = await httpClient.SendRequestAsync(request);

            responseMessage.EnsureSuccessStatusCode();

            var dataStream = await responseMessage.Content.ReadAsBufferAsync();
            var serializer = new DataContractJsonSerializer(typeof(ResponseOnLogin));
            var result = (ResponseOnLogin)serializer.ReadObject(dataStream.AsStream());

            if (result.Response != 200)
            {
                throw new VPNResponceException(result.Response.ToString());
            }

            return true;
        }

        public async Task<bool> PurchaseAsync(string receiptXML, string transactionID)
        {
            ProductPurchaceReceiptVPNServer productPurchaceReceiptVPNServer = new ProductPurchaceReceiptVPNServer()
            {
                Receipt = receiptXML
            };

            var serializer = new DataContractJsonSerializer(typeof(ProductPurchaceReceiptVPNServer));
            string receiptStringAsJson;
            using (MemoryStream ms = new MemoryStream())
            {
                serializer.WriteObject(ms, productPurchaceReceiptVPNServer);
                byte[] bytearray = ms.ToArray();
                receiptStringAsJson = Encoding.UTF8.GetString(bytearray, 0, bytearray.Count());
            }

            var multipartFormDataContent = new HttpMultipartFormDataContent();
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("apppurchase")), "action");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(Guid.NewGuid().ToString())), "purchaseid");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(transactionID)), "transaction_id");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(receiptStringAsJson)), "receipt");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64(AutoLoginAgent.Current.Session)), "session");
            multipartFormDataContent.Add(new HttpStringContent(ToBase64("win_phone_purch")), "purch_type");
            AddParametersForRequestToVPN(multipartFormDataContent);

            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(ApiUrlString, UriKind.Absolute))
            {
                Content = multipartFormDataContent
            };

            var httpClient = new HttpClient();
            var responseMessage = await httpClient.SendRequestAsync(request);

            responseMessage.EnsureSuccessStatusCode();

            var dataStream = await responseMessage.Content.ReadAsBufferAsync();
            serializer = new DataContractJsonSerializer(typeof(ResponseOnLogin));
            var result = (ResponseOnLogin)serializer.ReadObject(dataStream.AsStream());

            if (result.Response != 200)
            {
                

                if (result.Response == 503)
                {
                    await AutoLoginAgent.AutoLogin();
                    return await PurchaseAsync(receiptXML, transactionID);
                }
                else
                {
                    throw new VPNResponceException(result.Response.ToString());
                }
            }

            return true;
        }

        public async Task CheckIfLogoutIsNeededAsync(int vpnErrorCode)
        {
            if (vpnErrorCode == 302 || vpnErrorCode == 308 || vpnErrorCode == 309 || vpnErrorCode == 311 || vpnErrorCode == 303 ||
                        vpnErrorCode == 313 || vpnErrorCode == 323 || vpnErrorCode == 314 || vpnErrorCode == 327)
            {
                if (AutoLoginAgent.Current.Session != null)
                {
                    await AutoLoginAgent.Current.Logout();
                    AppViewModel.Current.ClearNavigationStack();
                    AppViewModel.Current.GoHome();
                }
            }
        }

        public async Task ShowErrorAsync(int vpnErrorCode)
        {
            string errorDescription;

            switch (vpnErrorCode)
            {
                case 501:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_INVALID_REQUEST_SERV");
                    break;
                case 502:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_INVALID_PARAMS_SERV");
                    break;
                case 503:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_INVALID_SESSION_SERV");
                    break;
                case 505:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_INVALID_DB_CONNECTION_SERV");
                    break;
                case 305:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_ENCRYPTYON_FAILURE_SERV");
                    break;
                case 306:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_INVALID_URL_SERV");
                    break;
                case 342:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_APPLESERVER_NOT_RESPONSE_SERV");
                    break;
                case 343:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_INVALID_PURCHASE_SERV");
                    break;
                case 504:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_INVALID_USERNAME_SERV");
                    break;
                case 301:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_ALREADY_CONFIRMED_SERV");
                    break;
                case 302:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_INVALID_USERNAME_PASSWORD_SERV");
                    break;
                case 303:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_LOGIN_ATTEMPTS_EXCEEDED_SERV").Replace("%s", AutoLoginAgent.Current.UserEmail ?? "");
                    break;
                case 304:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_INVALID_REFCODE_SERV");
                    break;
                case 308:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_TOO_MUCH_REGS");
                    break;
                case 341:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_PURCHASE_ALREADY_EXISTS_SERV");
                    break;
                case 361:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_TOO_MUCH_MAIL");
                    break;
                case 309:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_TOO_MANY_DEVICES");
                    break;
                case 310:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_USER_ALREADY_EXIST");
                    break;
                case 311:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_NEED_CONFIRM_EMAIL");
                    break;
                case 323:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_ACCOUNT_CANT_BE_USED");
                    break;
                case 313:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_USER_BANNED");
                    break;
                case 314:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_USER_DELETED");
                    break;
                case 327:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_PROFILE_IS_INCORRECT");
                    break;
                default:
                    errorDescription = Localization.LocalizedResources.GetLocalizedString("S_APPLESERVER_NOT_RESPONSE_SERV");
                    break;
            }

            MessageDialog msg = new MessageDialog(
                errorDescription.Replace("%s", vpnErrorCode.ToString()),
                    Localization.LocalizedResources.GetLocalizedString("S_ERROR"));

            msg.Commands.Add(new UICommand(Localization.LocalizedResources.GetLocalizedString("S_OK"), null));

            await msg.ShowAsync();


        }
    }
}
