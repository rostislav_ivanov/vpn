﻿using System;
using System.Globalization;
using System.Reflection;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Security.ExchangeActiveSyncProvisioning;
using Windows.Storage.Streams;
using Windows.System.Profile;
using Windows.UI;
using Windows.UI.Xaml;

namespace VPN.ViewModel
{
    public static class Constants
    {
        //Urls
        public const string PrivacyPolicyUrlString = "https://www.vpnunlimitedapp.com/privacy/";
        public const string FAQUrlString = "https://www.vpnunlimitedapp.com/help";
        public const string FacebookAppString = "https://www.facebook.com/vpnunlimitedapp";
        public const string TwitterAppString = "https://twitter.com/vpnunlimited";
        public const string SupportEmail = "support@keepsolid.com";
        
        //Social
        public const string GoogleClientId = "922621893574-grqe7ehb5sq05b4ag38nf0ho19icq2lt.apps.googleusercontent.com";
        public const string GoogleClientSecret = "VqZR9tiTyrP6-UY9G48_EJTg";
        public const string GoogleCallbackUrl = "http://localhost";


        public const string EncryptPassword = "L,2W~fKo|]*,VFiZ8&n";
        //Colors
        public static Color StatusOffColor = Color.FromArgb(255, 229, 20, 0);
        public static Color StatusOnColor = Color.FromArgb(255, 131, 186, 31);
        
        public static string GetAppVersion()
        {
            dynamic type = Application.Current.GetType();
            var version = (new AssemblyName(type.Assembly.FullName)).Version;
            return String.Format("{0}.{1}", version.Major, version.Minor);
        }

        public static string GetDeviceID()
        {
            HardwareToken token = HardwareIdentification.GetPackageSpecificToken(null);
            IBuffer hardwareId = token.Id;

            HashAlgorithmProvider hasher = HashAlgorithmProvider.OpenAlgorithm("MD5");
            IBuffer hashed = hasher.HashData(hardwareId);

            string hashedString = CryptographicBuffer.EncodeToHexString(hashed);
            return hashedString;
        }

        public static string GetDeviceModel()
        {
            var deviceInformation = new EasClientDeviceInformation();
            return deviceInformation.FriendlyName;
        }

        public static string GetCurrentLanguageNameTwoLetter()
        {
            //CultureInfo ci = new CultureInfo(Windows.System.UserProfile.GlobalizationPreferences.Languages[0]);
            //return ci.TwoLetterISOLanguageName;

            return Windows.System.UserProfile.GlobalizationPreferences.Languages[0].Substring(0, 2);
        }
        public static string GetTimeZone()
        {
            return ((TimeZoneInfo.Local.BaseUtcOffset < TimeSpan.Zero) ? "-" : "+") + TimeZoneInfo.Local.BaseUtcOffset.ToString("hhmm");
        }
    }
}
