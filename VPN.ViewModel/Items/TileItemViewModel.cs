﻿using System.Runtime.Serialization;
using System.Windows.Input;
using MetroLab.Common;

namespace VPN.ViewModel.Items
{
    [DataContract]
    public abstract class TileItemViewModel : BaseViewModel
    {
        [IgnoreDataMember] public abstract ICommand TileCommand { get; }
    }
}
