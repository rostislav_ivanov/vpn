﻿using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using MetroLab.Common;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Windows.Input;
using VPN.Model;
using VPN.ViewModel.Http;
using VPN.ViewModel.Pages;

namespace VPN.ViewModel.Items
{
    [DataContract]
    public class ServerViewModel : BaseViewModel
    {
        private string _country;
        [DataMember]
        public string Country
        {
            get { return _country; }
            set { SetProperty(ref _country, value); }
        }

        private string _flagUrl;
        [DataMember]
        public string FlagUrl
        {
            get { return _flagUrl; }
            set { SetProperty(ref _flagUrl, value); }
        }

        private string _flagIcon;
        [DataMember]
        public string FlagIcon
        {
            get { return _flagIcon; }
            set { SetProperty(ref _flagIcon, value); OnPropertyChanged("BorderBrush"); }
        }

        private string _city;
        [DataMember]
        public string City
        {
            get { return _city; }
            set { SetProperty(ref _city, value); }
        }

        public string CountryPlusCity
        {
            get { return String.Concat(_country, ", ", _city); }
        }
        public SolidColorBrush BorderBrush
        {
            get { return FlagIcon.Contains("optimal") ? new SolidColorBrush(Color.FromArgb(0, 0, 0, 0)) : 
                new SolidColorBrush(Color.FromArgb(255, 221, 221, 221)); }
        }

        private string _region;
        [DataMember]
        public string Region  //Id
        {
            get { return _region; }
            set { SetProperty(ref _region, value); }
        }
    }
}
