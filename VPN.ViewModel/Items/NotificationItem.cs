﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MetroLab.Common;
using VPN.ViewModel.Items;
using VPN.ViewModel.Pages;

namespace VPN.ViewModel.Items
{
    [DataContract]
    public class NotificationItem : BaseViewModel
    {
        private string _name;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        private string _iconName;
        [DataMember]
        public string IconName
        {
            get { return _iconName; }
            set { SetProperty(ref _iconName, value); }
        }

        private string _url;
        [DataMember]
        public string Url
        {
            get { return _url; }
            set { SetProperty(ref _url, value); }
        }

        public enum NotificationType
        {
            Browser,
            Share,
            Rate
        };

        [DataMember]
        public NotificationType Type { get; set; }
    }
}
