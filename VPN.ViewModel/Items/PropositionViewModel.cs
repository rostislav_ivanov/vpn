﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.ApplicationModel.Store;
using Windows.Storage;
using MetroLab.Common;
using VPN.ViewModel.Http;

namespace VPN.ViewModel.Items
{
    [DataContract]
    public class PropositionViewModel : BaseViewModel
    {
        public string ID;

        private string _price;
        [DataMember]
        public string Price
        {
            get { return _price; }
            set {
                if (SetProperty(ref _price, value))
                {
                    OnPropertyChanged("Price");
                } 
            }
        }

        /*
        public string PriceWitoutDiscountString
        {
            get { return _discount > 0 ? String.Concat("$ ", Math.Round(100.0 * _price / _discount, 2)) : null; }
        }
        */

        private string _period;
        [DataMember]
        public string Period
        {
            get { return _period; }
            set { SetProperty(ref _period, value); }
        }

        private int _discount;
        [DataMember]
        public int Discount
        {
            get { return _discount; }
            set { SetProperty(ref _discount, value); }
        }

        private int _daysCount;
        [DataMember]
        public int DaysCount
        {
            get { return _daysCount; }
            set { SetProperty(ref _daysCount, value); }
        }

        public string DiscountString
        {
            get { return _discount == 0 ? null : String.Concat(_discount.ToString(),"%"); }
        }
    }
}
