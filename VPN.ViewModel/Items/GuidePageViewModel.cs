﻿using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Windows.Input;
using MetroLab.Common;

namespace VPN.ViewModel.Items
{
    [DataContract]
    public class GuidePageViewModel : BaseViewModel
    {
        private string _screenPath;
        [DataMember]
        public string ScreenPath
        {
            get { return _screenPath; }
            set { SetProperty(ref _screenPath, value); }
        }

        private string _devicePath = "/Assets/VPNGuide/device.png";
        [DataMember]
        public string DevicePath
        {
            get { return _devicePath; }
            set { SetProperty(ref _devicePath, value); }
        }

        private string _stepNumber;
        [DataMember]
        public string StepNumber
        {
            get { return _stepNumber; }
            set { SetProperty(ref _stepNumber, value); }
        }

        private string _stepDescription;
        [DataMember]
        public string StepDescription
        {
            get { return _stepDescription; }
            set { SetProperty(ref _stepDescription, value); }
        }

        private string _dataToCopy;
        [DataMember]
        public string DataToCopy
        {
            get { return _dataToCopy; }
            set { SetProperty(ref _dataToCopy, value); }
        }


        //All the rest code is for implementing showing next button when user haven't done on the page more than 

        private bool _isLastPage;
        [DataMember]
        public bool IsLastPage
        {
            get { return _isLastPage; }
            set { SetProperty(ref _isLastPage, value); }
        }

        private const int TimeSpanBeforeNextButtonIsAppeared = 10000;

        private bool _isNextButtonIsVisible;
        public bool IsNextButtonIsVisible
        {
            get
            {
                if (IsLastPage == false)
                    return _isNextButtonIsVisible;
                else
                    return false;
            }
            set
            {
                if (IsLastPage == false)
                    SetProperty(ref _isNextButtonIsVisible, value);
            }
        }

        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { SetProperty(ref _isSelected, value); }
        }

        public int HowManyTimesUserFlippedToThisPage = 0;
        public async Task ShowNextButtonAsync()
        {
            HowManyTimesUserFlippedToThisPage++;

            await Task.Delay(TimeSpanBeforeNextButtonIsAppeared);

            HowManyTimesUserFlippedToThisPage--;

            if (HowManyTimesUserFlippedToThisPage == 0 && IsSelected)
            {
                IsNextButtonIsVisible = true;
            }
        }

        private ICommand _goBackToMainPageCommand;
        public ICommand GoBackToMainPageCommand
        {
            get
            {
                return IsLastPage ? GetCommand(ref _goBackToMainPageCommand, async o =>
                    await AppViewModel.Current.NavigateToViewModel(new MainPageViewModel())) : null;
            }
        }
    }
}
