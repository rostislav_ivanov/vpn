﻿using System.Runtime.Serialization;
using System.Threading.Tasks;
using MetroLab.Common;

namespace VPN.ViewModel.Items
{
    [DataContract]
    public class OverviewPageViewModel : BaseViewModel
    {
        private string _imagePath;
        [DataMember]
        public string ImagePath
        {
            get { return _imagePath; }
            set { SetProperty(ref _imagePath, value); }
        }

        private string _title;
        [DataMember]
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private string _text1;
        [DataMember]
        public string Text1   //First bold paragraph
        {
            get { return _text1; }
            set { SetProperty(ref _text1, value); }
        }

        private string _text2;
        [DataMember]
        public string Text2   
        {
            get { return _text2; }
            set { SetProperty(ref _text2, value); }
        }

        private string _text3;
        [DataMember]
        public string Text3
        {
            get { return _text3; }
            set { SetProperty(ref _text3, value); }
        }

        private string _text4;
        [DataMember]
        public string Text4
        {
            get { return _text4; }
            set { SetProperty(ref _text4, value); }
        }

        //All the rest code is for implementing showing next button when user haven't done on the page more than 

        private bool _isLastPage;
        [DataMember]
        public bool IsLastPage
        {
            get { return _isLastPage; }
            set { SetProperty(ref _isLastPage, value); }
        }

        private const int TimeSpanBeforeNextButtonIsAppeared = 10000;

        private bool _isNextButtonIsVisible;
        public bool IsNextButtonIsVisible
        {
            get
            {
                if (IsLastPage == false)
                    return _isNextButtonIsVisible;
                else
                    return true;
            }
            set 
            {   
                if (IsLastPage == false)
                    SetProperty(ref _isNextButtonIsVisible, value);
            }
        }

        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { SetProperty(ref _isSelected, value); }
        }

        public int HowManyTimesUserFlippedToThisPage = 0; 
        public async Task ShowNextButtonAsync()
        {
            HowManyTimesUserFlippedToThisPage++;

            await Task.Delay(TimeSpanBeforeNextButtonIsAppeared);

            HowManyTimesUserFlippedToThisPage--;

            if (HowManyTimesUserFlippedToThisPage == 0 && IsSelected)
            {
                IsNextButtonIsVisible = true;
            }
        }
    }
}