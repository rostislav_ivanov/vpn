﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage;
using Windows.Storage.Streams;

namespace VPN.ViewModel
{
    public static class CacheAgent
    {
        public const string GoogleCredentialsLocalSettingsName = "google";
        public const string FacebookCredentialsLocalSettingsName = "facebook";
        public const string KeepSolidCredentialsLocalSettingsName = "keepsolid";
        private const string IsNeedToShowApplicationTourLocalSettingsName = "applicationTour";
        private const string IsUserKnowHowToConnectSettingsName = "isUserKnowHowToConnect";
        

        public static void SaveToLocalSettings<T>(string name, T objectToSave)
        {
            using (var stream = new MemoryStream())
            {
                var serializer = new DataContractJsonSerializer(typeof(T));
                serializer.WriteObject(stream, objectToSave);
                byte[] bytearray = stream.ToArray();
                ApplicationData.Current.LocalSettings.Values[name] = Encoding.UTF8.GetString(bytearray, 0, bytearray.Count());
            }
        }

        public static T LoadFromLocalSettings<T>(string name) where T : class
        {
            try
            {
                string localSettingsValue = (string)ApplicationData.Current.LocalSettings.Values[name];
                if (localSettingsValue == null) return null;

                using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(localSettingsValue)))
                {
                    var serializer = new DataContractJsonSerializer(typeof(T));
                    return (T)serializer.ReadObject(stream);
                }
            }
            catch
            {
                return null;
            }
        }

        public static bool IsNeedToShowApplicationTour
        {
            get
            {
                return (ApplicationData.Current.LocalSettings.Values[IsNeedToShowApplicationTourLocalSettingsName] == null);
            }
            set
            {
                ApplicationData.Current.LocalSettings.Values[IsNeedToShowApplicationTourLocalSettingsName] = "no";
            }
            
        }

        public static bool IsUserKnowHowToConnect
        {
            get
            {
                return (ApplicationData.Current.LocalSettings.Values[IsUserKnowHowToConnectSettingsName] != null);
            }
            set
            {
                ApplicationData.Current.LocalSettings.Values[IsUserKnowHowToConnectSettingsName] = true;
            }

        }

        public static void DeleteLoginCache()
        {
            ApplicationData.Current.LocalSettings.Values[GoogleCredentialsLocalSettingsName] = null;
            ApplicationData.Current.LocalSettings.Values[FacebookCredentialsLocalSettingsName] = null;
            ApplicationData.Current.LocalSettings.Values[KeepSolidCredentialsLocalSettingsName] = null;
        }

        public static string Encrypt(string dataToEncrypt, string openPartOfPassword, out string key)
        {
            key = GenerateSalt();
            return Encrypt(dataToEncrypt,
                Constants.GetDeviceID() + Constants.GetDeviceModel() + openPartOfPassword + Constants.EncryptPassword, key);
        }

        private static string Encrypt(string dataToEncrypt, string password, string salt)
        {
            // Generate a key and IV from the password and salt
            IBuffer aesKeyMaterial;
            IBuffer iv;
            uint iterationCount = 10000;
            GenerateKeyMaterial(password, salt, iterationCount, out aesKeyMaterial, out iv);

            IBuffer plainText = CryptographicBuffer.ConvertStringToBinary(dataToEncrypt, BinaryStringEncoding.Utf8);

            // Setup an AES key, using AES in CBC mode and applying PKCS#7 padding on the input
            SymmetricKeyAlgorithmProvider aesProvider = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbcPkcs7);
            CryptographicKey aesKey = aesProvider.CreateSymmetricKey(aesKeyMaterial);

            // Encrypt the data and convert it to a Base64 string
            IBuffer encrypted = CryptographicEngine.Encrypt(aesKey, plainText, iv);
            return CryptographicBuffer.EncodeToBase64String(encrypted);
        }

        public static string Decrypt(string dataToDecrypt, string openPartOfPassword, string salt)
        {
            string password = Constants.GetDeviceID() + Constants.GetDeviceModel() + 
                openPartOfPassword + Constants.EncryptPassword;

            // Generate a key and IV from the password and salt
            IBuffer aesKeyMaterial;
            IBuffer iv;
            uint iterationCount = 10000;
            GenerateKeyMaterial(password, salt, iterationCount, out aesKeyMaterial, out iv);

            // Setup an AES key, using AES in CBC mode and applying PKCS#7 padding on the input
            SymmetricKeyAlgorithmProvider aesProvider = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbcPkcs7);
            CryptographicKey aesKey = aesProvider.CreateSymmetricKey(aesKeyMaterial);

            // Convert the base64 input to an IBuffer for decryption
            IBuffer ciphertext = CryptographicBuffer.DecodeFromBase64String(dataToDecrypt);

            // Decrypt the data and convert it back to a string
            IBuffer decrypted = CryptographicEngine.Decrypt(aesKey, ciphertext, iv);
            byte[] decryptedArray = decrypted.ToArray();
            return Encoding.UTF8.GetString(decryptedArray, 0, decryptedArray.Length);
        }

        private static void GenerateKeyMaterial(string password, string salt, uint iterationCount, out IBuffer keyMaterial, out IBuffer iv)
        {
            // Setup KDF parameters for the desired salt and iteration count
            IBuffer saltBuffer = CryptographicBuffer.ConvertStringToBinary(salt, BinaryStringEncoding.Utf8);
            KeyDerivationParameters kdfParameters = KeyDerivationParameters.BuildForPbkdf2(saltBuffer, iterationCount);

            // Get a KDF provider for PBKDF2, and store the source password in a Cryptographic Key
            KeyDerivationAlgorithmProvider kdf = KeyDerivationAlgorithmProvider.OpenAlgorithm(KeyDerivationAlgorithmNames.Pbkdf2Sha256);
            IBuffer passwordBuffer = CryptographicBuffer.ConvertStringToBinary(password, BinaryStringEncoding.Utf8);
            CryptographicKey passwordSourceKey = kdf.CreateKey(passwordBuffer);

            // Generate key material from the source password, salt, and iteration count.  Only call DeriveKeyMaterial once,
            // since calling it twice will generate the same data for the key and IV.
            int keySize = 256 / 8;
            int ivSize = 128 / 8;
            uint totalDataNeeded = (uint)(keySize + ivSize);
            IBuffer keyAndIv = CryptographicEngine.DeriveKeyMaterial(passwordSourceKey, kdfParameters, totalDataNeeded);

            // Split the derived bytes into a seperate key and IV
            byte[] keyMaterialBytes = keyAndIv.ToArray();
            keyMaterial = WindowsRuntimeBuffer.Create(keyMaterialBytes, 0, keySize, keySize);
            iv = WindowsRuntimeBuffer.Create(keyMaterialBytes, keySize, ivSize, ivSize);
        }

        public static string GenerateSalt()
        {
            IBuffer randomBuffer = CryptographicBuffer.GenerateRandom(1024);
            var hasher = HashAlgorithmProvider.OpenAlgorithm("SHA256");
            IBuffer hashed = hasher.HashData(randomBuffer);
            return CryptographicBuffer.EncodeToBase64String(hashed);
        }
    }
}
