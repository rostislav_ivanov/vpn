﻿using System;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Store;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using MetroLab.Common;
//using VPN.BackgroundAgent;
using VPN.View;
using VPN.ViewModel;
using VPN.ViewModel.Http;
using VPN.ViewModel.Pages;
//using BackgroundAgentsHelper = Calendar.BackgroundAgent.BackgroundAgentsHelper;

namespace VPN
{
    sealed partial class App
    {
        private static readonly Type HomePageViewModelType = typeof(LoginPageViewModel);
        private static readonly Type StartViewModelType = typeof(LoginPageViewModel);

        private MvvmFrame _mvvmFrame;

        [DataContract]
        private class AppStateSnapshot
        {
            [DataMember]
            public IPageViewModel[] PageViewModels { get; set; }
        }

        private class DataContractViewModelSerializer : IPageViewModelStackSerializer
        {
            private readonly DataContractSerializerSettings _settings;

            public DataContractViewModelSerializer()
            {
                var settings = new DataContractSerializerSettings
                {
                    KnownTypes = new[]
                    {
                        typeof (BindableBase), typeof (BaseViewModel), typeof (DataLoadViewModel),
                        typeof (SimplePageViewModel), typeof (MainPageViewModel), typeof (GuideGalleryPageViewModel),
                        typeof (LoginPageViewModel), typeof(LoginKeepSolidIDPageViewModel), 
                        typeof (SettingsPageViewModel), typeof(ServerCredentialsPageViewModel),
                        typeof (InviteFriendPageViewModel), typeof(OverviewGalleryPageViewModel), typeof(RegistrationPageViewModel),
                        typeof (ChangePasswordPageViewModel)
                    },
                    SerializeReadOnlyTypes = false,
                };
                _settings = settings;
            }

            public async Task SerializeAsync(IPageViewModel[] pageViewModels, System.IO.Stream streamForWrite)
            {
                var appStateSnapshot = new AppStateSnapshot
                {
                    PageViewModels = pageViewModels
                };
                var serializer = new DataContractSerializer(typeof(AppStateSnapshot), _settings);
                serializer.WriteObject(streamForWrite, appStateSnapshot);
                await streamForWrite.FlushAsync();
            }

            public async Task<IPageViewModel[]> DeserializeAsync(System.IO.Stream streamForRead)
            {
                var serializer = new DataContractSerializer(typeof(AppStateSnapshot), _settings);
                var appStateSnapshot = (AppStateSnapshot)serializer.ReadObject(streamForRead);
                return appStateSnapshot.PageViewModels;
            }
        }

        public App()
        {
            this.InitializeComponent();
            UnhandledException += CurrentUnhandledExceptionAsync;
            Suspending += OnSuspendingAsync;

            Windows.Phone.UI.Input.HardwareButtons.BackPressed += (s, e) =>
            {
                AppViewModel.Current.GoBack();
                e.Handled = true;
            };

        }

        public Action OnNewLogEntry { get; set; }

        private static MvvmFrame CreateMainFrame()
        {
            var result = new MvvmFrame(new DataContractViewModelSerializer()) { LoadDataAtBackgroundThread = true };
            result.AddSupportedPageViewModel(typeof(MainPageViewModel), typeof(MainPage));
            result.AddSupportedPageViewModel(typeof(GuideGalleryPageViewModel), typeof(GuideGalleryPage));
            result.AddSupportedPageViewModel(typeof(LoginPageViewModel), typeof(LoginPage));
            result.AddSupportedPageViewModel(typeof(LoginKeepSolidIDPageViewModel), typeof(LoginKeepSolidIDPage));
            result.AddSupportedPageViewModel(typeof(SettingsPageViewModel), typeof(SettingsPage));
            result.AddSupportedPageViewModel(typeof(ServerCredentialsPageViewModel), typeof(ServerCredentialsPage));
            result.AddSupportedPageViewModel(typeof(InviteFriendPageViewModel), typeof(InviteFriendPage));
            result.AddSupportedPageViewModel(typeof(OverviewGalleryPageViewModel), typeof(OverviewGalleryPage));
            result.AddSupportedPageViewModel(typeof(RegistrationPageViewModel), typeof(RegistrationPage));
            result.AddSupportedPageViewModel(typeof(ChangePasswordPageViewModel), typeof(ChangePasswordPage));
            return result;
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="args">Details about the launch request and process.</param>
        protected override async void OnLaunched(LaunchActivatedEventArgs args)
        {
            /*try
            {
                // Read the contents of the Certificate file
                System.Uri certificateFile = new System.Uri("ms-appx:///Assets/caCert-vpn.pem");
                Windows.Storage.StorageFile file = await Windows.Storage.StorageFile.GetFileFromApplicationUriAsync(certificateFile);
                Windows.Storage.Streams.IBuffer certBlob = await Windows.Storage.FileIO.ReadBufferAsync(file);

                // Create an instance of the Certificate class using the retrieved certificate blob contents
                Windows.Security.Cryptography.Certificates.Certificate rootCert = new Windows.Security.Cryptography.Certificates.Certificate(certBlob);

                // Get access to the TrustedRootCertificationAuthorities for your own app (not the system one)
                Windows.Security.Cryptography.Certificates.CertificateStore trustedStore = Windows.Security.Cryptography.Certificates.CertificateStores.TrustedRootCertificationAuthorities;

                // Add the certificate to the TrustedRootCertificationAuthorities store for your app
                trustedStore.Add(rootCert);
            }
            catch (Exception oEx)
            {
                // Catch and report exceptions
                System.Diagnostics.Debug.WriteLine("Exception Adding cert: " + oEx.Message);
            }*/


            if (args.PreviousExecutionState != ApplicationExecutionState.Running)
            {
                bool wasTerminated = (args.PreviousExecutionState == ApplicationExecutionState.Terminated);

                var extendedSplash = new ExtendedSplash();
                Window.Current.Content = extendedSplash;

                args.SplashScreen.Dismissed += (sender, o) =>
                    extendedSplash.Dispatcher.RunAsync(CoreDispatcherPriority.Low, async () =>
                    {
                        var statusBar = Windows.UI.ViewManagement.StatusBar.GetForCurrentView();
                        statusBar.BackgroundColor = Color.FromArgb(255, 97, 97, 97);
                        statusBar.ForegroundColor = Colors.White;
                        statusBar.BackgroundOpacity = 1;

                        if (!wasTerminated)
                        {
                            await ContentCache.Current.RemoveOutdatedFilesAsync(DateTime.Now - TimeSpan.FromDays(30));
                            await ContentCacheTemp.Current.ClearCachedFiles();
                        }

                        await ContentCache.Current.Initialize();
                        await ContentCacheTemp.Current.Initialize();

                        await InitializeTask;
                        OnLaunchedInnerAsync(args);
                    });
            }
            else
            {
                OnLaunchedInnerAsync(args);
            }
        }

        protected override void OnActivated(IActivatedEventArgs args)
        {
            base.OnActivated(args);

            var continuationEventArgs = args as IContinuationActivatedEventArgs;
            if (continuationEventArgs != null)
            {
                var continuationManager = new Common.ContinuationManager();
                continuationManager.Continue(continuationEventArgs);
            }

            var protocolActivatedEventArgs = args as ProtocolActivatedEventArgs;
            if (protocolActivatedEventArgs != null)
            {
                LoginPage.Current.ContinueAppAuthentication(Uri.UnescapeDataString(protocolActivatedEventArgs.Uri.ToString()));
            }

            if (args.Kind == ActivationKind.Protocol)
            {

                OnLaunchedInnerAsync(args);
                return;
            }
        }

        private async void OnLaunchedInnerAsync(IActivatedEventArgs args)
        {
            Window.Current.Content = _mvvmFrame;
            Window.Current.Activate();

            AppViewModel.Current.HomePageViewModelType = HomePageViewModelType;// Set startup Page

            if ((args.PreviousExecutionState == ApplicationExecutionState.Terminated))
            {
                try
                {
                    await _mvvmFrame.LoadFromStorageAsync(true); //restore
                    AutoLoginAgent.Current = CacheAgent.LoadFromLocalSettings<AutoLoginAgent>("temp");
                }
                catch (Exception e)
                {
                    //errors in deserializing
#if DEBUG
                    if (Debugger.IsAttached) Debugger.Break();
#endif

                    AppViewModel.Current.NavigateToViewModel((IPageViewModel)Activator.CreateInstance(StartViewModelType));
                }
            }
            else if (args.PreviousExecutionState == ApplicationExecutionState.Running)
            {
                Window.Current.Activate();
            }
            else
            {
                if (false)
                {
                   
                }
                else
                {
                    if (CacheAgent.IsNeedToShowApplicationTour)
                    {
                        AppViewModel.Current.NavigateToViewModel((IPageViewModel)Activator.CreateInstance(typeof(OverviewGalleryPageViewModel)));
                    }
                    else
                    {
                        AppViewModel.Current.NavigateToViewModel((IPageViewModel)Activator.CreateInstance(StartViewModelType));
                    }
                }
            }
        }

        private Task _initializeTask;

        private Task InitializeTask
        {
            get { return _initializeTask ?? (_initializeTask = InitializeAsync()); }
        }

        private async Task InitializeAsync()
        {
            try
            {
                if (!Resources.ContainsKey("AppViewModel"))
                    Resources.Add("AppViewModel", AppViewModel.Current);

                //ToDo:
                //await DebugMenuPageViewModel.InitCurrentLog();
                _mvvmFrame = CreateMainFrame();

                if (Constants.GetCurrentLanguageNameTwoLetter() == "ar")
                     _mvvmFrame.FlowDirection = FlowDirection.RightToLeft;
                

                AppViewModel.Current.MvvmFrame = _mvvmFrame;

                await VPN.ViewModel.BackgroundAgentsHelper.RegisterUpdateApplicationTileAgent();
                await ContentCache.Current.Initialize();
            }
            catch (Exception)
            {
                if (Debugger.IsAttached) Debugger.Break();
                throw;
            }
        }

        private void CurrentUnhandledExceptionAsync(object sender, UnhandledExceptionEventArgs e)
        {
            e.Handled = false;
#if DEBUG
            if (Debugger.IsAttached) Debugger.Break();
            ShowErrorMessage(e.Exception != null ? e.Exception.StackTrace : String.Empty, e.Message);
#else 
            ShowErrorMessage(CommonLocalizedResources.GetLocalizedString("msg_GeneralUnhandledException"),
                CommonLocalizedResources.GetLocalizedString("txt_Error"));
#endif
        }

        private async void ShowErrorMessage(string content, string title)
        {
            var md = new MessageDialog(content ?? string.Empty, title ?? string.Empty);
            md.Commands.Add(new UICommand("Ok"));
            await md.ShowAsync();
            Current.Exit();
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private async void OnSuspendingAsync(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            await AppViewModel.Current.MvvmFrame.SaveToStorageAsync();

            CacheAgent.SaveToLocalSettings("temp", AutoLoginAgent.Current);

            //ToDo:
            //await DebugMenuPageViewModel.SaveDebugMenuLog();
            deferral.Complete();
        }
    }
}