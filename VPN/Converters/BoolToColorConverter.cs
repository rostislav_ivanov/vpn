﻿using System;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;
using VPN.ViewModel;

namespace VPN.Converters
{
    public class BoolToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return (bool)value ? new SolidColorBrush(Constants.StatusOnColor) : new SolidColorBrush(Constants.StatusOffColor); ;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotSupportedException();
        }
    }
}