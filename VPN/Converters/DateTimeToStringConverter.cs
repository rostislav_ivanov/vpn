﻿using System;
using Windows.UI.Xaml.Data;

namespace VPN.Converters
{
    public class DateTimeToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return value == null ? null : ((DateTime) value).ToString(
                parameter != null && !string.IsNullOrEmpty((string)parameter) ? (string)parameter : "HH:mm yyyy.MM.dd");
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
