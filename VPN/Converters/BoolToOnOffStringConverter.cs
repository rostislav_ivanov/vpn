﻿using System;
using Windows.UI.Xaml.Data;

namespace VPN.Converters
{
    public class BoolToOnOffStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return (bool)value ? "on" : "off";
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotSupportedException();
        }
    }
}