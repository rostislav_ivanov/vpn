﻿using MetroLab.Common;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Resources;
using Windows.ApplicationModel.Store;
using Windows.System;
using Windows.UI.Popups;

namespace VPN.ViewModel.Pages
{
    public enum FeedbackState { Inactive, FirstReview, SecondReview, Feedback }

    /// <summary>
    /// This helper class controls the behaviour of the FeedbackOverlay control
    /// When the app has been launched 5 times the initial prompt is shown
    /// If the user reviews no more prompts are shown
    /// When the app has bee launched 10 times and not been reviewed, the prompt is shown
    /// </summary>
    public class RatingsHelper
    {
        private const string AppUri = "ms-windows-store:reviewapp?appid=69b97f20-7b3b-457b-9e15-3c6a3194ce71";

        private const string LaunchCount = "LAUNCH_COUNT";
        private const string Reviewed = "REVIEWED";
        private const string SupportMail = "mailto:{0}?subject={1}&body={2}";
        private const int FirstCount = 1;
        private const int SecondCount = 8;

        private int _launchCount;
        private bool _reviewed;
        private bool _isTrial;
        private string _message;
        private string _title;
        private string _yesText;
        private string _noText;

        private FeedbackState _state = FeedbackState.Inactive;
        private static ResourceLoader _resLoader;

        /// <summary>
        /// Loads last state from storage and works out the new state
        /// </summary>
        private void LoadState()
        {
            try
            {
                // Load counters from storage
                _launchCount = SettingsStorageHelper.GetSetting<int>(LaunchCount);
                _reviewed = SettingsStorageHelper.GetSetting<bool>(Reviewed);

                // Check trial state
                _isTrial = CurrentApp.LicenseInformation.IsActive && CurrentApp.LicenseInformation.IsTrial;

                if (!_reviewed)
                {
                    _launchCount++;

                    if (_launchCount == FirstCount)
                        _state = FeedbackState.FirstReview;
                    else if (_launchCount == SecondCount)
                        _state = FeedbackState.SecondReview;

                    StoreState();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("FeedbackHelper.LoadState - Failed to load state, Exception: {0}", ex);
            }
        }

        /// <summary>
        /// Stores current state
        /// </summary>
        private void StoreState()
        {
            try
            {
                SettingsStorageHelper.SetSetting(LaunchCount, _launchCount);
                SettingsStorageHelper.SetSetting(Reviewed, _reviewed);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("FeedbackHelper.StoreState - Failed to store state, Exception: {0}", ex);
            }
        }

        /// <summary>
        /// Entry point to initialise and start rating workflow
        /// </summary>
        /// <returns></returns>
        public async Task InitialiseAsync()
        {
            LoadState();

            // Only continue if not trial
            if (_isTrial) return;

            // Uncomment for testing
            //_state = FeedbackState.FirstReview;
            // _state = FeedbackState.SecondReview;
            // _state = FeedbackState.Feedback;

            if (_state == FeedbackState.FirstReview)
            {
                _title = Localization.LocalizedResources.GetLocalizedString("S_RATE_US_ON_STORE");
                _message = Localization.LocalizedResources.GetLocalizedString("WinPhoneRatingMessage1");
                _yesText = Localization.LocalizedResources.GetLocalizedString("WinPhoneRatingYes");
                _noText = Localization.LocalizedResources.GetLocalizedString("S_NO_BTN");

                await ShowMessageAsync();
            }
            else if (_state == FeedbackState.SecondReview)
            {
                _title = Localization.LocalizedResources.GetLocalizedString("S_RATE_US_ON_STORE");
                _message = Localization.LocalizedResources.GetLocalizedString(" WinPhoneRatingMessage2");
                _yesText = Localization.LocalizedResources.GetLocalizedString("WinPhoneRatingYes");
                _noText = Localization.LocalizedResources.GetLocalizedString("S_NO_BTN");

                await ShowMessageAsync();
            }
        }

        public async Task MakeReviewWithMessages(bool IsFirstMessage)
        {
            if (IsFirstMessage) _state = FeedbackState.FirstReview;
            else _state = FeedbackState.SecondReview;

            if (_state == FeedbackState.FirstReview)
            {
                _title = Localization.LocalizedResources.GetLocalizedString("S_RATE_US_ON_STORE");
                _message = Localization.LocalizedResources.GetLocalizedString("WinPhoneRatingMessage1");
                _yesText = Localization.LocalizedResources.GetLocalizedString("WinPhoneRatingYes");
                _noText = Localization.LocalizedResources.GetLocalizedString("S_NO_BTN");

                await ShowMessageAsync();
            }
            else if (_state == FeedbackState.SecondReview)
            {
                _title = Localization.LocalizedResources.GetLocalizedString("S_RATE_US_ON_STORE");
                _message = Localization.LocalizedResources.GetLocalizedString("WinPhoneRatingMessage2");
                _yesText = Localization.LocalizedResources.GetLocalizedString("WinPhoneRatingYes");
                _noText = Localization.LocalizedResources.GetLocalizedString("S_NO_BTN");

                await ShowMessageAsync();
            }
        }

        private async Task ShowMessageAsync()
        {
            var md = new MessageDialog(_message, _title);
            var result = 0;

            md.Commands.Add(new UICommand(_yesText, a => result = 1));
            md.Commands.Add(new UICommand(_noText, a => result = 2));
            md.DefaultCommandIndex = 0;

            await md.ShowAsync();

            if (result == 1) await OnYesClickAsync();
            else await OnNoClickAsync();
        }

        private async Task OnNoClickAsync()
        {
            if (_state == FeedbackState.FirstReview)
            {
                _title = Localization.LocalizedResources.GetLocalizedString("WinPhoneFeedbackTitle");
                _message = String.Format(Localization.LocalizedResources.GetLocalizedString("WinPhoneFeedbackMessage1"), Localization.LocalizedResources.GetLocalizedString("app_name"));
                _yesText = Localization.LocalizedResources.GetLocalizedString("S_WRITE_FEEDBACK");
                _noText = Localization.LocalizedResources.GetLocalizedString("S_NO_BTN");

                _state = FeedbackState.Feedback;
                await ShowMessageAsync();
            }
        }

        private async Task OnYesClickAsync()
        {
            if (_state == FeedbackState.FirstReview || _state == FeedbackState.SecondReview)
            {
                _reviewed = true;
                StoreState();
                await Launcher.LaunchUriAsync(new Uri(AppUri, UriKind.Absolute));
            }
            else if (_state == FeedbackState.Feedback)
                await SendFeedback();
        }

        public static async Task MakeReview() 
        {
            SettingsStorageHelper.SetSetting(Reviewed, true);
            await Launcher.LaunchUriAsync(new Uri(AppUri, UriKind.Absolute));
        }

        public static async Task SendFeedback()
        {
            var package = Package.Current;
            var packageId = package.Id;
            var version = packageId.Version;

            var pkgInfo = String.Format(
                                "\n\n\nPlatform: Windows Phone\n" +
                                "Version: {0}\n" +
                                "Device: {1}\n",
                                Constants.GetAppVersion(),
                                Constants.GetDeviceModel());
            
            // Body text including hardware, firmware and software info
            var body = pkgInfo;

            var subject = Localization.LocalizedResources.GetLocalizedString("S_FEEDBACK_EMAIL");

            var email = Uri.EscapeUriString(string.Format(SupportMail,
                Constants.SupportEmail, subject, body));

            await Launcher.LaunchUriAsync(new Uri(email, UriKind.Absolute));            
        }
    }
}
