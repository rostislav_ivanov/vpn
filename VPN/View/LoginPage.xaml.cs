﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using MetroLab.Common;
using VPN.Common;
using VPN.ViewModel;
using VPN.ViewModel.Http;
using VPN.ViewModel.Pages;

namespace VPN.View
{
    public sealed partial class LoginPage : IWebAuthenticationContinuable
    {
        public static LoginPage Current { get; private set; }
        public LoginPage()
        {
            InitializeComponent();
            Current = this;
        }
        
        public void ContinueWebAuthentication(Windows.ApplicationModel.Activation.WebAuthenticationBrokerContinuationEventArgs args)
        {
            ((LoginPageViewModel)ViewModel).Finalize(args);
        }

        public void ContinueAppAuthentication(string queryString)
        {
            ((LoginPageViewModel)ViewModel).FinalizeAppAuthentication(queryString);
        }

        protected override async void OnViewModelLoadedOrUpdatedSuccessfully()
        {
            if (AutoLoginAgent.Current.Session != null)
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    AppViewModel.Current.ClearNavigationStack();
                    AppViewModel.Current.NavigateToViewModel(new MainPageViewModel());
                });
            }

            base.OnViewModelLoadedOrUpdatedSuccessfully();
        }
    }
}
