﻿using System;
using Windows.ApplicationModel.Contacts;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;
using VPN.ViewModel;
using VPN.ViewModel.Http;

namespace VPN.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class InviteFriendPage
    {
        public InviteFriendPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private async void InviteFriendFromContacts(object sender, RoutedEventArgs e)
        {
            var contactPicker = new Windows.ApplicationModel.Contacts.ContactPicker();
            contactPicker.DesiredFieldsWithContactFieldType.Add(ContactFieldType.Email);
            Contact contact = await contactPicker.PickContactAsync();

            if (contact != null)
            {
                await ((InviteFriendPageViewModel) this.ViewModel).InviteFriendAsync(contact.Emails[0].Address);

                /*var emailMessage = new Windows.ApplicationModel.Email.EmailMessage();
                emailMessage.Body = "10 дней в подарок.";
                emailMessage.Subject = "Зацени новое VPN приложение!";
                var emailRecipient = new Windows.ApplicationModel.Email.EmailRecipient(contact.Emails[0].Address);
                emailMessage.To.Add(emailRecipient);
                await Windows.ApplicationModel.Email.EmailManager.ShowComposeNewEmailAsync(emailMessage);
                 */
            }
        }
    }
}
