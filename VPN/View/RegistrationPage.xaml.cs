﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;

namespace VPN.View
{
    public sealed partial class RegistrationPage 
    {
        public RegistrationPage()
        {
            this.InitializeComponent();
        }

        private void LoginTextBoxOnKeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                this.PasswordBox.Focus(FocusState.Keyboard);
            }
        }

        private void PasswordBoxOnKeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                Windows.UI.ViewManagement.InputPane.GetForCurrentView().TryHide();
            }
        }
    }
}
