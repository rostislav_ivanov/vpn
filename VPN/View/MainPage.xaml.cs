﻿using System;
using System.Diagnostics;
using Windows.ApplicationModel.DataTransfer;
using Windows.ApplicationModel.Store;
using Windows.Foundation;
using Windows.Networking.Connectivity;
using Windows.Storage;
using Windows.System.Threading;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using MetroLab.Common;
using VPN.Common;
using VPN.ViewModel;
using VPN.ViewModel.Http;
using VPN.ViewModel.Items;
using VPN.ViewModel.Pages;

namespace VPN.View
{
    public sealed partial class MainPage
    {
        private DispatcherTimer timerForRefreshingAccountStatus;
        private const int updatingVPNConnectionStatusFrequency = 40; //every * seconds
        public MainPage()
        {
            InitializeComponent();

            //SetupTimerForRefreshingAccountStatus();
            NetworkStatusChange();
        }

        /*public void SetupTimerForRefreshingAccountStatus()
        {
            timerForRefreshingAccountStatus = new DispatcherTimer();
            timerForRefreshingAccountStatus.Tick += RefreshAccountStatus;
            timerForRefreshingAccountStatus.Interval = new TimeSpan(0, 0, updatingVPNConnectionStatusFrequency);
            timerForRefreshingAccountStatus.Start();
        }

        private void RefreshAccountStatus(object sender, object e)
        {
            if (ViewModel == null)
            {
                timerForRefreshingAccountStatus.Stop();
                timerForRefreshingAccountStatus.Tick -= RefreshAccountStatus;
                timerForRefreshingAccountStatus = null;
            }
            else
            {
                ((MainPageViewModel)ViewModel).UpdateStatusAsync();
            }
        }*/

        private bool registeredNetworkStatusNotif;
        void NetworkStatusChange()
        {
            // register for network status change notifications
            try
            {
                var networkStatusCallback = new NetworkStatusChangedEventHandler(OnNetworkStatusChange);
                if (!registeredNetworkStatusNotif)
                {
                    NetworkInformation.NetworkStatusChanged += networkStatusCallback;
                    registeredNetworkStatusNotif = true;
                }
            }
            catch (Exception ex){}
        }

        async void OnNetworkStatusChange(object sender)
        {
            ((MainPageViewModel)ViewModel).UpdateStatusAsync();
        }

        /*ToDo: push notifications
        private async void NotificationClick(object sender, ItemClickEventArgs e)
        {
            NotificationItem selectedItem = ((NotificationItem)e.ClickedItem);

            if (selectedItem != null)
            {
                if (selectedItem.Type == NotificationItem.NotificationType.Browser)
                {
                    await Windows.System.Launcher.LaunchUriAsync(new Uri(selectedItem.Url));
                }
                else if (selectedItem.Type == NotificationItem.NotificationType.Rate)
                {
                    await RatingsHelper.MakeReview();
                }
                else if (selectedItem.Type == NotificationItem.NotificationType.Share)
                {
                    DataTransferManager.ShowShareUI();
                }
            }
        }*/

        private async void SelectServer(object sender, ItemClickEventArgs e)
        {
            ServerViewModel selectedServer = (ServerViewModel) e.ClickedItem;

            MainPageViewModel mainPageViewModel = ((MainPageViewModel)ViewModel);

            if (mainPageViewModel.IsExpired)
            {
                MessageDialog msg = new MessageDialog(
                    Localization.LocalizedResources.GetLocalizedString("S_NO_VPN_EXPIRED"),
                    Localization.LocalizedResources.GetLocalizedString("S_NO_VPN_EXPIRED_TITLE"));

                msg.Commands.Add(new UICommand(Localization.LocalizedResources.GetLocalizedString("S_OK"), new UICommandInvokedHandler(NavigateToPurchasesPivotItem)));

                await msg.ShowAsync();
            }
            else
            {
                mainPageViewModel.SelectServer(selectedServer);
            }
        }

        public async void NavigateToPurchasesPivotItem(IUICommand commandLabel)
        {
            this.Pivot.SelectedIndex = 1;
        }
        protected override void OnViewModelLoadedOrUpdatedSuccessfully()
        {
            var showMakeReview = ApplicationData.Current.LocalSettings.Values["ShowMakeReview"];

            if (showMakeReview != null)
            {
                bool firstOrSecond;

                if (showMakeReview.Equals("Sheduled1"))
                {
                    firstOrSecond = true;
                }
                else if (showMakeReview.Equals("Sheduled2"))
                {
                    firstOrSecond = false;
                }
                else
                {
                    return;
                }

                ThreadPool.RunAsync(o =>
                {
                    try
                    {
                        Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                            async () =>
                            {

                                await new RatingsHelper().MakeReviewWithMessages(firstOrSecond);
                                ApplicationData.Current.LocalSettings.Values["ShowMakeReview"] = firstOrSecond ? "FirstShown" : "SecondShown";
                            });

                    }
                    catch (Exception e)
                    {
                        if (Debugger.IsAttached) Debugger.Break();
                    }
                });
            }
        }

        private async void SelectProposition(object sender, ItemClickEventArgs e)
        {
            PropositionViewModel selectedProposition = (PropositionViewModel)e.ClickedItem;

            ((MainPageViewModel)ViewModel).SelectProposition(selectedProposition);
        }
    }
}
