﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using MetroLab.Common;
using VPN.ViewModel.Items;
using VPN.ViewModel.Pages;

namespace VPN.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class GuideGalleryPage
    {
        public GuideGalleryPage()
        {
            InitializeComponent();
        }

        private async void Next(object sender, TappedRoutedEventArgs e)
        {
            if (InnerFlipView.Items != null && InnerFlipView.SelectedIndex < InnerFlipView.Items.Count - 1)
            {
                await Task.Delay(100);            //very important! It doesn't work on any other way and this solution also solves animation problem
                InnerFlipView.SelectedIndex += 1;
            }
        }

        private async void FlipViewSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (null == e.AddedItems || e.AddedItems.Count == 0) return;

            var selectedPage = (GuidePageViewModel)e.AddedItems[0];

            if (null == selectedPage || selectedPage.IsLastPage) return;

            if (selectedPage.DataToCopy != null)
            {
                SelectDataToCopy();
            }
            
            foreach (var page in ((GuideGalleryPageViewModel)ViewModel).Items)
            {
                page.IsNextButtonIsVisible = false;
                page.IsSelected = false;
            }
            selectedPage.IsSelected = true;

            await selectedPage.ShowNextButtonAsync();
        }


        public void SelectDataToCopy()
        {
            var _Container = InnerFlipView.ItemContainerGenerator
                .ContainerFromItem(InnerFlipView.SelectedItem);

            if (_Container == null)
                return;

            var _Children = AllChildren(_Container);

            var _FirstName = _Children
                // only interested in TextBoxes
                .OfType<TextBox>()
                // only interested in FirstName
                .First(x => x.Name.Equals("DataToCopyTextBox"));

            _FirstName.IsEnabled = true;
            _FirstName.Focus(FocusState.Pointer);
            _FirstName.SelectAll();
        }

        public List<Control> AllChildren(DependencyObject parent)
        {
            var _List = new List<Control>();
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
            {
                var _Child = VisualTreeHelper.GetChild(parent, i);
                if (_Child is Control)
                    _List.Add(_Child as Control);
                _List.AddRange(AllChildren(_Child));
            }
            return _List;
        }
    }
}
