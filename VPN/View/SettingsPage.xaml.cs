﻿using System;
using System.Diagnostics;
using System.Reflection;
using Windows.ApplicationModel.DataTransfer;
using Windows.UI.Xaml;
using MetroLab.Common;
using VPN.ViewModel;
using VPN.ViewModel.Pages;

namespace VPN.View
{
    public sealed partial class SettingsPage
    {
        public SettingsPage()
        {
            InitializeComponent();


            TextBlockVersion.Text = string.Format("{0} {1}",
                "VPN Unlimited", Constants.GetAppVersion());
            TextBlockYearAndCopyRight.Text = "© 2015 KeepSolid Inc.";
        }

        private async void SendFeedbackOnClick(object sender, RoutedEventArgs e)
        {
            try { await RatingsHelper.SendFeedback(); }
            catch (Exception ex) { if (Debugger.IsAttached) Debugger.Break(); }
        }

        private async void AppOverviewOnClick(object sender, RoutedEventArgs e)
        {
            await AppViewModel.Current.NavigateToViewModel(new OverviewGalleryPageViewModel(){IsLoadedFromSettings = true});
        }

        private async void MakeReviewOnClick(object sender, RoutedEventArgs e)
        {
            await RatingsHelper.MakeReview();
        }

        private async void ShareOnClick(object sender, RoutedEventArgs e)
        {
             DataTransferManager.ShowShareUI();
        }


    }
}