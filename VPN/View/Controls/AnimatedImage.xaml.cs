﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace VPN.View.Controls
{ 
    public sealed partial class AnimatedImage
    {
        public AnimatedImage()
        {
            InitializeComponent();
            
            Stretch = Stretch.Uniform;            
            
            Unloaded += (s, e) => {
                _isStopped = true;                
                _cancellationTokenSource.Cancel();                
            };

            Loaded += (s, e) => {
                _isStopped = false;
                if (SourceStream != null) InitializeWithNewImage(SourceStream);
            };
        }

        class ImageFrameContainer
        {
            public ImageFrameContainer(ImageSource imageSource, TimeSpan delay)
            {
                ImageSource = imageSource;
                Delay = delay;
            }

            public ImageSource ImageSource { get; private set; }
            public TimeSpan Delay { get; private set; }

            public int PixelHeight { get; set; }
            public int PixelWidth { get; set; }
        }        

        private static async Task<ImageFrameContainer> GetFrame(PixelDataProvider provider, int width, int height, TimeSpan delay)
        {
            var pixels = provider.DetachPixelData();

            var writeableBitmap = new WriteableBitmap(width, height);
            using (var stream = writeableBitmap.PixelBuffer.AsStream())
                await stream.WriteAsync(pixels, 0, pixels.Length);

            return new ImageFrameContainer(writeableBitmap, delay) {PixelWidth = width, PixelHeight = height };
        }

        private volatile bool _isStopped = true;
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        private async Task InitializeWithNewImage(IRandomAccessStream imageStream)
        {
            try
            {
                _isStopped = true;
                _cancellationTokenSource.Cancel();

                if (imageStream == null || imageStream.Size == 0)
                {                    
                    InnerImage.Source = null;                    
                    return;
                }

                imageStream.Seek(0);
                var items = new List<ImageFrameContainer>();

                var decoder = await BitmapDecoder.CreateAsync(imageStream);
                var frameCount = decoder.FrameCount;

                if (frameCount > 1)
                {
                    for (uint frameNumber = 0; frameNumber < frameCount; frameNumber++)
                    {
                        var frame = await decoder.GetFrameAsync(frameNumber);
                        var pixelDataProvider = await frame.GetPixelDataAsync(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Ignore,
                            new BitmapTransform(), ExifOrientationMode.RespectExifOrientation, ColorManagementMode.DoNotColorManage);
                        
                        var frameProperties = await frame.BitmapProperties.GetPropertiesAsync(new List<string>());
                        var gifControlExtensionProperties = await ((BitmapPropertiesView)frameProperties["/grctlext"].Value)
                            .GetPropertiesAsync(new List<string> { "/Delay" });
                        var delay = TimeSpan.FromSeconds(Double.Parse(gifControlExtensionProperties["/Delay"].Value.ToString()) / 100);

                        items.Add(await GetFrame(pixelDataProvider, (int)frame.PixelWidth, (int)frame.PixelHeight, delay));
                    }
                }
                else
                {
                    var pixelDataProvider = await decoder.GetPixelDataAsync(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Ignore,
                        new BitmapTransform(), ExifOrientationMode.RespectExifOrientation, ColorManagementMode.DoNotColorManage);
                    items.Add(await GetFrame(pixelDataProvider, (int)decoder.PixelWidth, (int)decoder.PixelHeight, TimeSpan.Zero));
                }                    

                _isStopped = false;

                _cancellationTokenSource = new CancellationTokenSource();
                await ShowFramesLoopAsync(items, _cancellationTokenSource.Token);
            }
            catch (TaskCanceledException) { }
            catch (Exception ex) { if (Debugger.IsAttached) Debugger.Break(); }
        }

        private async Task ShowFramesLoopAsync(List<ImageFrameContainer> items, CancellationToken cancellationToken)
        {
            if (items.Count > 1)
            {
                int currentFrameNumber = 0;
                while (true)
                {
                    if (cancellationToken.IsCancellationRequested || _isStopped || Parent == null) break;

                    currentFrameNumber = currentFrameNumber % items.Count;
                    var current = items[currentFrameNumber];
                    
                    if (!IsStretch)
                    {
                        InnerImage.MaxWidth = current.PixelWidth;
                        InnerImage.MaxHeight = current.PixelHeight;
                    }

                    if (NeedToEnlarge)
                        EnlargeImage(current.PixelWidth, current.PixelHeight);

                    InnerImage.Source = current.ImageSource;                        
                    await Task.Delay(current.Delay, cancellationToken);
                    currentFrameNumber++;
                }
            }
            else if (items.Count == 1)
            {
                var current = items[0];

                if (!IsStretch)
                {
                    InnerImage.MaxWidth = current.PixelWidth;
                    InnerImage.MaxHeight = current.PixelHeight;                    
                }

                if (NeedToEnlarge)
                    EnlargeImage(current.PixelWidth, current.PixelHeight);
                
                InnerImage.Source = current.ImageSource;
            }
        }

        public bool IsStretch { get; set; }
        
        /// <summary>
        /// Enlarge Image to 150% - for EffectResultPage 
        /// </summary>
        public bool NeedToEnlarge { get; set; }

        private void EnlargeImage(int currentWidth, int currentHeight)
        {
            var windowSize = Window.Current.Bounds;

            if (currentWidth > windowSize.Width || currentHeight > windowSize.Height) return;

            var scale = 1.5;
            var width = currentWidth * scale;
            var height = currentHeight * scale;

            if (height > windowSize.Height || width > windowSize.Width)
            {
                scale = Math.Min(windowSize.Height / currentHeight, windowSize.Width / currentWidth);
                if (scale < 1) return;

                width = currentWidth * scale;
                height = currentHeight * scale;
            }

            InnerImage.MaxWidth = width;
            InnerImage.MaxHeight = height;   
        }

        public static readonly DependencyProperty SourceStreamProperty = DependencyProperty.Register(
            "SourceStream", typeof(IRandomAccessStream), typeof(AnimatedImage), new PropertyMetadata(null, PropertyChangedCallback));
        
        public IRandomAccessStream SourceStream {
            get { return (IRandomAccessStream)GetValue(SourceStreamProperty); }
            set { SetValue(SourceStreamProperty, value); }
        }

        private static async void PropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs args)
        {
            var aImage = (AnimatedImage) dependencyObject;
            var stream = (IRandomAccessStream) args.NewValue;
            if (stream == null) await aImage.InitializeWithNewImage(null);
            else aImage.InitializeWithNewImage(stream);
        }

        public static readonly DependencyProperty StretchProperty = DependencyProperty.Register(
            "Stretch", typeof(Stretch), typeof(AnimatedImage), new PropertyMetadata(null));

        public Stretch Stretch {
            get { return (Stretch)GetValue(StretchProperty); }
            set { SetValue(StretchProperty, value); }
        }
    }
}
