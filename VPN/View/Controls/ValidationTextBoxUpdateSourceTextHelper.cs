﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace VPN.View.Controls
{
    public class ValidationTextBoxUpdateSourceTextHelper : FrameworkElement
    {
        public static string GetUpdateSourceText(DependencyObject obj) {
            return (string)obj.GetValue(UpdateSourceTextProperty);
        }

        public static void SetUpdateSourceText(DependencyObject obj, string value) {
            obj.SetValue(UpdateSourceTextProperty, value);
        }

        public static readonly DependencyProperty UpdateSourceTextProperty = DependencyProperty.RegisterAttached("UpdateSourceText", 
            typeof(string), typeof(ValidationTextBoxUpdateSourceTextHelper), new PropertyMetadata(""));

        public static bool GetIsEnabled(DependencyObject obj) {
            return (bool)obj.GetValue(IsEnabledProperty);
        }

        public static void SetIsEnabled(DependencyObject obj, bool value) {
            obj.SetValue(IsEnabledProperty, value);
        }

        public static readonly DependencyProperty IsEnabledProperty = DependencyProperty.RegisterAttached("IsEnabled", 
            typeof(bool), typeof(ValidationTextBoxUpdateSourceTextHelper), new PropertyMetadata(false, IsEnabledChangedCallback));

        private static void IsEnabledChangedCallback(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            var textBox = sender as TextBox;
            if (textBox == null) return;
                   
            if ((bool)args.NewValue) textBox.TextChanged += AttachedTextBoxTextChanged;
            else textBox.TextChanged -= AttachedTextBoxTextChanged;
        }

        private static void AttachedTextBoxTextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox == null) return;
            
            var tb = (TextBox)sender;
            tb.SetValue(UpdateSourceTextProperty, tb.Text);
        }
    }
}