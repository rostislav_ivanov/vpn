﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace VPN.View.Controls
{
    public sealed partial class XamlIcon : UserControl
    {
        public XamlIcon()
        {
            this.InitializeComponent();
            (this.Content as FrameworkElement).DataContext = this;
        }

        public string IconName
        {
            get { return (string)GetValue(IconNameProperty); }
            set { SetValue(IconNameProperty, value); }
        }

        public static readonly DependencyProperty IconNameProperty =
            DependencyProperty.Register("IconName", typeof(string), typeof(XamlIcon),
            new PropertyMetadata(null, IconNamePropertyChnaged));

        public static void IconNamePropertyChnaged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            XamlIcon xamlIcon = d as XamlIcon;
            if (xamlIcon != null)
            {
                xamlIcon.LoadIcon();
            }
        }

        public void LoadIcon()
        {
            /*switch (IconName)
            {
                case "ua":
                    LayoutRootGrid.Children.Add(new UkraineFlag());
                    break;
                case "tr":
                    LayoutRootGrid.Children.Add(new TurkeyFlag());
                    break;
                case "fr":
                    LayoutRootGrid.Children.Add(new FranceFlag());
                    break;
                case "br":
                    LayoutRootGrid.Children.Add(new BrazilFlag());
                    break;
                case "jp":
                    LayoutRootGrid.Children.Add(new JapanFlag());
                    break;
                case "de":
                    LayoutRootGrid.Children.Add(new GermanyFlag());
                    break;
                case "us":
                    LayoutRootGrid.Children.Add(new USAFlag());
                    break;
                case "sg":
                    LayoutRootGrid.Children.Add(new SingapureFlag());
                    break;
                case "ro":
                    LayoutRootGrid.Children.Add(new RomainaFlag());
                    break;
                case "ru":
                    LayoutRootGrid.Children.Add(new RussiaFlag());
                    break;
                case "au":
                    LayoutRootGrid.Children.Add(new AustraliaFlag());
                    break;
                case "ca":
                    LayoutRootGrid.Children.Add(new CanadaFlag());
                    break;
                case "fi":
                    LayoutRootGrid.Children.Add(new FinlandFlag());
                    break;
                case "nl":
                    LayoutRootGrid.Children.Add(new NetherlandsFlag());
                    break;
                case "lu":
                    LayoutRootGrid.Children.Add(new LuxemburgFlag());
                    break;
                case "pa":
                    LayoutRootGrid.Children.Add(new PanamaFlag());
                    break;
                case "gb":
                    LayoutRootGrid.Children.Add(new UKFlag());
                    break;
                case "hk":
                    LayoutRootGrid.Children.Add(new HongkongFlag());
                    break;
                case "ab":
                    LayoutRootGrid.Children.Add(new AbhaziaFlag());
                    break;
                case "icon_rocket":
                    LayoutRootGrid.Children.Add(new RocketIcon());
                    break;
               case "icon_share":
                    LayoutRootGrid.Children.Add(new ShareIcon());
                    break;
               case "icon_rate":
                    LayoutRootGrid.Children.Add(new RateIcon());
                    break;
               case "icon_optimal_server":
                    LayoutRootGrid.Children.Add(new OptimalServerIcon());
                    break;
                default:
                    break;
            }*/
        }

        /*public async void Draw()
        {
            string fileContent;
            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/FlagsSVG/flag_"+ IconName + ".svg", UriKind.Absolute));
            using (StreamReader sRead = new StreamReader(await file.OpenStreamForReadAsync()))
                fileContent = await sRead.ReadToEndAsync();

            string[] words = fileContent.Split(new string[] { Environment.NewLine, " ", "\t"}, StringSplitOptions.None);
            
            List<Windows.UI.Xaml.Shapes.Path> paths = new List<Windows.UI.Xaml.Shapes.Path>();

            Windows.UI.Xaml.Shapes.Path tmpPath = null;
            string hexaColor;

            foreach (var word in words)
            {
                if (word.Equals("<path"))
                {
                    tmpPath = new Windows.UI.Xaml.Shapes.Path();
                }
                else if (word.StartsWith("fill"))
                {
                    if (tmpPath != null)
                    {
                        hexaColor = word.Substring(6, 7);
                        tmpPath.Fill = new SolidColorBrush(Color.FromArgb(
                                                                            255,
                                                                            Convert.ToByte(hexaColor.Substring(1, 2), 16),
                                                                            Convert.ToByte(hexaColor.Substring(3, 2), 16),
                                                                            Convert.ToByte(hexaColor.Substring(5, 2), 16)
                                                                        ));
                    }
                }
                else if (word.StartsWith("d="))
                {
                    if (tmpPath != null)
                    {
                        string xamlPath =
                            "<Geometry xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation'>" +
                            word.Substring(3, word.Length - 6) + "</Geometry>";
                        tmpPath.Data = XamlReader.Load(xamlPath) as Geometry;
                        paths.Add(tmpPath);
                        this.LayoutRootGrid.Children.Add(tmpPath);
                    }
                }
            }
        }*/
    }
}
