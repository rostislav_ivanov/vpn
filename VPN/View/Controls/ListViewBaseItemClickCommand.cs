﻿using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace VPN.View.Controls
{
    public class ListViewBaseItemClickCommand
    {
        public static readonly DependencyProperty CommandProperty = DependencyProperty.RegisterAttached(
            "Command", typeof(ICommand), typeof(ListViewBaseItemClickCommand), new PropertyMetadata(null, CommandPropertyChanged));

        public static void SetCommand(DependencyObject attached, ICommand value)
        {
            attached.SetValue(CommandProperty, value);
        }

        public static ICommand GetCommand(DependencyObject attached)
        {
            return (ICommand)attached.GetValue(CommandProperty);
        }

        private static void CommandPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
           ((ListViewBase)d).ItemClick += ListViewBaseItemClick;
        }

        private static void ListViewBaseItemClick(object sender, ItemClickEventArgs e)
        {
            var listViewBase = (ListViewBase)sender;
            var command = GetCommand(listViewBase);
            command.Execute(e.ClickedItem);
        }
    }
}
