﻿using System;
using System.ComponentModel;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;


namespace VPN.View.Controls
{
    public sealed partial class Sector : UserControl, INotifyPropertyChanged
    {
        private const int radius = 40;
        public Sector()
        {
            this.InitializeComponent();
            (this.Content as FrameworkElement).DataContext = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        } 

        public int Percent
        {
            get { return (int)GetValue(PercentProperty); }
            set { SetValue(PercentProperty, value); }
        }

        public static readonly DependencyProperty PercentProperty =
            DependencyProperty.Register("Percent", typeof(int), typeof(Sector),
            new PropertyMetadata(0,OnSomePropertyChnaged));

        public static void OnSomePropertyChnaged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Sector sector = d as Sector;
            if (sector != null)
            {
                sector.OnPropertyChanged("PathPercent");
                sector.OnPropertyChanged("PathRestPart");
                sector.OnPropertyChanged("PercentString");
                sector.OnPropertyChanged("PercentStringFontSize");
            }
        }

        public PathGeometry PathPercent 
        {
            get
            {
                if (Percent > 0 && Percent < 100)
                {
                    PathFigure pthFigure1 = new PathFigure();

                    //vertical line from center to percent point
                    pthFigure1.StartPoint = new Point(radius, radius);
                    LineSegment lineSeg = new LineSegment();
                    lineSeg.Point = new Point(radius*(1.0 + Math.Sin(2.0*Math.PI*Percent/100.0)),
                        radius*(1.0 - Math.Cos(2.0*Math.PI*Percent/100.0)));

                    //arc percent point to top
                    ArcSegment arcSeg1 = new ArcSegment();
                    arcSeg1.Point = new Point(radius, 0); // ending cordinates of arcs
                    arcSeg1.Size = new Size(radius, radius);
                    arcSeg1.IsLargeArc = Percent < 50 ? true : false;
                    arcSeg1.SweepDirection = SweepDirection.Clockwise;

                    PathSegmentCollection myPathSegmentCollection1 = new PathSegmentCollection();
                    myPathSegmentCollection1.Add(lineSeg);
                    myPathSegmentCollection1.Add(arcSeg1);
                    pthFigure1.Segments = myPathSegmentCollection1;

                    PathFigureCollection pthFigureCollection1 = new PathFigureCollection();
                    pthFigureCollection1.Add(pthFigure1);

                    PathGeometry pthGeometry1 = new PathGeometry();
                    pthGeometry1.Figures = pthFigureCollection1;

                    return pthGeometry1;
                }
                else
                {
                    //just draw full circle
                    //TODO: EllipseGeometry
                    PathFigure pthFigure1 = new PathFigure();
                    pthFigure1.StartPoint = new Point(radius, 0);

                    ArcSegment arcSeg1 = new ArcSegment();
                    arcSeg1.Point = new Point(radius, 2*radius);
                    arcSeg1.Size = new Size(radius, radius);

                    ArcSegment arcSeg2 = new ArcSegment();
                    arcSeg2.Point = new Point(radius, 0);
                    arcSeg2.Size = new Size(radius, radius);

                    PathSegmentCollection myPathSegmentCollection1 = new PathSegmentCollection();
                    myPathSegmentCollection1.Add(arcSeg1);
                    myPathSegmentCollection1.Add(arcSeg2);

                    pthFigure1.Segments = myPathSegmentCollection1;
                    PathFigureCollection pthFigureCollection1 = new PathFigureCollection();
                    pthFigureCollection1.Add(pthFigure1);

                    PathGeometry pthGeometry1 = new PathGeometry();
                    pthGeometry1.Figures = pthFigureCollection1;

                    return pthGeometry1;
                }
            }
        }

        public PathGeometry PathRestPart
        {
            get
            {
                if (Percent > 0 && Percent < 100)
                {
                    PathFigure pthFigure1 = new PathFigure();

                    //vertical line from center to top
                    pthFigure1.StartPoint = new Point(radius, radius);
                    LineSegment lineSeg = new LineSegment();
                    lineSeg.Point = new Point(radius, 0);

                    //arc from to top to percent point
                    ArcSegment arcSeg1 = new ArcSegment();
                    arcSeg1.Point = new Point(radius*(1.0 + Math.Sin(2*Math.PI*Percent/100.0)),
                        radius*(1.0 - Math.Cos(2*Math.PI*Percent/100.0))); // ending cordinates of arcs
                    arcSeg1.Size = new Size(radius, radius);
                    arcSeg1.IsLargeArc = Percent > 50 ? true : false;
                    arcSeg1.SweepDirection = SweepDirection.Clockwise;

                    PathSegmentCollection myPathSegmentCollection1 = new PathSegmentCollection();
                    myPathSegmentCollection1.Add(lineSeg);
                    myPathSegmentCollection1.Add(arcSeg1);

                    pthFigure1.Segments = myPathSegmentCollection1;
                    PathFigureCollection pthFigureCollection1 = new PathFigureCollection();
                    pthFigureCollection1.Add(pthFigure1);

                    PathGeometry pthGeometry1 = new PathGeometry();
                    pthGeometry1.Figures = pthFigureCollection1;

                    return pthGeometry1;
                }
                else return null;
            }
        }

        public string PercentString
        {
            get { return (Percent > 0 && Percent < 100) ? String.Concat("-", Percent, "%") : Localization.LocalizedResources.GetLocalizedString("WinPhoneNoDiscountLabel"); }
        }

        public int PercentStringFontSize
        {
            get { return (Percent > 0 && Percent < 100) ? 27 : 16; }
        }
    }
}
