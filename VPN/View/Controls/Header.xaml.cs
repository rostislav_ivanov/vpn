﻿using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using VPN.ViewModel.Http;
using VPN.ViewModel;

namespace VPN.View.Controls
{
    public sealed partial class Header : UserControl
    {
        public Header()
        {
            this.InitializeComponent();
            (this.Content as FrameworkElement).DataContext = this;

            if (VPNServerAgent.Current.AccountStatus != null)
            {
                OnOrOff = VPNServerAgent.Current.AccountStatus.User.IsVPNActive;
            }

            var statusBar = Windows.UI.ViewManagement.StatusBar.GetForCurrentView();

            if (!IsStatusBarColorNeeded)
            {
                statusBar.BackgroundColor = Color.FromArgb(255, 97, 97, 97);
            }
            else if (OnOrOff)
            {
                statusBar.BackgroundColor = Color.FromArgb(255, 131, 186, 31);
            }
            else
            {
                statusBar.BackgroundColor = Color.FromArgb(255, 229, 20, 0);
            }

            statusBar.ForegroundColor = Colors.White;
            statusBar.BackgroundOpacity = 1;
        }
        public bool IsStatusNeeded
        {
            get { return (bool)GetValue(IsStatusNeededProperty); }
            set { SetValue(IsStatusNeededProperty, value); }
        }

        public static readonly DependencyProperty IsStatusNeededProperty =
            DependencyProperty.Register("IsStatusNeeded", typeof(bool), typeof(Header),
            new PropertyMetadata(null));

        public bool IsStatusBarColorNeeded
        {
            get { return (bool)GetValue(IsStatusBarColorNeededProperty); }
            set { SetValue(IsStatusBarColorNeededProperty, value); }
        }

        public static readonly DependencyProperty IsStatusBarColorNeededProperty =
            DependencyProperty.Register("IsStatusBarColorNeeded", typeof(bool), typeof(Header),
            new PropertyMetadata(null, OnOrOffPropertyChnaged));

        public bool OnOrOff
        {
            get { return (bool)GetValue(OnOrOffProperty); }
            set { SetValue(OnOrOffProperty, value); }
        }

        public static readonly DependencyProperty OnOrOffProperty =
            DependencyProperty.Register("OnOrOff", typeof(bool), typeof(Header),
            new PropertyMetadata(null, OnOrOffPropertyChnaged));

        public static void OnOrOffPropertyChnaged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Header header = d as Header;


            var statusBar = Windows.UI.ViewManagement.StatusBar.GetForCurrentView();
            if (!header.IsStatusBarColorNeeded)
            {
                statusBar.BackgroundColor = Color.FromArgb(255, 97, 97, 97);
            }
            else if (header.OnOrOff)
            {
                statusBar.BackgroundColor = Color.FromArgb(255, 131, 186, 31);
            }
            else
            {
                statusBar.BackgroundColor = Color.FromArgb(255, 229, 20, 0);
            }

            statusBar.ForegroundColor = Colors.White;
            statusBar.BackgroundOpacity = 1;
        }

        public string ApplicationName { get { return Localization.LocalizedResources.GetLocalizedString("app_name"); } }
    }
}
