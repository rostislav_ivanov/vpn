﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using MetroLab.Common;
using VPN.ViewModel;
using VPN.ViewModel.Items;
using VPN.ViewModel.Pages;

namespace VPN.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class OverviewGalleryPage
    {
        public OverviewGalleryPage()
        {
            InitializeComponent();
        }

        private async void Next(object sender, TappedRoutedEventArgs e)
        {
            if (InnerFlipView.Items != null && InnerFlipView.SelectedIndex < InnerFlipView.Items.Count - 1)
            {
                await Task.Delay(100);
                    //very important! It doesn't work on any other way and this solution also solves animation problem
                InnerFlipView.SelectedIndex += 1;
            }
            else
            {
                CacheAgent.IsNeedToShowApplicationTour = false;
                if (((OverviewGalleryPageViewModel) ViewModel).IsLoadedFromSettings)
                {
                    AppViewModel.Current.GoBack();
                }
                else
                {
                    AppViewModel.Current.NavigateToViewModel(new LoginPageViewModel());
                }
            }
        }

        private async void InnerFlipView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (null == e.AddedItems || e.AddedItems.Count == 0) return;

            var selectedPage = (OverviewPageViewModel)e.AddedItems[0];

            if (null == selectedPage || selectedPage.IsLastPage) return;

            foreach (var page in ((OverviewGalleryPageViewModel)ViewModel).Items)
            {
                page.IsNextButtonIsVisible = false;
                page.IsSelected = false;
            }
            selectedPage.IsSelected = true;

            await selectedPage.ShowNextButtonAsync();
        }
    }
}
