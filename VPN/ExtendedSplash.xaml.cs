﻿using System;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;

namespace VPN
{
    public sealed partial class ExtendedSplash
    {
        private DispatcherTimer _showWindowTimer;

        public ExtendedSplash()
        {
            InitializeComponent();

            ProgressRing.MaxWidth = 45;
            ProgressRing.MinWidth = 45;
            ProgressRing.MaxHeight = 45;
            ProgressRing.MinHeight = 45;
            ProgressRing.Margin = new Thickness(0, 0, 0, 180);
            ProgressRing.VerticalAlignment = VerticalAlignment.Bottom;
            InnerImage.Visibility = Visibility.Visible;
            InnerImage.ImageOpened += ExtendedSplashImageImageOpened;
        }

        private void OnShowWindowTimer(object sender, object e)
        {
            _showWindowTimer.Tick -= OnShowWindowTimer;
            _showWindowTimer.Stop();

            // Activate/show the window, now that the splash image has rendered
            Window.Current.Activate();
        }

        private void ExtendedSplashImageImageOpened(object sender, RoutedEventArgs e)
        {
            // ImageOpened means the file has been read, but the image hasn't been painted yet.
            // Start a short timer to give the image a chance to render, before showing the window and starting the animation.
            _showWindowTimer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(50) };
            _showWindowTimer.Tick += OnShowWindowTimer;
            _showWindowTimer.Start();
        }
    }
}
