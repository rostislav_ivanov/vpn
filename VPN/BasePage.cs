﻿using MetroLab.Common;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace VPN
{
    public class BasePage : MvvmPage
    {
        public BasePage()
        {
#if DEBUG && WINDOWS_PHONE_APP
            Loaded += ShowMemoryUsageOnLoaded;
#endif
        }

#if DEBUG && WINDOWS_PHONE_APP
        private void ShowMemoryUsageOnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            Loaded -= ShowMemoryUsageOnLoaded;

            var textBlock = new TextBlock
            {
                Foreground = new SolidColorBrush(Colors.Red),
                IsHitTestVisible = false,
                FontSize = 15,
                VerticalAlignment = VerticalAlignment.Top,
                HorizontalAlignment = HorizontalAlignment.Right
            };
            var binding = new Binding { Source = MemoryUsage.Current, Path = new PropertyPath("UserFriendlyString") };
            textBlock.SetBinding(TextBlock.TextProperty, binding);
            var panel = Content as Grid;
            if (panel != null)
            {
                panel.Children.Add(textBlock);
            }
        }
#endif
    }
}
