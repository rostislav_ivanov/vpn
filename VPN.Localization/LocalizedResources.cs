﻿using Windows.ApplicationModel.Resources;

namespace VPN.Localization
{
    public static class LocalizedResources
    {
        private static ResourceLoader _resourceLoader;
        private static ResourceLoader _xUidResourceLoader;

        public static string GetLocalizedString(string resourceName)
        {
            if (_resourceLoader == null)
                _resourceLoader = ResourceLoader.GetForViewIndependentUse("VPN.Localization/Resources");

            return _resourceLoader.GetString(resourceName);
        }
    }
}