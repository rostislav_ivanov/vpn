﻿using System.Runtime.Serialization;

namespace VPN.Model
{
    [DataContract]
    public class Server
    {
        [DataMember(Name = "region")]
        public string Region { get; set; }

        [DataMember(Name = "domain")]
        public string Domain { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "country_code")]
        public string CountryCode { get; set; }

        [DataMember(Name = "flag_http_2x")]
        public string FlagUrl { get; set; }

    }
}
