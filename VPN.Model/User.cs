﻿using System.Runtime.Serialization;

namespace VPN.Model
{
    [DataContract]
    public class User
    {
        [DataMember(Name = "real_ip_address")]
        public string RealIP { get; set; }

        [DataMember(Name = "ip_address")]
        public string CurrentIP { get; set; }

        [DataMember(Name = "timelast")]
        public double TimeLeft { get; set; }

        [DataMember(Name = "vpn_active")]
        public bool IsVPNActive { get; set; }

        [DataMember(Name = "vpn_region")]
        public string VPNRegion { get; set; }

        [DataMember(Name = "vpn_region_name")]
        public string VPNRegionName { get; set; }

        [DataMember(Name = "vpn_region_description")]
        public string VPNRegionDescription { get; set; }
    }
}
