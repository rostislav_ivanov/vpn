﻿using System.Runtime.Serialization;
using Windows.UI.Xaml.Controls.Primitives;
using VPN.Model.SocialNetworks;

namespace VPN.Model
{
    [DataContract]
    public class CredentialsFacebookVPNServer
    {
        [DataMember(Name = "permissions")]
        public string[] Permissions { get; set; }

        [DataMember(Name = "accessToken")]
        public string AccessToken { get; set; }

        [DataMember(Name = "expirationDate")]
        public long ExpirationDate { get; set; }

        public CredentialsFacebookVPNServer(CredentialsFacebook credentialsGoogle)
        {
            this.AccessToken = credentialsGoogle.AccessToken;
            this.ExpirationDate = credentialsGoogle.ExpirationDate;
            this.Permissions = CredentialsFacebook.Permissions;
        }
    }
}
