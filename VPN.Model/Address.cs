﻿using System.Runtime.Serialization;

namespace VPN.Model
{
    [DataContract]
    public class Address
    {
        [DataMember(Name = "ip")]
        public string IP { get; set; }

        [DataMember(Name = "speed")]
        public string Speed { get; set; }
    }
}
