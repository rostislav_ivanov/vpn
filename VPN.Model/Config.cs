﻿using System.Runtime.Serialization;

namespace VPN.Model
{
    [DataContract]
    public class Config
    {
        [DataMember(Name = "username_xauth")]
        public string UsernameXauth { get; set; }

        [DataMember(Name = "addresses")]
        public Address[] Addresses { get; set; }

        [DataMember(Name = "psk")]
        public string PresharedKey { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }

    }
}
