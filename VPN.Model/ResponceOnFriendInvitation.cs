﻿using System.Runtime.Serialization;

namespace VPN.Model
{
    [DataContract]
    public class ResponceOnFriendInvitation
    {
        [DataMember(Name = "response")]
        public int Response { get; set; }

        [DataMember(Name = "code")]
        public string Session { get; set; }
    }
}