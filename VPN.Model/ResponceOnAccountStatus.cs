﻿using System.Runtime.Serialization;

namespace VPN.Model
{
    [DataContract]
    public class ResponceOnAccountStatus
    {
        [DataMember(Name = "response")]
        public int Response { get; set; }

        [DataMember(Name = "account")]
        public User User { get; set; }
    }
}
