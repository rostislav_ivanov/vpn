﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace VPN.Model
{
    [DataContract]
    public class ResponceOnServersList
    {
        [DataMember(Name = "response")]
        public int Response { get; set; }

        [DataMember(Name = "servers")]
        public List<Server> Items { get; set; }
    }
}
