﻿using System.Runtime.Serialization;

namespace VPN.Model
{
    [DataContract]
    public class ResponceOnConfigInfo
    {
        [DataMember(Name = "response")]
        public int Response { get; set; }

        [DataMember(Name = "config")]
        public Config Config { get; set; }
    }
}
