﻿using System;

namespace VPN.Model
{
    public class VPNResponceException: Exception
    {
        public VPNResponceException(string message) : base(message) { }
    }
}