﻿using System.Runtime.Serialization;

namespace VPN.Model
{
    [DataContract]
    public class UserInfo
    {
        [DataMember(Name = "autologin_url")]
        public string CabinetUrl { get; set; }
    }
}
