﻿using System.Runtime.Serialization;

namespace VPN.Model
{
    [DataContract]
    public class ResponseOnLogin
    {
        [DataMember(Name = "response")]
        public int Response { get; set; }

        [DataMember(Name = "session")]
        public string Session { get; set; }

        [DataMember(Name = "userinfo")]
        public UserInfo UserInfo { get; set; }
        
    }
}
