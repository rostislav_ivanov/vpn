﻿using System.Runtime.Serialization;

namespace VPN.Model.SocialNetworks
{
    [DataContract]
    public class CredentialsKeepSolidID
    {
        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }

        [DataMember(Name = "key")]
        public string Key { get; set; }

    }
}
