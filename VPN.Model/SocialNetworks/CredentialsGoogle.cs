﻿using System.Runtime.Serialization;

namespace VPN.Model.SocialNetworks
{
    [DataContract]
    public class CredentialsGoogle
    {
        [DataMember(Name = "access_token")]
        public string AccessToken { get; set; }

        [DataMember(Name = "token_type")]
        public string TokenType { get; set; }

        [DataMember(Name = "expires_in")]
        public long ExpiresDate { get; set; }

        [DataMember(Name = "id_token")]
        public string IdToken { get; set; }

        [DataMember(Name = "refresh_token")]
        public string RefreshToken { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "code")]
        public string Code { get; set; }

        [DataMember(Name = "serviceProvider")]
        public string ServiceProvider { get; set; }

        [DataMember(Name = "Key")]
        public string Key { get; set; }
    }
}
