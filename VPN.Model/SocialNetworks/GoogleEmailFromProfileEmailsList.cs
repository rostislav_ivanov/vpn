﻿using System.Runtime.Serialization;

namespace VPN.Model.SocialNetworks
{
    [DataContract]
    public class GoogleEmailFromProfileEmailsList
    {
        [DataMember(Name = "value")]
        public string Email { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }
    }
}
