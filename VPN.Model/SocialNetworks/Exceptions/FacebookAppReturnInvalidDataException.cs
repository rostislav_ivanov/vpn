﻿using System;

namespace VPN.Model.SocialNetworks.Exceptions
{
    public class FacebookAppReturnInvalidDataException: Exception
    {
        public FacebookAppReturnInvalidDataException(string message) : base(message) { }
    }
}