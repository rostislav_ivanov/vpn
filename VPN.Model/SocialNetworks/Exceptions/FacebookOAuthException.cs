﻿using System;

namespace VPN.Model.SocialNetworks.Exceptions
{
    public class FacebookOAuthException : Exception
    {
        public FacebookOAuthException(string message) : base(message) { }
    }
}
