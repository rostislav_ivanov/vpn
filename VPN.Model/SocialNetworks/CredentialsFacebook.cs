﻿using System.Runtime.Serialization;

namespace VPN.Model.SocialNetworks
{
    [DataContract]
    public class CredentialsFacebook
    {
        [DataMember(Name = "accessToken")]
        public string AccessToken { get; set; }

        [DataMember(Name = "expirationDate")]
        public long ExpirationDate { get; set; }

        [DataMember(Name = "realExpirationDate")]
        public System.DateTime RealExpirationDate { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        public readonly static string[] Permissions = new[] { "email", "public_profile" };

        public static string Scope
        {
            get
            {
                string scope = "";
                for (int i = 0; i < Permissions.Length; i++)
                {
                    scope += Permissions[i];
                    if (i != Permissions.Length - 1)
                    {
                        scope += ",";
                    }
                }

                return scope;
            }
        }

        [DataMember(Name = "Key")]
        public string Key { get; set; }
    }
}
