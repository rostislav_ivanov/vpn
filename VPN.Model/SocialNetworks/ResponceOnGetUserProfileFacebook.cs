﻿using System.Runtime.Serialization;

namespace VPN.Model.SocialNetworks
{
    [DataContract]
    public class ResponceOnGetUserProfileFacebook
    {
        [DataMember(Name = "email")]
        public string Email { get; set; }
    }
}

