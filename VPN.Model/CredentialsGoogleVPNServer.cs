﻿using System.Runtime.Serialization;
using Windows.UI.Xaml.Controls.Primitives;
using VPN.Model.SocialNetworks;

namespace VPN.Model
{
    [DataContract]
    public class CredentialsGoogleVPNServer
    {
        [DataMember(Name = "accessToken")]
        public string AccessToken { get; set; }

        [DataMember(Name = "token_type")]
        public string TokenType { get; set; }

        [DataMember(Name = "expirationDate")]
        public long ExpiresDate { get; set; }

        [DataMember(Name = "expires_in")]
        public string ExpiresDateString { get; set; }

        [DataMember(Name = "id_token")]
        public string IdToken { get; set; }

        [DataMember(Name = "refreshToken")]
        public string RefreshToken { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "code")]
        public string Code { get; set; }

        [DataMember(Name = "serviceProvider")]
        public string ServiceProvider { get; set; }

        public CredentialsGoogleVPNServer(CredentialsGoogle credentialsGoogle)
        {
            this.AccessToken = credentialsGoogle.AccessToken;
            this.Code = credentialsGoogle.Code;
            this.Email = credentialsGoogle.Email;
            this.ExpiresDate = credentialsGoogle.ExpiresDate;
            this.ExpiresDateString = ExpiresDate.ToString();
            this.IdToken = credentialsGoogle.IdToken;
            this.RefreshToken = credentialsGoogle.RefreshToken;
            this.ServiceProvider = credentialsGoogle.ServiceProvider;
            this.TokenType = credentialsGoogle.TokenType;
        }
    }
}
