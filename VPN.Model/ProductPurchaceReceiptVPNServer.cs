﻿using System.Runtime.Serialization;

namespace VPN.Model
{
    [DataContract]
    public class ProductPurchaceReceiptVPNServer
    {
        [DataMember(Name = "xml")]
        public string Receipt { get; set; }

    }
}
